import React, { FC } from 'react'
import {
  ChatBubbleWrapperStyled,
  ChatContentWrapperStyled,
  ChatContentStyled,
  ChatDateStyled,
  ChatNameStyled,
  ChatContentWrapperActionStyled
} from 'compose/popup/change/chat/styled'
import { IChildren } from 'utils/interface'

interface IBubble extends IChildren {
  name?: string;
  date?: string;
  className?: string;
}

export const ChatBubble: FC<IBubble> = ({
  name,
  date,
  className,
  children
}) => {

  const Content = name ? ChatContentWrapperStyled : ChatContentWrapperActionStyled
  return (
    <ChatBubbleWrapperStyled className={className}>
      { name && <ChatNameStyled name={name} aria-label={name}>{ name }
        <ChatDateStyled>{ date }</ChatDateStyled>
      </ChatNameStyled>}
      <Content>
        <ChatContentStyled>{children}
        { !name && <ChatDateStyled>{ date }</ChatDateStyled>}</ChatContentStyled>
      </Content>
    </ChatBubbleWrapperStyled>
  )
}