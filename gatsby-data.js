import React from 'react';
import { getApolloContext } from "react-apollo"
import { renderToStaticMarkup } from 'react-dom/server'

function makeDefaultQueryInfo() {
  return {
      seen: false,
      observable: null
  };
}
export const RenderPromises = (function () {
  function RenderPromises() {
      this.queryPromises = new Map();
      this.queryInfoTrie = new Map();
  }
  RenderPromises.prototype.registerSSRObservable = function (observable, props) {
    //console.log('registerSSRObservable', props);
      this.lookupQueryInfo(props).observable = observable;
  };
  RenderPromises.prototype.getSSRObservable = function (props) {
    //console.log('getSSRObservable', props);
      return this.lookupQueryInfo(props).observable;
  };
  RenderPromises.prototype.addQueryPromise = function (queryInstance, finish) {
      var info = this.lookupQueryInfo(queryInstance.getOptions());
      console.log('addQueryPromise -------------');
      if (!info.seen) {
          this.queryPromises.set(queryInstance.getOptions(), new Promise(function (resolve, reject) {
            //console.log('addQueryPromise fetch', queryInstance.getOptions());
            try {
              const data = queryInstance.fetchData()
              console.log('FETCH DATA', data);
              resolve(data)
            } catch (e) {
              console.log('FETCH ERROR', e);
              reject()
            }
          }));
          return null;
      }
      return finish();
  };
  RenderPromises.prototype.hasPromises = function () {
      return this.queryPromises.size > 0;
  };
  RenderPromises.prototype.consumeAndAwaitPromises = function () {
      var _this = this;
      var promises = [];
      this.queryPromises.forEach(function (promise, queryInstance) {
          _this.lookupQueryInfo(queryInstance).seen = true;
          promises.push(promise);
      });
      this.queryPromises.clear();
      try {
        console.log('CONSUME', promises);
        return Promise.all(promises);
        //const result = Promise.all(promises);
        //const result = await Promise.allSettled(promises);
        //console.log('STATUS', result.map(x=>x.status));
        //return result
      } catch (e) {
        console.log('CONSUME ERROR', e);
      }
      return
  };
  /*
  RenderPromises.prototype.addQueryPromise = function (queryInstance, finish) {
    var info = this.lookupQueryInfo(queryInstance.getOptions());
    if (!info.seen) {
        this.queryPromises.set(queryInstance.getOptions(), new Promise(function (resolve) {
            resolve(queryInstance.fetchData());
        }));
        return null;
    }
    return finish();
};

RenderPromises.prototype.consumeAndAwaitPromises = function () {
    var _this = this;
    var promises = [];
    this.queryPromises.forEach(function (promise, queryInstance) {
        _this.lookupQueryInfo(queryInstance).seen = true;
        promises.push(promise);
    });
    this.queryPromises.clear();
    return Promise.all(promises);
};
*/
  RenderPromises.prototype.lookupQueryInfo = function (props) {
      var queryInfoTrie = this.queryInfoTrie;
      var query = props.query, variables = props.variables;
      var varMap = queryInfoTrie.get(query) || new Map();
      if (!queryInfoTrie.has(query))
          queryInfoTrie.set(query, varMap);
      var variablesString = JSON.stringify(variables);
      var info = varMap.get(variablesString) || makeDefaultQueryInfo();
      if (!varMap.has(variablesString))
          varMap.set(variablesString, info);
      return info;
  };
  return RenderPromises;
}());
const __assign = Object.assign


export function myGetDataFromTree(tree, context) {
    if (context === void 0) { context = {}; }
    return getMarkupFromTree({
        tree: tree,
        context: context,
        renderFunction: renderToStaticMarkup
    });
}
export function getMarkupFromTree(_a) {
    var tree = _a.tree, _b = _a.context, context = _b === void 0 ? {} : _b, _c = _a.renderFunction, renderFunction = _c === void 0 ? renderToStaticMarkup : _c;
    var renderPromises = new RenderPromises();
    function process() {
        var ApolloContext = getApolloContext();
        //console.log('ApolloContext', ApolloContext);
        //console.log('tree', tree)
        return new Promise(function (resolve) {
            var element = React.createElement(ApolloContext.Provider, { value: __assign(__assign({}, context), { renderPromises: renderPromises }) }, tree);
            //console.log('--element', element);
            resolve(renderFunction(element));
        }).then(function (html) {
          console.log('--hh', html);
          console.log('--hh renderPromises', renderPromises);
          if (renderPromises.hasPromises()) {
              console.log('--hh HAS!');
              return renderPromises.consumeAndAwaitPromises()
                .then(function () {
                  console.log('--hh CONSUMED!');
                  process()
                })
          } else {
              return html;
          }
        });
    }
    return Promise.resolve().then(process);
}
//# sourceMappingURL=getDataFromTree.js.map
