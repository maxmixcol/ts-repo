import React, { FC, memo, useMemo } from 'react'
import { Header } from 'components/header/Header'

import { StatusBarCompose } from 'compose/statusbar/StatusBarCompose'

import { Main } from 'components/common/common'

import { PrivateCompose } from 'compose/private/PrivateCompose'
import { IChildren } from 'utils/interface'

interface IDashboardProps extends IChildren {
  header?: React.ReactNode;
  route: string;
  location: Location;
}

const DashboardViewMemo: FC<IDashboardProps> = ({ route, header, children, location }) => {
  return (
    <PrivateCompose route={route}>
      <Header>
        { header }
      </Header>
      <Main>
        { children }
      </Main>
      <StatusBarCompose selected={route} />
    </PrivateCompose>
  )
}

export const DashboardView = memo(DashboardViewMemo)