import React, { FC, useState } from 'react';

import { IChildren } from 'utils/interface';
import { IMessageBoxProps, MessageBox } from 'components/message/MessageBox';
import { MessageIcon } from 'compose/popup/change/styled';
import useStorage from 'use/storage/useStorage';

interface IProps extends IChildren {
  flagId: string;
  messageBoxProps: Omit<IMessageBoxProps, 'onClose' | 'children'>;
}

export const FloatingMessageBox: FC<IProps> = ({
  flagId,
  messageBoxProps,
  children
}) => { 

  const { getFlag, setFlag } = useStorage()
  const flagValue = getFlag(flagId)

  const [ showMessage, setShowMessage ] = useState(flagValue)
  const onChange = (value) => {
    setShowMessage(value)
    setFlag(flagId, value)
  }

  const clickInfo = () => {
    onChange(true)
  }
  const closeMessage = () => {
    onChange(false)
  }

  
  return (
    showMessage
    ?
      <MessageBox {...messageBoxProps} onClose={closeMessage}>
        { children }
      </MessageBox>
    : <div onClick={clickInfo}><MessageIcon /></div>
  );
}