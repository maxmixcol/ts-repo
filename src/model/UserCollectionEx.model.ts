import { mapArrayBy, mapBy } from "utils/helpers";
import { IElementDoubleMap, IMappedElementDoubleMap, IMappedElementMap } from "utils/interface";
import { IElementModel } from "./Element.model";
import { IUserCollectionModel } from "./UserCollection.model";
import { IUserElementModel, IUserElementModelEx } from "./UserElement.model";
import { IUserExchangeModel } from "./UserExchange.model";

export interface IUserCollectionExModel {
  code: number;
  userCollection: IUserCollectionModel;
  elements: IElementModel[];
  userElements: IUserElementModel[];
  exchanges: IUserExchangeModel[];
  countExchanges: number;
  countFaults: number;
  countRepeated:number;
}

export default class UserCollectionExModel {
/*
  {
    "id":119,
    "iduser":22,
    "CodigoC":5,
    "Fecha":"0000-00-00 00:00:00",
    "FechaMod":"2019-02-20 11:37:09",
    "Caracteristicas":"3NNN:SNN",
    "Comentarios":"",
    "EFaltas":"",
    "ERepes":""
  }
*/
  static groups = {
    B: 'group', // B: Basica
    C: 'group', // C: Coloca
    N: 'group', // N: Nuevo Fichaje Act.
    O: 'type' // O: other
  }
  static toElementMaps = (list: IUserCollectionExModel[]): IMappedElementMap => {
    return list?.reduce((tot, items: IUserCollectionExModel): IMappedElementMap => {
      tot[items.elements[0].code] = {
        mappedElements: mapBy(items.elements, 'order'),
        mappedUserElements: (items.userElements || []).reduce((acc, uel) => {
          const ob: IUserElementModelEx = { ...uel, amount: 1 }
          if (acc[uel.order]) {
            ob.amount = acc[uel.order].amount + 1
          }
          acc[uel.order] = ob
          return acc
        }, {})
      }
      return tot
    }, {})
  }

  static paginateWithGroups= <T>(list: T[], pagLength: number): T[][] => {
    const get = (arr, pk) => {
      const arrPages = []
      const mapElementsByGroup = {}
      
      arr.forEach((item) => {
        if (item) {
          const key = item[pk]
          if (!mapElementsByGroup[key]) {
            mapElementsByGroup[key] = []
          }
          mapElementsByGroup[key].push(item)          
        }
      })
      Object.keys(mapElementsByGroup).forEach((x) => {
        const arrByGroup = mapElementsByGroup[x]
        for (var i = 0, l = arrByGroup.length; i < l; i += pagLength) {
          const elements = arrByGroup.splice(0, pagLength);
          arrPages.push(elements)
        }
      })
      return arrPages
    }
    const byType = mapArrayBy(list, 'type')
    let result = []
    for (var key in byType) {
      result = result.concat(get(byType[key], UserCollectionExModel.groups[key] ? 'group' : 'type'));
    }
    return result
  }
}
