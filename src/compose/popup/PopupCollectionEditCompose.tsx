import React, { FC } from 'react'

import { LinearProgressStyled } from 'components/status/styled'
import { FullModalStyled } from './_shared/styled'
import { BigNameCollectionStyled } from 'components/popup/collection/styled'
import { EditContainer } from 'components/popup/edit/EditContainer'
import usePopupCollectionEdit from 'use/popup/impl/usePopupCollectionEdit'


export const PopupCollectionEditCompose: FC = () => {
  const { labels, result, fullModal } = usePopupCollectionEdit()
  const {
    editContainerProps,
    showProgress,
  } = result || {}

  return (
    <>{ fullModal.open &&
      <FullModalStyled
      open={open}
      close={close}
      headerDark={true}
      title={<BigNameCollectionStyled>{labels.title}</BigNameCollectionStyled>}
    >
        { showProgress && <LinearProgressStyled /> }
        <EditContainer {...editContainerProps} />
      </FullModalStyled>
    }
    </>
  )
}
