import { useEffect, useState } from "react";
import { TFilterOptionUpdate } from "compose/filter/FilterCompose";
import { ChangeOffer, NewChangeFilter } from "model/UserChange.model";
import UserExchangeModel, { IUserExchangeModel } from "model/UserExchange.model";
import moment, { Moment } from "moment";
import { filters } from "setup/constants";
import useI18n from "use/i18n/useI18n"
import FilterModel, { IFilterModelMap, TFilterMapper } from "../store/model/FilterModel";
import SortModel from "../store/model/SortModel";
import SortTypeEnum from "../store/model/SortTypeEnum";
import useFilter from "../useFilter";
import { ISortModelMap } from "utils/interface";

interface ISearchData {
  days: number;
  min: number;
  name: string;
  liked: number;
}
interface IUserExchangeModelParsed extends IUserExchangeModel {
  searchData: ISearchData
}
/*
- ordenar por: ultimo acceso usuario. total. likes
- buscar por: nombre de usuario
- filtros:
  - con offer/ sin offer
  - coleccion
  - usuarios favoritos
  - num cromos
  */

const filterId = filters.newChange

/* SORT */
const sortLabelIds = {
  access: 'filter.sort.access',
  favCollector: 'filter.sort.favCollector',
  totalItems: 'filter.sort.totalItems',
}
enum ChangeSort {
  ACCESS = 'access',
  FAV_COLLECTOR = 'favCollector',
  TOTAL_ITEMS = 'totalItems'
} 
const SORT_MAP: ISortModelMap = {
  [ChangeSort.ACCESS]: [
    { name: 'searchData.days', type: SortTypeEnum.Number, asc: false },
    { name: 'searchData.min', type: SortTypeEnum.Number, asc: false },
  ],
  [ChangeSort.FAV_COLLECTOR]: [
    { name: 'searchData.liked', type: SortTypeEnum.Number, asc: false },
    { name: 'searchData.days', type: SortTypeEnum.Number, asc: false },
    { name: 'searchData.min', type: SortTypeEnum.String, asc: true },
  ],
  [ChangeSort.TOTAL_ITEMS]: [
    { name: 'searchData.min', type: SortTypeEnum.Number, asc: false },
    { name: 'searchData.days', type: SortTypeEnum.Number, asc: false },
  ]
}
/* FILTER */
const labelIds = {
  byCollector: 'filter.by.collector',
  byCollection: 'filter.by.collection',
  offerFilled: 'filter.label.changeOffer.filled',
  offerEmpty: 'filter.label.changeOffer.empty',
  searchChange: 'filter.by.searchChange',
}

const { SORT, SEARCH, OFFER, COLLECTOR, COLLECTION } = NewChangeFilter

const FILTER_MAP: IFilterModelMap = {
  [OFFER]: { name: OFFER, label: '', input: ChangeOffer.FILLED, server: true },
  [COLLECTION]: { name: COLLECTION, label: labelIds.byCollection, input: '' },
  [SEARCH]: { name: SEARCH, label: labelIds.searchChange, input: '' },
  [SORT]: { name: SORT, label: '', input: ChangeSort.ACCESS },
}

const getSearchData = (item: IUserExchangeModel, now: Moment): ISearchData => {
  const total = UserExchangeModel.getTotal(item.exchanges)
  const min = Math.min(total.mine, total.his)
  const days = moment(item.user.lastLogin * 1000).diff(now, 'days')
  const name = FilterModel.flatName(item.user.username)
  const liked = item.user.mine ? 100000 : 0 + item.user.likes

  return { days, min, name, liked }
}
const updateOptions = (list: IUserExchangeModelParsed[], t: any): TFilterOptionUpdate[] => {
  const searchOptions = {}
  const offerOptions = [
    { title: t(labelIds.offerFilled), value: ChangeOffer.FILLED },
    { title: t(labelIds.offerEmpty), value: ChangeOffer.EMPTY },
  ]
  // const collectorOptions = {}
  const collectionOptions = {}

  list.forEach(item => {
    const { user, exchanges, searchData } = item
    // collectorOptions[user.iduser] = { title: user.username, value: user.iduser }
    exchanges.forEach(({ collection }) => {
      if (collection && collection.code) {
        collectionOptions[collection.code] = { title: collection.name, value: collection.code }
      }
    })
    
    searchOptions[user.iduser] = searchData.name
  })
  return [
    { name: OFFER, options: offerOptions },
    // { name: COLLECTOR, options: FilterModel.EMPTY.concat(SortModel.sortOptions(collectorOptions)) },
    { name: COLLECTION, options: FilterModel.EMPTY.concat(SortModel.sortOptions(collectionOptions)) },
    { name: SEARCH, query: (value: any, input: any) => searchOptions[value].indexOf(input) !== -1 },
  ]
}
// const mineLengthReducer = (tot: number, acc: IExchangeData): number => tot + acc.mine.length
const filterMapper: TFilterMapper<IUserExchangeModel> = (
  { user, exchanges },
  { name, input, query }
) => {
  // const mineLength = exchanges.reduce(mineLengthReducer, 0)
  return {
    [SEARCH]: query && input && query(user.iduser, input),
    [OFFER]: true,
    // [OFFER]: ({ // no se si es necesario
    //   [ChangeOffer.FILLED]: mineLength > 0,
    //   [ChangeOffer.EMPTY]: mineLength === 0,
    // })[input],
    // [COLLECTOR]: user.iduser === input,
    [COLLECTION]: !!exchanges.find(e => e.collection.code === input),
  }[name]
}

const useNewChangeFilter = () => {
  const { t, tm } = useI18n()
  const sortLabels = tm(sortLabelIds)

  const {
    getFilter,
    getInit,
    onChange,
    updateFilter,
    values,
  } = useFilter(filterId)

  const init = () => {
    useEffect(() => { getInit(FILTER_MAP, t, ChangeSort.ACCESS)}, [])
  }

  const process = (list: IUserExchangeModel[]) => {
    let listIds = list.map(({ user }) => `${user.iduser}:${user.likes}`)
    const [ parsedList, setParsedList ] = useState<IUserExchangeModelParsed[]>()

    useEffect(() => {
      if (list.length) {
        const sortFilterOptions = SortModel.updateSortOptions({
          sortMap: SORT_MAP, sortLabels
        })
        const now = moment()
        const parsedList = list.map(item => ({ ...item, ...{ searchData: getSearchData(item, now) }}))
        updateFilter(SORT, { options: sortFilterOptions })
        updateOptions(parsedList, t).forEach(o => updateFilter(o.name, o))
        setParsedList(parsedList)
      }
    }, [ listIds.join() ])
    
    return {
      model: getFilter || [],
      list: parsedList
    }
  }

  return {
    init,
    process,
    filterProps: {
      onChange,
      filterId,
      updateOptions,
      filterMapper,
      sortMap: SORT_MAP,
      sortName: SORT,
      searchName: SEARCH
    },
    updateFilter,
    values
  }  
}

export default useNewChangeFilter