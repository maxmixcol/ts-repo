import { RawButtonWithIcon } from 'components/button/RawButtonWithIcon';
import { PrimaryEmptyButton } from 'components/common/common.styled';
import { HeroColor } from 'components/hero/Hero';
import React, { FC } from 'react';
import { IClick } from 'utils/interface';
import { ColorListItemStyled } from './styled';


interface IItemProps {
  name: string;
  value: string | number;
  color?: HeroColor;
}

export const CardItem: FC<IItemProps> = ({ name, value, color }) => (
  <ColorListItemStyled color={color}>
    <span>{ name }:</span>
    <span>{ value }</span>
  </ColorListItemStyled>
)

export const CardItemButton: FC<IItemProps & IClick> = ({ name, value, color, onClick }) => (
  <ColorListItemStyled color={color}>
    <RawButtonWithIcon item={PrimaryEmptyButton} onClick={onClick}>
      <span>{ name }:</span>
      <span>{ value }</span>
    </RawButtonWithIcon>
  </ColorListItemStyled>
)