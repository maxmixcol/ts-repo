
import React, { FC } from 'react';
import { flags } from 'setup/constants';
import useI18n from 'use/i18n/useI18n';
import { FloatingMessageBox } from 'components/message/FloatingMessageBox';

interface IProps {
  label: string;
}
export const IntroMessageCompose: FC<IProps> = ({
  label
}) => {
  
  const flagId = flags.popupChangeIntro

  const { t } = useI18n()

  const helpButton = t('popup.change.helpButton')
  // const helpList1 = 
  //   <Trans i18nKey='popup.change.helpList1'>
  //     <Icon icon={icons.edit} />
  //   </Trans>
  // const helpList2 = 
  //   <Trans i18nKey='popup.change.helpList2'>
  //     <MiniCard> </MiniCard>
  //   </Trans>
  const helpList3 = t('popup.change.helpList3')
  // const helpList4 = t('popup.change.helpList4')

  return (
    <FloatingMessageBox
      flagId={flagId}
      messageBoxProps={{
        variant: 'info',
        closeButton: helpButton
      }}>
      <>
        <p>{ label }</p>
        <p>{ helpList3 }</p>
      </>
    </FloatingMessageBox>
  )
}

