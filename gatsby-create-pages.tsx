export const createPages = async ({ actions, graphql, reporter }) => {
  const { createPage } = actions
  const Template = require.resolve(`./src/templates/MDWrapper.tsx`)
  const result = await graphql(`
    {
      allMarkdownRemark(
        sort: { order: DESC, fields: [frontmatter___date] }
        limit: 1000
      ) {
        edges {
          node {
            id
            html
            frontmatter {
              slug
              title
            }
          }
        }
      }
    }
  `)

    // Handle errors
    if (result.errors) {
      reporter.panicOnBuild(`Error while running GraphQL query.`)
      return
    }
    result.data.allMarkdownRemark.edges.forEach(({ node }) => {
      createPage({
        path: node.frontmatter.slug,
        component: Template,
        context: {
          slug: node.frontmatter.slug,
          markdownRemark: node
        },
      })
    })
  }