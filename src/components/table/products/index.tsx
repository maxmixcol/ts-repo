/**
 * React, Gatsby, Jest, TypeScript, Apollo - Starter
 * https://github.com/eduard-kirilov/gatsby-ts-apollo-starter
 * Copyright (c) 2020 Eduard Kirilov | MIT License
 */
import React, { FC, memo } from 'react';
import { ICollection } from 'utils/interface';

import { Edit, Delete } from '@material-ui/icons';

import {
  Tooltip,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  IconButton,
  TableSortLabel,
  TablePagination,
} from 'components/core';
import { LinearStatus } from 'components/status';
import { TableStyled, ButtonWrapper } from './styles';

interface IProps {
  direction: any;
  handleChangePage: (_: unknown, newPage: number) => void;
  handleChangeRowsPerPage: (e: any) => void;
  handleDelProduct: (_id: number) => void;
  handleOpen: (_id: number) => void;
  loading?: boolean;
  page?: number;
  products: {
    total?: number;
    page?: number;
    data?: ICollection[];
  };
  perPage: number;
  toggleDirection: () => void;
}

const TableProductsMemo: FC<IProps> = ({
  direction,
  handleChangePage,
  handleChangeRowsPerPage,
  handleDelProduct,
  handleOpen,
  loading = false,
  page,
  perPage = 5,
  products,
  toggleDirection,
}) => {
  const { data = [], total = 0 } = products;
  
  return (
    <Paper>
      <TableContainer>
        <TableStyled size="small" aria-label="products table">
          <TableHead>
            <TableRow>
              <TableCell
                sortDirection={direction}
              >
                <TableSortLabel
                  active={true}
                  direction={direction}
                  onClick={toggleDirection}
                >
                  Id
                </TableSortLabel>
              </TableCell>
              <TableCell>Name</TableCell>
              <TableCell>Price</TableCell>
              <TableCell>Actions</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {loading
              ? <LinearStatus />
              : data.map((item) => (
              <TableRow key={item.code}>
                <TableCell>{item.code}</TableCell>
                <TableCell>{item.name}</TableCell>
                <TableCell>{item.description}</TableCell>
                <TableCell>
                  <ButtonWrapper>
                    <Tooltip title="Change this product">
                      <IconButton
                        aria-label="edit"
                        onClick={() => handleOpen(item.code)}
                        color="inherit"
                        size="small"
                      >
                        <Edit fontSize="small" />
                      </IconButton>
                    </Tooltip>
                    <Tooltip title="Delete this product">
                      <IconButton
                        aria-label="remove"
                        onClick={() => handleDelProduct(item.code)}
                        color="inherit"
                        size="small"
                      >
                        <Delete fontSize="small" />
                      </IconButton>
                    </Tooltip>
                  </ButtonWrapper>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </TableStyled>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={total}
        rowsPerPage={perPage}
        page={total > 0 ? page : -1}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </Paper>
  );
};

export const TableProducts = memo(TableProductsMemo);
