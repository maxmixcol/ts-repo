import { v4 as uuid } from 'uuid';

export default class ErrorModel {
  id = uuid();
  status = 0;
  message = '';
  errors = [];
  url = '';
  raw = null;
}