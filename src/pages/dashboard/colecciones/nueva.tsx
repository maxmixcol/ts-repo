import React, { memo, useEffect } from 'react'
import { routes } from 'setup/constants'

import { usePageQuery, useQueryAll } from 'use/fetch/useQuery'
import useAuth from 'use/auth/useAuth'
import useNewCollectionFilter from 'use/filter/impl/useNewCollectionFilter'

import { FilterCompose } from 'compose/filter/FilterCompose'
import useI18n from 'use/i18n/useI18n'
import { HeaderTabs } from 'components/header/types/HeaderTabs'

import { SingleListCompose } from 'compose/filter/SingleListCompose'
import { CardCollectionDashboardNewCollectionCompose } from 'compose/card/collection/CardCollectionDashboardNewCollectionCompose'
import { BackIcon } from 'components/common/common.styled'
import { LinearProgress } from '@material-ui/core'
import { DASHBOARD_COLLECTIONS_NEW_ALL_SEARCH_QUERY, DASHBOARD_COLLECTIONS_NEW_SEARCH_PAGE_QUERY, DASHBOARD_COLLECTIONS_NEW_USER_COLLECTIONS_QUERY } from 'graphql/query'
import { EmptyListCompose } from 'compose/empty/EmptyListCompose'
import { ISearchCollectionModel } from 'model/SearchCollection.model'
import { useModalCollectionNew } from 'use/modal/useModal'
import { IUserCollectionModel } from 'model/UserCollection.model'
import { PopupNewCollectionCompose } from 'compose/popup/PopupNewCollectionCompose'
import { DashboardView } from 'components/typography/view/dashboard/DashboardView'

const labelIds = {
  userCollectionsTitle: 'pages.dashboard/colecciones.title',
  newCollectionTitle: 'pages.dashboard/colecciones/nueva.title',
  numCollections: 'filter.numCollections',
}


const DashboardColeccionesNuevaPage = ({ location }) => {
  const userId = useAuth().user
  const { t, tm } = useI18n()
  const labels = tm(labelIds)

  const { open: openCollection, isOpen: isCollectionOpen } = useModalCollectionNew()
  const onClickCollection = (item: ISearchCollectionModel) => {
    openCollection(item,{
      refetch: [DASHBOARD_COLLECTIONS_NEW_USER_COLLECTIONS_QUERY, 'DashboardCollectionsNewUserQuery']
    })
  }

  const { init, process, categorize, filterProps } = useNewCollectionFilter()
  init()
  
  const allUserCollections = useQueryAll<IUserCollectionModel>('allUserCollections', DASHBOARD_COLLECTIONS_NEW_USER_COLLECTIONS_QUERY)
  const allSearchCollections = useQueryAll<ISearchCollectionModel>('allSearchCollections', DASHBOARD_COLLECTIONS_NEW_ALL_SEARCH_QUERY)
  // para tener una carga previa
  const searchCollectionsPage = usePageQuery<ISearchCollectionModel>('searchCollectionsPage', DASHBOARD_COLLECTIONS_NEW_SEARCH_PAGE_QUERY, { per_page: 200 })

  useEffect(() => {
    if (userId) {
      allUserCollections.get({ userId })
      searchCollectionsPage.get({})
      allSearchCollections.get({})
    }
  }, [userId])

  const processedProps = process(allSearchCollections?.list?.length ? allSearchCollections.list : searchCollectionsPage.list, allUserCollections?.list)
  const singleListProps = {
    pageSize: 20,
    item: CardCollectionDashboardNewCollectionCompose
  }

  const loading = allUserCollections.loading || allSearchCollections.loading || searchCollectionsPage.loading

  const emptyUserCollections = !loading && allUserCollections.list?.length === 0
  const tabCollections = { label: labels.userCollectionsTitle, to: routes.userCollections, selected: false }
  const tabNewCollection = { label: labels.newCollectionTitle, to: routes.userCollectionsJoin, selected: true }
  const headerTabs = [tabCollections, tabNewCollection]

  return (
    <DashboardView route={routes.userCollections}
      location={location}
      header={
        <>
          <BackIcon linkTo={routes.userDashboard} />
          <HeaderTabs items={headerTabs}/>
        </>}>
      { !!allSearchCollections.loading && <LinearProgress /> }
      { emptyUserCollections && <EmptyListCompose id='emptyNewCollections' /> }
      <FilterCompose
        {...filterProps}
        {...processedProps}
        loading={allSearchCollections.loading}
      >{list => {
        const categorized = categorize(list)
        return list.length ?
          <>
            { categorized.map(({ id, items }, i) => 
              (
                !!items.length &&
                  <SingleListCompose key={id}
                    title={id}
                    list={items}
                    labelNum={labelIds.numCollections}
                    onClick={item => onClickCollection(item)}
                    {...singleListProps} />
              ))}
          </>
        : <></>
      }}</FilterCompose>
      { isCollectionOpen && <PopupNewCollectionCompose />}
    </DashboardView>
  )
}

export default memo(DashboardColeccionesNuevaPage)