import { gql } from '@apollo/client';

export const GET_USER_CHANGE = gql`
fragment GetUserChange on UserChange {
  id
  stateId
  chat
}
`