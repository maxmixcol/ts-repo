import React, { FC, memo } from 'react';
import { IChildren } from 'utils/interface';
import { IconDefinition } from 'components/theme/icons';

import {
  ButtonStyled,
  CardContentStyled, CardToolbarStyled, CardToolbarWrapperStyled,
} from './styled';

export interface IProps extends IChildren {
  color?: 'success' | 'info' | ''
  title: string;
  icon?: IconDefinition;
  onClick?: () => void;
  className?: string;
}


const CardToolbarMemo: FC<IProps> = ({
  color,
  title,
  icon,
  onClick,
  className,
  children
}) => {

  return (
    <>
      <CardToolbarWrapperStyled>
        <CardToolbarStyled color={color} className={className}>
          { title }
        </CardToolbarStyled>
        { icon && <ButtonStyled icon={icon} onClick={onClick} color={color} />}
      </CardToolbarWrapperStyled>
      <CardContentStyled>
        { children }
      </CardContentStyled>
    </>
  );
};

export const CardToolbar = memo(CardToolbarMemo);
