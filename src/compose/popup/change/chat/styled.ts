import { grayColor, hexToRgb, infoColor, roseColor, successColor } from 'components/theme/constants';
import styled, { css } from 'styled-components';
import { randomColor, randomDarkColor } from 'utils/helpers';

export const ChatWrapper = styled.ul`
  font-size: 75%;
  padding: 0.5rem;
  margin: 0 0 0.8rem;
  overflow: auto;
  max-height: 50vh;

  list-style: none;
  counter-reset: item;

  background-color: ${grayColor[2]};
  border-radius: 4px;
  @media (max-width: 768px) {
    max-height: 40vh;
  }
`
export const ChatItem = styled.li`
  counter-increment: item;
  margin-bottom: 0.5rem;
  display: grid;
  grid-template-columns: 1.5rem 1fr;

  &:before {
    content: counter(item);
    background: lightblue;
    border-radius: 100%;
    color: white;
    width: 1.6em;
    text-align: center;
    display: inline-block;
    height: 1.6em;
    line-height: 1.8em;
  }
`
export const ChatWrite = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  width: 100%;
`

export const ChatBubbleWrapperStyled = styled.div`
  border-radius: 0 0.5rem 0 0.5rem;
  background-color: ${grayColor[6]};
  box-shadow: 0 1px .5px rgba(${hexToRgb(grayColor[1])},.5);
  padding: 0.2rem;
`
export const ChatNameStyled = styled.div`
  font-weight: 500;
  color: ${({ name }) => randomDarkColor(name)};
`
export const ChatDateStyled = styled.span`
  float: right;
  margin: 0 2px 0 0;
  color: ${infoColor[0]};
  position: relative;
  font-style: normal;
  font-weight: 500;
`
export const ChatContentWrapperStyled = styled.div`
  padding: 2px 0 4px 4px;
  box-sizing: border-box;
  -webkit-user-select: text;
  -moz-user-select: text;
  -ms-user-select: text;
  user-select: text;
`
export const ChatContentWrapperActionStyled = styled(ChatContentWrapperStyled)`
  color: ${roseColor[0]};
  font-style: italic;
  font-weight: 400;
`
export const ChatContentStyled = styled.span`
`