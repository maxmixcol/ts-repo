import { IStep } from "components/list/stepper/CustomStepper";
import { IChangeCardCollection } from "compose/popup/change/card/ChangeCard";
import { INumberLabel } from "compose/popup/change/ChangeCollection";
import { IChangeTotalProps } from "compose/popup/change/total/ChangeTotalCompose";
import { IResultChangeProps } from "compose/popup/change/ResultChangeCompose";
import { ICollectionModel } from "model/Collection.model";
import { IChangeChat, IUserChangeModel, ChangeAction } from "model/UserChange.model";
import { IAddress, AddressState } from "model/UserInfo.model";
import { IUserInfoExModel } from "model/UserInfoEx.model";
import { ReactNode } from "react";
import { IMappedElementMap, IMenuLink, TElement } from "utils/interface";
import { IEditContainerProps } from "components/popup/edit/EditContainer";
import { IShareContainerProps } from "components/popup/share/ShareContainer";

export interface IPopupChange extends INumberLabel {
  editing: boolean;
  elementData?: IMappedElementMap;
  address?: Partial<IAddress>;
  addressState?: AddressState;
  offered: IChangeCardCollection;
  requested: IChangeCardCollection;
  actionLinksChange: IMenuLink[];
}
export interface IPopupChangeChat {
  actionLinksChat: IMenuLink[];
  allowChat: boolean;
  chatList: IChangeChat[];
  chatMessage: string;
  updateChat: (text: string) => void;
  refreshChat: (userChange: IUserChangeModel) => void;
}
export interface ITotalLink extends IMenuLink {
  action: ChangeAction;
}
export type TActionLinkResult = (props: IResultChangeProps) => ReactNode;
export interface IResult {
  name: string;
  user: IUserInfoExModel;
  stepList: IStep[];
  stepNum: number;
  showProgress: boolean;
  showUserData: boolean;
  total: IChangeTotalProps;
  saving: boolean;
  actionLinksTotal: IMenuLink[];
}

export type IPopupChangeResult = IResult & IPopupChange & IPopupChangeChat;


export interface IPopupUserResult {
  values: any;
  showProgress: boolean;
  name: string;
  image: string;
  iduser: number;
  doLike: () => void;
}

export interface IPopupCollectionEditResult {
  loadingCollectionData: boolean;
  items?: TElement[];
  showProgress: boolean;
  editContainerProps: IEditContainerProps;
}

export interface IPopupCollectionShareResult {
  collection: ICollectionModel;
  loadingCollectionData: boolean;
  items?: TElement[];
  handleImage: (img: string) => void;
  url: string;
  text: string;
  shareContainerProps: Omit<IShareContainerProps, 'ref'>;
}

interface ICollectionActions {
  edit: () => void;
  changesInProgress: () => void;
  changesCompleted: () => void;
  changesCancelled: () => void;
  fav: () => void;
  delete: () => void;
  index: () => void;
  sleep: () => void;
  share: () => void;
}
export interface IPopupCollectionResult {
  collection: ICollectionModel;
  showProgress: boolean;
  actions: ICollectionActions;
  values: any;
  refreshing: boolean;
}