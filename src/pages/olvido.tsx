import React, { useEffect } from 'react'
import IndexPage from 'pages'
import { useModalAuth } from 'use/modal/useModal'
import useAuth from 'use/auth/useAuth'
import { AUTH_TAB } from 'components/popup/auth/PopupAuth'

const OlvidoPage = ({ location }) => { 
  const { logout } = useAuth()
  const { open: openAuth } = useModalAuth()
  useEffect(() => {
    logout()
    openAuth()
  }, [])
  return (
    <IndexPage location={location} defaultTab={AUTH_TAB.RECOVER} />
  )
}

export default OlvidoPage