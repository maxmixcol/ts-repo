import styled, { css } from 'styled-components';
import { Button } from 'components/button/custom/styled';
import { CardToolbar } from 'components/card/CardToolbar';
import { Card, Container, Grid } from 'components/core';
import { grayColor, infoColor, roseColor, whiteColor } from 'components/theme/constants';
import icons from 'components/theme/icons';
import shadows from 'components/theme/shadows';
import { Icon } from 'components/styled';
import { Drawer } from 'components/drawer/Drawer';
import { Album } from './Album';
import { Textarea } from 'components/form/styled';
import { CardActions } from 'components/card/custom/CardActions';
import { TabPanel } from '@material-ui/lab';
import { ChatBubble } from './chat/ChatBubble';
import { BottomNavigationAction } from '@material-ui/core';
import { CustomStepper } from 'components/list/stepper/CustomStepper';
import { TextDefault } from 'components/typography';
import { ViewModeSelector } from './ViewModeSelector';
import { ElementBox } from '../../../components/popup/change/element/ElementBox';
import { UserDataButton } from 'components/card/custom/styled';

const errorColor = '#f42';

export const AddressCardStyled = styled(Card)`
  font-size: 0.75rem;
  padding: 0.5rem;
  color: ${grayColor[7]};
  background-color: ${grayColor[2]};

  & span:first-of-type {
    margin-top: 0;
    font-weight: bold;
  }
  & span {
    margin-top: 0.5rem;
    display: block;
  }

  & br {
    display: block;
    margin-top: 0.5em;
  }
  & img {
    position: absolute;
    right: 1.5rem;
  }
`
export const ActionLinksWrapperStyled = styled.div`
  clear: both;
  display: inline-block;
`
export const AlbumStyled = styled(Album)`
  display: flex;
  width: 100%;
  list-style: none;
  margin: 0;
  padding: 0;
  flex-wrap: wrap;

  &.selected {
    li > div {
      background-color: ${infoColor[6]};
    }
  }
`

export const AlbumThreeRowStyled = styled(AlbumStyled).attrs(props => ({
  item: ElementBox,
}))`
  display: grid;

  grid-template-columns: repeat(3, 1fr);
  grid-column-gap: 3px;
  grid-row-gap: 3px;

  @media (max-width: 540px) {
    grid-template-columns: repeat(2, 1fr);
  }
  max-height: 13rem;
  overflow: auto;
  width: 100%;
`
export const AlbumBigCardStyled = styled(AlbumStyled).attrs(props => ({
  item: ElementBox,
}))`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  & li {
    width: 4rem;
  }
`
export const Block = styled(Grid).attrs(props => ({
  item: true,
  sm: 12,
  md: 6
}))`
  width: 100%;
`
export const BlockLarge = styled(Grid).attrs(props => ({
  item: true,
  md: 12,
  lg: 6
}))`
  width: 100%;
`
export const BlockUser = styled(Block)`
  width: 100%;
`
export const BottomNavigationActionStyled = styled(BottomNavigationAction)`
  background-color: ${grayColor[2]};
`

export const ButtonAvatarStyled = styled.span`
  position: relative;
  height: 2.5rem;
  width: 2.5rem;
  margin: 0 0.5rem 0 0;
`


export const ButtonEdit = styled(Button).attrs(props => ({
  color: 'rose',
  icon: icons.edit
}))`
`
export const ButtonRandom = styled(Button).attrs(props => ({
  color: 'rose'
}))`
`
export const ChatBubbleStyled = styled(ChatBubble)`
  position: absolute;
  top: 0;
  right: 0;
  width: 10rem;
  font-size: 70%;
  opacity: 0.5;
`
export const CardActionsStyled = styled(CardActions)`
  ${({ variant }) => variant === 'raw'
    && css`
      border: 0;
      box-shadow: none;
      > div {
        padding: 0 0.5rem 0.5rem;
      }
    `}
`
export const CardActionsTotalStyled = styled(CardActionsStyled)`
  background: transparent;
  color: ${whiteColor};
`

export const CardToolbarStyled = styled(CardToolbar)`
  height: 2rem;
  margin-top: 0.5rem;
  margin-bottom: 0.5rem;
  min-height: 2rem;
  font-size: 0.7rem;
  font-weight: bold;
  display: flex;
  justify-content: space-between;
  width: 100%;
`


export const CustomStepperStyled = styled(CustomStepper)`
  background: ${grayColor[0]};
  padding: 0.25rem 0;
  border-bottom: 1px solid ${grayColor[1]};

  position: absolute;
  top: 0;
  left: 0;
  width: 100%;

  .MuiTypography-body2 {
    font-size: 0.7rem;
  }
`
export const CustomStepperBlockStyled = styled.div`
  height: 2rem;
  width: 100%;
`

export const DrawerBottom = styled(Drawer).attrs(props => ({
  variant: 'permanent',
  anchor: 'bottom',
  open: true
}))`
  opacity: 1;
  .h-drawerDark {
    padding: 0rem;

    ${({ theme }) => css`
      ${theme.breakpoints.up('sm')} {
        & > div {
          flex-direction: row;
        }
      }
    `}
  }
  .MuiDrawer-paper {
    border-top: 0;
  }
`

export const IntroWrapper = styled.div`
  width: 100%;
  padding: 1.5rem;
  background: ${infoColor[5]};
  font-size: 0.7rem;
  color: ${grayColor[1]};
`
export const MaxiListWrapper = styled.div`
  display: block;
  & li {
    display: grid;
  }
`
export const MessageIcon = styled(Icon)
  .attrs(props => ({
    icon: icons.help,
    size: '2x',
  }))`
  position: absolute;
  right: 2.5rem;
  top: -2.4rem;
  z-index: 9999;
  color: ${infoColor[7]};
`


export const NumberBig = styled.span`
  font-size: 1.2rem;
  font-weight: bold;
  line-height: 1rem;
`
export const NumberSmall = styled.span`
  line-height: 1rem;
`
export const NumberWrapper = styled.span`
  float: right;
  width: auto;
  color: white;
  padding: 0.25rem 0.375rem;
  display: flex;
  align-items: center;
  flex-flow: row;
  font-size: 0.75rem;
  font-weight: normal;

  border-radius: 5px;
  box-shadow: ${shadows.shadowDefault};
  background-color: ${({ error }) => error ? errorColor : grayColor[18]};

  & svg {
    margin: 0 0.25rem 0 0.1rem;
  }
`
export const NumberWrapperTotal = styled(NumberWrapper)`
  padding: 0.5rem;

  & span {
    font-size: 1.5rem;
  }
`
export const TabWrapperSmallStyled = styled.div`
  display: none;
  @media (min-width: 768px) {
    display: block;
  }
`
export const TabWrapperBigStyled = styled.div`
  display: none;
  @media (min-width: 768px) {
    display: block;
  }
`
export const TabPanelStyled = styled(TabPanel)`
  padding: 0.5rem;
  width: 100%;
`
export const TextareaStyled = styled(Textarea)`
  label {
    font-size: 0.9rem;
  }
  .MuiInputBase-root {
    padding: 0.5rem;
  }
`
export const TextResult = styled(TextDefault)`
  font-size: 0.9rem;
  padding-bottom: 0.5rem;
`
export const TotalButton = styled(Button).attrs(props => ({
  color: 'primary'
}))`
  float: right;
`
export const TotalError = styled.p`
  color: ${whiteColor};
  font-size: 0.6rem;
  font-weight: bold;

  border-radius: 4px;
  line-height: 1rem;
`
export const ToolbarLabelStyled = styled.span`
  font-size: 0.7rem;
  display: block;
`

export const UserDataCard = styled.div`
  flex: 1;
  width: 100%;
`
export const UserDataWrapper = styled(Card)`
  width: 100%;
  display: flex;
  box-shadow: none;

  @media (min-width: 768px) {
    flex-flow: column;
    align-items: center;
  }
`
export const ViewModeSelectorStyled = styled(ViewModeSelector)`
  position: absolute;
  right: 1rem;
  padding: 0.5rem 0.6rem;
  margin-top: -0.75rem;
  min-width: 2.25rem;
  border: 1px solid ${roseColor[0]};
`
export const ChangeWrapperFullStyled = styled(Container)`
  margin-bottom: 8rem;
  padding: 1rem;
`



export const AddressButton = styled(UserDataButton).attrs(props => ({
  icon: icons.address
}))`
`
export const ButtonChat = styled(UserDataButton).attrs(props => ({
  iconRight: icons.chat
}))`
  margin-left: 0.5rem;
`