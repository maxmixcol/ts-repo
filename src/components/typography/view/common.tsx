import React, { FC } from 'react'
import { ILayout, Layout } from 'components/layout/Layout'
import { IChildrenOpt } from 'utils/interface'
import { PublicHeaderCompose } from 'compose/header/PublicHeaderCompose'

interface IProps extends IChildrenOpt {
  variant?: ILayout;
}
export const EmptyView: FC<IProps> = ({
  variant = ILayout.PLAIN,
  children
 }) => {

  return (
    <>
      <PublicHeaderCompose />
      <Layout variant={variant}>
        { children }
      </Layout>
    </>
  )
}