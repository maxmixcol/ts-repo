export interface IUserElementModel {
  id: number; // id
  iduser: number; // iduser
  code: number; // CodigoC
  order: string; // Orden
  state: string; // Estado
  date: string; // FechaAlta
  type: string; // Tipo
  index: number; // IdEU
  changeId: number; // IdCamb
  changeState: string; // EstadoCamb
}

export interface IUserElementModelEx extends IUserElementModel {
  amount: number;
}

export enum IUserElementModelTypeEnum {
  NONE,
  FAULT,
  REPEATED,
}
export default class UserElementModel {

/*
  Elemuser
  {
    "id":100051663,
    "iduser":21,
    "CodigoC":214,
    "Orden":"107",
    "IdEU":1,
    "FechaAlta":"2018-11-09 12:24:51",
    "Tipo":"R",
    "Estado":"",
    "IdCamb":0,
    "EstadoCamb":"",
    "Foto":""
  }
*/
  static isRepeated = (userElement: IUserElementModel) => userElement.type === 'R'
  static isFault = (userElement: IUserElementModel) => userElement.type === 'N'
  static getTypeVariant = (type: string): IUserElementModelTypeEnum => ({
    N: IUserElementModelTypeEnum.FAULT,
    R: IUserElementModelTypeEnum.REPEATED,
    '':  IUserElementModelTypeEnum.NONE
  }[type])
}