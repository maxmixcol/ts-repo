---
slug: "/cookies-mas-info"
date: "2021-01-01"
title: "Más información sobre las cookies"
---
### ¿Qué es una cookie?
Una cookie es un fichero de texto inofensivo que se almacena en su navegador cuando visita casi cualquier página web. La utilidad de la cookie es que la web sea capaz de recordar su visita cuando vuelva a navegar por esa página. Aunque mucha gente no lo sabe las cookies se llevan utilizando desde hace 20 años, cuando aparecieron los primeros navegadores para la World Wide Web.

### ¿Qué NO ES una cookie?
No es un virus, ni un troyano, ni un gusano, ni spam, ni spyware, ni abre ventanas pop-up.

### ¿Qué información almacena una cookie?
Las cookies no suelen almacenar información sensible sobre usted, como tarjetas de crédito o datos bancarios, fotografías, su DNI o información personal, etc. Los datos que guardan son de carácter técnico, preferencias personales, personalización de contenidos, etc.

El servidor web no le asocia a usted como persona si no a su navegador web. De hecho, si usted navega habitualmente con Internet Explorer y prueba a navegar por la misma web con Firefox o Chrome verá que la web no se da cuenta que es usted la misma persona porque en realidad está asociando al navegador, no a la persona.

### ¿Qué tipo de cookies existen?
Cookies técnicas: Son las más elementales y permiten, entre otras cosas, saber cuándo está navegando un humano o una aplicación automatizada, cuándo navega un usuario anónimo y uno registrado, tareas básicas para el funcionamiento de cualquier web dinámica.
Cookies de análisis: Recogen información sobre el tipo de navegación que está realizando, las secciones que más utiliza, productos consultados, franja horaria de uso, idioma, etc.
Cookies publicitarias: Muestran publicidad en función de su navegación, su país de procedencia, idioma, etc.

### ¿Qué son las cookies propias y las de terceros?
Las cookies propias son las generadas por la página que está visitando y las de terceros son las generadas por servicios o proveedores externos como Facebook, Twitter, Google, etc.

### ¿Qué ocurre si desactivo las cookies?
Para que entienda el alcance que puede tener desactivar las cookies le mostramos unos ejemplos:

No podrá compartir contenidos de esa web en Facebook, Twitter o cualquier otra red social.
El sitio web no podrá adaptar los contenidos a sus preferencias personales, como suele ocurrir en las tiendas online.
No podrá acceder al área personal de esa web, como por ejemplo Mi cuenta, o Mi perfil o Mis pedidos.
Tiendas online: Le será imposible realizar compras online, tendrán que ser telefónicas o visitando la tienda física si es que dispone de ella.
No será posible personalizar sus preferencias geográficas como franja horaria, divisa o idioma.
El sitio web no podrá realizar analíticas web sobre visitantes y tráfico en la web, lo que dificultará que la web sea competitiva.
No podrá escribir en el blog, no podrá subir fotos, publicar comentarios, valorar o puntuar contenidos. La web tampoco podrá saber si usted es un humano o una aplicación automatizada que publica spam.
No se podrá mostrar publicidad sectorizada, lo que reducirá los ingresos publicitarios de la web.
Todas las redes sociales usan cookies, si las desactiva no podrá utilizar ninguna red social.
¿Se pueden eliminar las cookies?
Sí. No sólo eliminar, también bloquear, de forma general o particular para un dominio específico.

Para eliminar las cookies de un sitio web debe ir a la configuración de su navegador y allí podrá buscar las asociadas al dominio en cuestión y proceder a su eliminación.

### Configuración de cookies para los navegadores más polulares
Nota: estos pasos pueden variar en función de la versión del navegador

* **Chrome**:
    - Vaya a Configuración o Preferencias mediante el menú Archivo o bien pinchando el icono de personalización que aparece arriba a la derecha.
    - Verá diferentes secciones, pinche la opción Mostrar opciones avanzadas.
    - Vaya a Privacidad, Configuración de contenido.
    - Seleccione Todas las cookies y los datos de sitios.
    - Aparecerá un listado con todas las cookies ordenadas por dominio. Para que le sea más fácil encontrar las cookies de un determinado dominio introduzca parcial o totalmente la dirección en el campo Buscar cookies.
    - Tras realizar este filtro aparecerán en pantalla una o varias líneas con las cookies de la web solicitada. Ahora sólo tiene que seleccionarla y pulsar la X para proceder a su eliminación.


* **Internet Explorer**: 
    - Vaya a Herramientas, Opciones de Internet
    - Haga click en Privacidad.
    - Mueva el deslizador hasta ajustar el nivel de privacidad que desee.


* **Firefox**:
    - Vaya a Opciones o Preferencias según su sistema operativo.
    - Haga click en Privacidad.
    - En Historial elija Usar una configuración personalizada para el historial.
    - Ahora verá la opción Aceptar cookies, puede activarla o desactivarla según sus preferencias.


* **Safari para OSX**:
    - Vaya a Preferencias, luego Privacidad.
    - En este lugar verá la opción Bloquear cookies para que ajuste el tipo de bloqueo que desea realizar.

* **Safari para iOS**:
    - Vaya a Ajustes, luego Safari.
    - Vaya a Privacidad y Seguridad, verá la opción Bloquear cookies para que ajuste el tipo de bloqueo que desea realizar.


* **navegador para dispositivos Android**:
    - Ejecute el navegador y pulse la tecla Menú, luego Ajustes.
    - Vaya a Seguridad y Privacidad, verá la opción Aceptar cookies para que active o desactive la casilla.


* **navegador para dispositivos Windows Phone**:
    - Abra Internet Explorer, luego Más, luego Configuración
    - Ahora puede activar o desactivar la casilla Permitir cookies.