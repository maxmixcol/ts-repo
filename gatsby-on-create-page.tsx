import React from 'react'
import * as fpath from 'path'
import ApolloClient from 'apollo-client'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { HttpLink } from 'apollo-link-http'
import fetch from 'cross-fetch'

/*
const apolloClient = new ApolloClient({
  cache: new InMemoryCache(),
  ssrMode: true,
  link: new HttpLink({
    uri: "https://acabaramoscom.globat.com/ssr/graphql/public/index.php",
    fetch
  }),
})
*/

export const onCreatePage = process.env.NODE_ENV === 'development' 
  ? () => {}
  : async (
  props
) => {
  const { page, actions, cache } = props
  const { path, component } = page
  const { createPage } = actions

  //const comp = fpath.resolve('./src/gatsby/node.jsx')
  //const client = createApolloClient()

  const url = fpath.parse(component).name

  //const data = await getDataFromTree(component)


  //console.log('==> CREATE PAGE result', result);
  //console.log('state', client.extract());
  /*
  apolloClient
    .query({
      query: COLLECTIONS_QUERY,
      variables: {
        page: 0,
        per_page: 5,
        direction: 'ASC'
      }
    })
    .then((result) => {
      console.log('===>===< result oncreate', result); 
      console.log('srtate', apolloClient.extract());
    })
*/

    // console.log('CREATE PAGE END');
    /*
  const result = await gql(COLLECTIONS_QUERY, {
    page: 0,
    per_page: 5,
    direction: 'ASC'
  })
  console.log('result oncreate', result); 
  */
  

/*
  if (result.errors) {
    throw result.errors;
  }

  if (!result.data) {
    throw new Error('ERROR: Could not fetch posts on build');
  }

  // Create blog posts pages.
  const posts = result.data.allMarkdownRemark.edges;
  */
  //console.log('result', result);
  

  createPage({
    path,
    component,
    context: {
      url, //component.split('/').pop()?.split('.'), 
//      store: store.getState()
    }
  })
}
