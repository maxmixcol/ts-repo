export const getUser = (state:any) => {
  return state.auth.user;
}

export const getToken = (state:any) => {
  return state.auth.token;
}
