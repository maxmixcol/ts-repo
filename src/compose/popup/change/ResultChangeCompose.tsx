import React, { FC, useState } from 'react';
import { TextareaStyled, TextResult } from './styled';
import { ChangeAction } from 'model/UserChange.model';
import useI18n from 'use/i18n/useI18n';
import { FormRadioGroup } from 'components/form/FormRadioGroup';
import { ChatWrite } from './chat/styled';

const labelIds = {
  checkNotReceived: 'popup.change.result.checkNotReceived',
  checkReceivedWrong: 'popup.change.result.checkReceivedWrong',
  checkReceived: 'popup.change.result.checkReceived',
  comment: 'popup.change.result.comment',
}

const {
  RECEIVED, NOT_RECEIVED, RECEIVED_WRONG
} = ChangeAction

export interface IResultChangeProps {
  resultAction: ChangeAction;
  resultText: string;
  selectedResultChecks?: () => ChangeAction;
  onChangeResultChecks?: (e: ChangeAction) => void;
  onChangeResultMessage: (e: ChangeAction) => void;
}
export const ResultChangeCompose: FC<IResultChangeProps> = ({
  resultAction,
  selectedResultChecks,
  resultText,
  onChangeResultChecks,
  onChangeResultMessage
}) => {
  const labels = useI18n().tm(labelIds)
  const [ received, setReceived ] = useState<ChangeAction>(selectedResultChecks())
  const [ message, setMessage ] = useState<string>('')

  const showChecks = resultAction === RECEIVED

  return (
    <>
      <TextResult>{ resultText }</TextResult>
    {showChecks && 
      <FormRadioGroup
        selected={received}
        items={[
          { key: RECEIVED, label: labels.checkReceived },
          { key: RECEIVED_WRONG, label: labels.checkReceivedWrong },
          { key: NOT_RECEIVED, label: labels.checkNotReceived },
        ]}
        onChange={e => {
          setReceived(e)
          onChangeResultChecks(e)
        }} />}
      <ChatWrite>
        <TextareaStyled
          value={message}
          label={labels.comment}
          onChange={e => {
            setMessage(e.target.value)
            onChangeResultMessage(e.target.value)
          }} />
      </ChatWrite>
    </>
  );
};

