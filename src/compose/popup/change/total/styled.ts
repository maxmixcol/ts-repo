import styled, { css } from 'styled-components';
import { ChangeNum } from 'compose/popup/change/ChangeNum';
import { TextStyled } from 'components/common/common.styled';
import { grayColor } from 'components/theme/constants';

export const ChangeNumStyled = styled(ChangeNum)`
  margin-left: 0.5rem;
`

export const ChangeTotalStyled = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  align-items: center;
  padding: 0.5rem;
  & button {
    width: auto;
  }
`
export const ChangeTotalTitleStyled = styled.span`
  display: inline-flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  font-size: 0.75rem;
  margin: 0 1rem;
`
export const ChangeTotalsButtonsWrapperStyled = styled.div`
  display: flex;
  flex-direction: row;

`
export const ChangeTotalsWrapperStyled = styled.div`
  display: flex;
  flex-direction: row;
`
export const ChangeTitle = styled(TextStyled)`
  display: block;
  font-size: 0.8rem;
  font-weight: bold;
  width: 100%;
  border-bottom: 1px solid ${grayColor[1]};
  margin: 0.5rem 0;
`