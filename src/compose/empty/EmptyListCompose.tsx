import React, { FC } from 'react'
import icons from 'components/theme/icons'
import useI18n from 'use/i18n/useI18n'
import { CustomStepper } from 'components/list/stepper/CustomStepper'
import { MessageBoxStyled } from './dashboard/styled'
import { routes } from 'setup/constants'
import { empty } from './list/common'


interface IProps {
  id: string;
}


export const EmptyListCompose: FC<IProps> = ({
  id,
}) => {
  const { t } = useI18n()
  const { href, icon, content, pre } = {
    emptyChanges: empty.changes,
    emptyUserElements: {
      href: routes.userCollections,
      icon: icons.userCollections,
      content: '<b>¿Has añadido repetidos y faltas?</b><br>EDITA tus colecciones para añadir tus repetidos y faltas. Así podrás encontrar cambios con el resto de coleccionistas de <i>Acabáramos</i>.',
      label: 'pages.dashboard/colecciones.title'
    },
    emptyCollections: empty.collections,
    emptyNewCollections: empty.newCollections,
    emptySearchChanges: empty.searchChanges
  }[id] || {}

  const steps = [{
    label: t(`${pre}.step`),
    button: t(`${pre}.button`),
    icon,
    content,
    href 
  }]
  return (
    <MessageBoxStyled
      variant='card'>
      <CustomStepper variant='vertical' steps={steps} step={0} />
    </MessageBoxStyled>
  )
}
