import React, { FC, useState } from 'react'
import { Title } from 'components/common/common'
import { ListHorizontal, ListVertical, TextStyled } from 'components/common/common.styled'
import { LoadMoreButton } from '../../components/filter/common'
import useI18n from 'use/i18n/useI18n'


interface IServerList {
  title?: string;
  list: any[];
  pageSize: number;
  onClick?: (e: any) => void;
  linkTo?: (item: any) => string;
  item: React.FC<any>;
  labelNum: string;
  variant?: 'horizontal' | 'vertical';
  total: number;
  fetchMore: () => void | undefined;
  loading: boolean;
}

const labelIds = {
  loadMore: 'filter.loadmore',
  show: 'filter.show'
}

export const ServerListCompose: FC<IServerList> = ({
  title = '',
  list,
  loading,
  onClick,
  linkTo,
  item,
  labelNum,
  variant = 'horizontal',
  total,
  fetchMore
}) => {
  const { t, tm } = useI18n()
  const labels = tm(labelIds)
  const show = list.length

  const List = {
    horizontal: ListHorizontal,
    vertical: ListVertical
  }[variant]

  const totalLabel = (total: number) => t(labelNum, { num: total })
  const fullText = (show: number, total: number) => total > 0 ? t(labels.show, { show, total: totalLabel(total) }) : ''

  return (
  <>
    { title && <Title value={title} />}
    { (show && total) ? <TextStyled smallText>{show && show < total && fullText(show, total)}</TextStyled> : '' }
    <List
      list={list}
      onClick={onClick}
      linkTo={linkTo}
      item={item}>
      { !loading && fetchMore && <LoadMoreButton onClick={fetchMore}>{labels.loadMore}</LoadMoreButton>}

    </List>
  </>
  )
}