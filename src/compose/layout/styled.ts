import { Backdrop } from '@material-ui/core';
import styled from 'styled-components';


export const BackdropStyled = styled(Backdrop)`
  z-index: 99999;
`
