import React from 'react'
import { routes } from 'setup/constants'

import useI18n from 'use/i18n/useI18n'
import { HeaderLogo, HeaderMenuAndUser } from 'components/common/common'
import { HeaderTitle } from 'components/common/common.styled'
import { NavLeft } from 'components/header/styled'
import { CardCategoriesCompose } from 'compose/card/index/CardCategoriesCompose'
import { DashboardView } from 'components/typography/view/dashboard/DashboardView'

const labelIds = {
  pageTitle: 'pages.dashboard/buscar.title'
}

const DashboardBuscarPage = ({ location }) => {
  const labels = useI18n().tm(labelIds)

  return (
    <DashboardView
      location={location}
      route={routes.userSearch}
      header={
        <>
          <NavLeft><HeaderLogo /></NavLeft>
          <HeaderTitle>{labels.pageTitle}</HeaderTitle>
          <HeaderMenuAndUser />
        </>
      }>
      <CardCategoriesCompose />
    </DashboardView>
  )
}
export default DashboardBuscarPage