export interface IAuthModel {
  accessGranted: boolean;
  action: string;
  data: any;
  email: string;
  errorMessage: string;
  success: string;
  userId: string;
  userName: string;
}