import React from 'react'
import { routes } from 'setup/constants'
import useI18n from 'use/i18n/useI18n'
import { BackIcon, HeaderTitle } from 'components/common/common.styled'
import { CardToolbarStyled } from 'components/card/styled'
import icons from 'components/theme/icons'
import { Main } from 'components/common/common'
import { Header } from 'components/header/Header'
import { PrivateCompose } from 'compose/private/PrivateCompose'

const labelIds = {
  pageTitle: 'pages.dashboard/perfil.title'
}

const DashboardProfilePage = ({ location }) => {
  const labels = useI18n().tm(labelIds)
  const icon = icons.edit
  return (
      <PrivateCompose>
        <Header>
          <BackIcon linkTo={routes.userDashboard} />
          <HeaderTitle>{labels.pageTitle}</HeaderTitle>
        </Header>
        <Main>
          <CardToolbarStyled title='nombre' icon={icon}>
            Nombre
          </CardToolbarStyled>
        </Main>
      </PrivateCompose>
  )
}
export default DashboardProfilePage