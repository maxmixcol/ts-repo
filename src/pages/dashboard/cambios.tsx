import React, { memo, useEffect } from 'react'

import { routes } from 'setup/constants'
import { LinearProgress } from '@material-ui/core'
import { DASHBOARD_CHANGES_QUERY } from 'graphql/query'
import { useQueryAll } from 'use/fetch/useQuery'
import useAuth from 'use/auth/useAuth'
import useChangeFilter from 'use/filter/impl/useChangeFilter'

import { DashboardView } from 'components/typography/view/dashboard/DashboardView'
import { CardChangeDashboardChangesCompose } from 'compose/card/change/CardChangeDashboardChangesCompose'
import { FilterCompose } from 'compose/filter/FilterCompose'
import useI18n from 'use/i18n/useI18n'
import { HeaderTabs } from 'components/header/types/HeaderTabs'
import { PopupChangeCompose } from 'compose/popup/PopupChangeCompose'
import { BackIcon } from 'components/common/common.styled'
import { PopupUserCompose } from 'compose/popup/PopupUserCompose'
import { EmptyListCompose } from 'compose/empty/EmptyListCompose'
import { IUserExchangeModel } from 'model/UserExchange.model'
import { useModalChange, useModalUser } from 'use/modal/useModal'
import { SingleListCompose } from 'compose/filter/SingleListCompose'

const labelIds = {
  numChanges: 'filter.numChanges',
  userChangesTitle: 'pages.dashboard/cambios.title',
	userChangeSearchTitle: 'pages.dashboard/cambios/buscar.title',
}
const DashboardCambiosPage = ({ location }) => {
// const DashboardCambiosPage = ({ location }) => {
  // const params = queryParams(location)
  const userId = useAuth().user
  const labels = useI18n().tm(labelIds)
  const { isOpen: isUserOpen } = useModalUser()
  const { isOpen: isChangeOpen, open: openChange } = useModalChange()
  
  const { init, values, process, filterProps } = useChangeFilter()
  init()

  const { state, user, collection } = values()
  
  const onClickChange = (exchange: IUserExchangeModel)  => {
    openChange({ exchange, isNew: false }, {
      refetch: [DASHBOARD_CHANGES_QUERY, 'DashboardChangesQuery']
    })
  }

  const exchangesQuery = useQueryAll<IUserExchangeModel>('allUserExchanges', DASHBOARD_CHANGES_QUERY)

  // para tener los nombres de las colecciones sin cambios en los filtros
  // cuando entramos con queryParams con code
  // const collectionsQuery = useQueryAll<IUserCollectionModel>('allUserCollections', DASHBOARD_CHANGES_ALL_COLLECTIONS_QUERY)
  const variables = { userId }

  // useEffect(() => {
  //   collectionsQuery.get(variables)
  // }, [])

  useEffect(() => {
    if (userId) {
      if (state || user || collection) {
        exchangesQuery.get({
          ...variables,
          ...state && { state },
          ...user && { otherUserId: user },
          ...collection && { codes: [collection] }
        })
      }
    }
  }, [ userId, state, user, collection ])

  const processedProps = process(exchangesQuery.list || [])
  const emptyChanges = !exchangesQuery.loading && exchangesQuery.list?.length === 0
  const tabChanges = { label: labels.userChangesTitle, to: routes.userChanges, selected: true }
  const tabSearchChanges = { label: labels.userChangeSearchTitle, to: routes.userChangeSearch, selected: false }
  const headerTabs = [tabChanges, tabSearchChanges]
  
  // const loading = collectionsQuery.loading || exchangesQuery.loading
  const loading = exchangesQuery.loading

  return (
    <DashboardView route={routes.userChanges}
      location={location}
      header={
        <>
          <BackIcon linkTo={routes.userDashboard} />
          <HeaderTabs items={headerTabs}/>
        </>}>
      { loading && <LinearProgress /> }    
      { emptyChanges && 
        <EmptyListCompose id='emptyChanges' />}
      { processedProps.list &&
        <FilterCompose
          { ...filterProps}
          { ...processedProps}
          loading={loading}
        >{list => (
          <SingleListCompose
						pageSize={10}
						labelNum={labelIds.numChanges}
						list={list}
						variant={'vertical'}
						onClick={item => onClickChange(item)}
						item={CardChangeDashboardChangesCompose}>
					</SingleListCompose>
        )}</FilterCompose>}
        { isChangeOpen && <PopupChangeCompose />}
        { isUserOpen && <PopupUserCompose />}
      </DashboardView>
  )
}
export default memo(DashboardCambiosPage)
