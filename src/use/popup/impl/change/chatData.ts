import { useEffect, useState } from 'react';
import UserChangeModel, { ChangeStateIndex } from 'model/UserChange.model';
import icons from 'components/theme/icons';
import { IPopupChangeChat } from 'compose/popup/interface';

const labelIds = {
  buttonChat: 'popup.change.button.chat'
}

const {
  NEW, INIT, ACCEPTED, RECEIVED, NOT_RECEIVED, FINISHED
} = ChangeStateIndex
const allowedChatStates = [INIT, ACCEPTED, RECEIVED, NOT_RECEIVED]

interface IPopupChatInput {
  state: ChangeStateIndex,
  chat: string,
  saveChat?: (msg: string) => void,
  changeId?: number
}
const chatData = (
  tm: any,
  { state, chat, saveChat, changeId }: IPopupChatInput
): IPopupChangeChat => {
  const labels = tm(labelIds)
  const [ chatMessage, setChatMessage ] = useState('')
  const [ fullChat, setFullChat ] = useState(chat)
  const [ disabledChat, disableChat ] = useState(true)
  const updateChat = (text) => {
    disableChat(text.length === 0)
    setChatMessage(text)
  }

  const allowChat = allowedChatStates.indexOf(state) !== -1

  useEffect(() => {
    updateChat('')
  }, [ changeId ])

  return {
    allowChat,
    actionLinksChat: allowChat ? [
      { icon: icons.chat, disabled: disabledChat, text: labels.buttonChat, onClick: () => {
        saveChat && saveChat(chatMessage)
        updateChat('')
      }},
    ] : [],
    chatMessage,
    chatList: fullChat.split('<br>').map(item => UserChangeModel.parseChat(item)),
    refreshChat: (userChange) => {
      setFullChat(userChange.chat)
    },
    updateChat,
  }
}

export default chatData
