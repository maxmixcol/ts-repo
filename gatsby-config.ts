import fs from 'fs'
import { buildSchema } from 'graphql'

export default {
  flags: { PRESERVE_WEBPACK_CACHE: true },
  pathPrefix: `/latest`,
  // pathPrefix: `/dev/latest`,

  siteMetadata: {
    title: `Acabáramos`,
    description: "ACABARAMOS.COM es la herramienta definitiva para coleccionistas. Almacena colecciones ILIMITADAS GRATIS, encuentra los MEJORES CAMBIOS de forma SENCILLA, PUBLICA tu colección en FACEBOOK o TWITTER con un CLICK, CHATEA con otros coleccionistas. ACABARAMOS.COM, la herramienta definitiva para coleccionistas.",
    siteUrl: `https://acabaramos.com/`,
  },
  plugins: [
    'gatsby-plugin-sass',
    'gatsby-plugin-styled-components',
    {
      resolve: `gatsby-plugin-sharp`,
      options: {
        icon: `src/assets/images/logo.png` //favicon
      }
    },
    `gatsby-plugin-image`,
    `gatsby-plugin-sharp`,
    `gatsby-transformer-sharp`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/src/markdown-pages`,
        name: `markdown-pages`,
      },
    },
    `gatsby-transformer-remark`,
    /*
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/content/assets`,
        name: `assets`,
      },
    },

    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: 590,
            },
          },
          {
            resolve: `gatsby-remark-responsive-iframe`,
            options: {
              wrapperStyle: `margin-bottom: 1.0725rem`,
            },
          },
          `gatsby-remark-prismjs`,
          `gatsby-remark-copy-linked-files`,
          `gatsby-remark-smartypants`,
        ],
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-feed`,
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        //trackingId: `ADD YOUR TRACKING ID HERE`,
      },
    },
        */
    `gatsby-plugin-layouts`,    
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Gatsby Starter Blog`,
        short_name: `GatsbyJS`,
        start_url: `/`,
        background_color: `#ffffff`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `content/assets/gatsby-icon.png`,
      },
    },
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-resolve-src`,
    {
      resolve: `gatsby-plugin-alias-imports`,
      options: {
        alias: {
          "assets": "src/assets",
          "components": "src/components",
          "config": "src/config",
          "images": "src/images",
          "utils": "src/utils",
        }
      }
    },
    /*
    {
      resolve: `gatsby-plugin-typography`,
      options: {
        pathToConfigModule: `src/utils/typography`,
      },
    },
    */
    `gatsby-plugin-typescript`,
    {
      resolve: `gatsby-source-graphql`,
      options: {
        fieldName: `api`,
        url: `http://acabaramoscom.globat.com/dev/graphql-local.php`,
        typeName: `API`,
        createSchema: async () => {
          const schema = fs.readFileSync(`${__dirname}/src/graphql/schema.graphql`).toString()        
          return buildSchema(schema)
        }
      }
    }
  ],
};
