import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';
import createSagaMiddleware from 'redux-saga';
// import logger from 'redux-logger';
import { fork } from 'redux-saga/effects';
import watchFetchImage from './src/compose/image/saga';
import { isProduction, isNotProduction } from './src/utils/mode';


import imageReducer from './src/compose/image/reducer';
import adminReducer from './src/stores/admin/table';

export default ({ initState = {}, rest = {} } = {}) => {
  const middleware = getDefaultMiddleware({
    immutableCheck: true,
    serializableCheck: true,
    thunk: false,
  });

  const sagaMiddleware = createSagaMiddleware();

  const store = configureStore({
    reducer: {
      imageReducer,
      adminReducer
    },
    devTools: isNotProduction(),
    middleware: isProduction()
      ? middleware.concat([
          sagaMiddleware
      ])
      : middleware.concat([
        //logger,
          sagaMiddleware
      ]),
    preloadedState: initState
  });

  function* sagas() {
    yield fork(watchFetchImage, rest);
  }

  const rootSaga = sagaMiddleware.run(sagas);

  return { store, rootSaga };
};

/*
export const store = configureStore({
  reducer: {
    adminTable,
  },
});
*/