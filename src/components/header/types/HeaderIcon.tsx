import React, { FC, memo, SyntheticEvent } from 'react'
import {
  HeaderAvatarIcon,
  IconButtonStyled,
} from '../styled'
import { IHeaderType } from 'components/header/Header'
import { Icon } from 'components/styled'

interface IIcon {
  size: string;
  onClick?: (_?: SyntheticEvent) => void;
}
const HeaderIconMemo: FC<IHeaderType & IIcon> = ({
  value,
  size,
  onClick,
}) => {
  return (
    <HeaderAvatarIcon>
      <IconButtonStyled onClick={onClick} color="inherit"><Icon icon={value} size={size} /></IconButtonStyled>
    </HeaderAvatarIcon>
  )
}

export const HeaderIcon = memo(HeaderIconMemo)