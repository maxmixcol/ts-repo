import { AlbumWrapper } from 'components/album/AlbumWrapper';
import { ShareContainerStyled, ShareTitleStyled, AlbumShareStyled, AlbumShareTextStyled } from 'components/popup/share/styled';
import { IAlbumProps } from 'compose/popup/change/Album';
import { IElementModel } from 'model/Element.model';
import UserCollectionExModel from 'model/UserCollectionEx.model';
import React, { FC } from 'react';

export interface IShareContainerProps {
  labels: {
    title: string;
    footer: string;
    faults: string;
    repeated: string; 
  };
  elements: IElementModel[];
  faults: IElementModel[];
  repeated: IElementModel[];
  albumProps: Omit<IAlbumProps, 'items'>
}

export const ShareContainer: FC<IShareContainerProps> = ({
  labels,
  elements,
  faults,
  repeated,
  albumProps,
}) => {
  const paginate = UserCollectionExModel.paginateWithGroups
  return (
    <ShareContainerStyled>
      <ShareTitleStyled>{ labels.title }</ShareTitleStyled>
      <AlbumWrapper
        album={AlbumShareStyled}
        albumProps={albumProps}
        pages={paginate(elements, 12)} />
      { !!faults.length && <>
        <ShareTitleStyled>{ labels.faults }</ShareTitleStyled>
        <AlbumWrapper variant='list'
          album={AlbumShareTextStyled}
          albumProps={albumProps}
          pages={paginate(faults, 100000)} />
      </>}
      { !!repeated.length && <>
        <ShareTitleStyled>{ labels.repeated }</ShareTitleStyled>
        <AlbumWrapper variant='list'
          album={AlbumShareTextStyled}
          albumProps={albumProps}
          pages={paginate(repeated, 100000)} />
      </>}
      <ShareTitleStyled>{ labels.footer }</ShareTitleStyled>
    </ShareContainerStyled>
  );
};
