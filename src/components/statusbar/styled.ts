import styled, { css } from 'styled-components';

import { grayColor, whiteColor, appBarShadow } from 'components/theme/constants';
import { Tab, Tabs, Toolbar } from 'components/core';

export const ToolbarStyled = styled(Toolbar).attrs(props => ({
  as: 'nav'
}))`
  top: auto;
  bottom: 0;
  left: auto;
  right: 0;

  color: ${whiteColor};
  background-color: ${grayColor[9]} !important;
  box-shadow: ${appBarShadow.boxShadow};

  position: fixed;
  z-index: 1100;

  width: 100%;

  ${({ theme }) => css`
    ${theme.breakpoints.up('sm')} {
      display: none;
    }
  `}
`;

export const TabStyled = styled(Tab)`
  font-size: 0.7rem;
`;

export const TabsStyled = styled(Tabs).attrs(props => ({
  indicatorColor: "primary",
  variant: "fullWidth"
}))`
  font-size: 0.7rem;
`;
