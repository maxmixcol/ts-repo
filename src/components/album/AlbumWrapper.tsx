import React, { FC } from 'react';
import { IAlbumProps } from 'compose/popup/change/Album';
import { IElementModel } from 'model/Element.model';
import { AlbumWrapperStyled, GroupStyled, MaxiListWrapperAlbum } from './styled';

export interface IAlbumWrapperProps {
  album: React.FC<IAlbumProps>;
  albumProps: Omit<IAlbumProps, 'items'>;
  pages: IElementModel[][];
  variant?: 'list' | 'album';
}
export const AlbumWrapper: FC<IAlbumWrapperProps> = ({
  album: Album,
  albumProps,
  variant,
  pages,
}) => {
  let lastKey = ''
  return (
    (<AlbumWrapperStyled variant={variant}>
      {pages.map((page, index) => {
        const items = page.map((item) => item.order)
        const { group, type } = page[0]
        // const lastGroup = index - 1 < 0 ? undefined : pages[index - 1][0].group

        const key = `${type && type !== 'B' && type !== group
          ? type
          : group}`
        const showGroup = lastKey !== key || index % 8 === 0

        lastKey = key
        return (
          <li key={`${key}_${index}`}>
            <GroupStyled dangerouslySetInnerHTML={{ __html: showGroup ? key : '' }} />
            <MaxiListWrapperAlbum variant={variant}>
              <Album {...albumProps} items={items} />
            </MaxiListWrapperAlbum>
          </li>
        )})}
    </AlbumWrapperStyled>)
  );
};
