import React, { FC } from 'react';

import { IChildrenOpt, IMenuLink } from 'utils/interface';

import {
  BottomNavigationStyled,
  CustomActionsCardColor,
  CustomActionsCardWhite,
  CustomCardBody,
  CustomCardColorHeaderToolbar,
  CustomCardWhiteHeaderToolbar,
  UserDataButton,
} from './styled';

export type TAnchorLink = 'top' | 'bottom' | 'both';
interface IProps extends IChildrenOpt {
  background?: string;
  variant?: 'invert';
  anchorLinks?: TAnchorLink;
  color?: string;
  buttonColor?: string;
  overflow?: boolean;
  content?: React.ReactNode;
  actionLinks?: IMenuLink[];
  className?: string;
}

export const 
CardActions: FC<IProps> = ({
  background = '',
  variant,
  color = '',
  buttonColor = 'primary',
  content = '',
  overflow = undefined,
  children = '',
  actionLinks = [],
  anchorLinks = 'bottom',
  className
}) => {
  const inverted = variant === 'invert'
  const Wrapper = inverted ? CustomActionsCardWhite : CustomActionsCardColor
  const Header = inverted ? CustomCardColorHeaderToolbar : CustomCardWhiteHeaderToolbar
  const Body = CustomCardBody
  const showActions = !!actionLinks.length

  const actions = (
    actionLinks.map((item, i) => (
     <UserDataButton key={i}
      color={buttonColor}
      icon={item.icon}
      disabled={item.disabled}
      onClick={item.onClick}
      variant={item.variant}
     >{item.text}</UserDataButton>
    ))
  )
  return (
    <Wrapper color={color} className={className}
      background={background}>
      <Header color={color}>
        {content}
      </Header>
      { showActions && anchorLinks !== 'bottom' &&
        <BottomNavigationStyled showLabels>{ actions }</BottomNavigationStyled>}
      { children && <Body className='card-body' overflow={overflow ? 'true' : undefined}>{children}</Body>}
      { showActions && anchorLinks !== 'top' &&
        <BottomNavigationStyled showLabels>{ actions }</BottomNavigationStyled>}
    </Wrapper>
  );
};
