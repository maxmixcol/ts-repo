import { ICollectionModel } from "./Collection.model";

interface ICollector {
  total: number;
  recent: number;
}
export interface ISearchCollectionModel {
  collection: ICollectionModel; //CodigoC
  collectors: ICollector;
}