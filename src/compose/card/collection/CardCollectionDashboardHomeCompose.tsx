
import React, { FC, memo } from 'react'
import { CardCollection } from 'components/card/collection/CardCollection'
import { AdditionalDataStyled } from 'components/card/collection/styled'
import { Badge } from 'components/styled'
import CollectionModel from 'model/Collection.model'
import UserCollectionModel, { IUserCollectionModel } from 'model/UserCollection.model'
import useI18n from 'use/i18n/useI18n'
import { IUserCollectionExModel } from 'model/UserCollectionEx.model'



interface IAdditionalItem {
  color: 'success' | 'danger';
  text: string;
}

const AdditionalItem: FC<IAdditionalItem> = ({ color, text }) => (
  <Badge color={color}>{text}</Badge>
)
const labelIds = [
  'pages.dashboard.numFaults',
  'pages.dashboard.numRepeated'
]
const CardCollectionDashboardHomeComposeMemo: FC<IUserCollectionExModel> = ({ 
  userCollection,
  countFaults,
  countRepeated
 }) => {
  const { t } = useI18n()
  const { collection, priority } = userCollection
  const { fav, sleep } = UserCollectionModel.parsePriority(priority)
  const numFaults = t(labelIds[0], { num: countFaults })
  const numRepeated = t(labelIds[1], { num: countRepeated })
  const { code, name } = collection
  
  const image = CollectionModel.getImage(code)

  return (
    <CardCollection
      image={image}
      name={name}
      fav={fav}
      sleep={sleep}
      select={() => {}}>
      <AdditionalDataStyled>
        <AdditionalItem color={'danger'} text={numFaults} />
        <AdditionalItem color={'success'} text={numRepeated} />
      </AdditionalDataStyled>
    </CardCollection>
  )
}


export const CardCollectionDashboardHomeCompose = memo(CardCollectionDashboardHomeComposeMemo)