import React, { FC } from 'react'

import { IChildren } from 'utils/interface'
import { IconDefinition } from 'components/theme/icons';
import { Step, StepLabel } from 'components/core'

import { Icon } from 'components/styled'
import { ButtonWithIconStyled, StepContentStyled, StepIconWrapper, StepperStyled, TextDefaultStyled } from './styled'
import icons from 'components/theme/icons'
import { CustomLink } from 'components/button/CustomLink'
import { PrimaryButton } from 'components/common/common.styled'


export interface IProps {
  steps: IStep[];
  variant?: 'horizontal' | 'vertical';
  step?: number;
  className?: string;
}

export interface IStep {
  label: string;
  icon?: IconDefinition;
  button?: string;
  href?: string;
  content?: string | React.ReactNode;
  expanded?: boolean;
  completed?: boolean;
}
export const CustomStepper: FC<IProps> = ({ 
  variant = 'horizontal',
  steps,
  step,
  className,
}) => {

  return (
    <StepperStyled className={className} nonLinear={true} activeStep={step}
      orientation={variant} connector={<></>}>
      {steps.map(({ label, icon, button, content, href = '', expanded }, index) => (
        <Step key={index}>
          <StepLabel StepIconComponent={icon && StepIcon(icon, step > index)}>{label}</StepLabel>
          { content && <StepContentStyled expanded={expanded || step === index}>
            <Content>{content}</Content>
            { href && <Button href={href} label={button} />}
          </StepContentStyled>}
        </Step>
      ))}
    </StepperStyled>
  )
}


const Content: FC<IChildren> = ({
  children
}) => (<TextDefaultStyled>{children}</TextDefaultStyled>)

interface IStepProps {
  active?: boolean;
  completed?: boolean;
}
const StepIcon = (icon: IconDefinition, done: boolean = false): FC<IStepProps> => ({ active, completed }) => (
  <StepIconWrapper active={active} completed={done || completed}><Icon icon={icon} /></StepIconWrapper>
)

interface IButtonProps {
  label: string;
  href?: string;
}
const Button: FC<IButtonProps> = ({
  href,
  label
}) => {
  const children = <ButtonWithIconStyled item={PrimaryButton} iconRight={icons.arrowRight}>{ label }</ButtonWithIconStyled>
  return (<CustomLink to={href}>{ children }</CustomLink>);
}
