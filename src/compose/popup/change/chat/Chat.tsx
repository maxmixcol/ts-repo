import React, { FC, useRef } from 'react'
import { useEffect } from 'react'
import {
  ChatItem,
  ChatWrapper,
} from 'compose/popup/change/chat/styled'
import { IChangeChat } from 'model/UserChange.model'

import { ChatBubble } from './ChatBubble'

interface IProps {
  chatList: IChangeChat[];
}
export const Chat: FC<IProps> = ({
  chatList = []
}) => {
  const ref = useRef<HTMLUListElement>()
  useEffect(() => {
    ref.current.scrollTop = ref.current.scrollHeight
  },[ chatList ] )
  return (
    <ChatWrapper ref={ref}>
      {chatList.map(({ name, date, text }, i) => 
      <ChatItem key={i} >
        <ChatBubble name={name} date={date}>{ text }</ChatBubble>
      </ChatItem>)}
    </ChatWrapper>
  )
}