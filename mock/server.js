// grab the packages we need
var express = require('express');
var cors = require('cors')

const MOCK_PATH = './mock_data';
const mockBase = require(`${MOCK_PATH}/mockBase`);

let userID;
let locale;
let USERS;

const app = express();
const port = process.env.PORT || 7071;

const corsOptions = {
    origin: function(origin, callback){
        return callback(null, true);
    },
    optionsSuccessStatus: 200,
    credentials: true
};
app.use(cors(corsOptions))

const bodyParser = require('body-parser');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

// Headers and CORS
app.use((req, res, next) => {
    res.header('Content-Type', 'application/json');
    res.header('Access-Control-Allow-Headers', 'Accept, Content-Type, tsec, consumerrequestid, authenticationchallenge, authenticationtype, authenticationdata, authenticationstate, contactid, consumeruuid, consumerapplicationid, perimetertsec, authorization, responsewarningcode, responsewarningdescription, cuf, prefer, phoneNumber, Location, X-RHO-PARENTSPANID, X-RHO-TRACEID, preferredauthenticationtype, X-HTTP-Method-Override, Access-Control-Allow-Methods, iv-remote-address, thirdparty-x-forwarded-for, thirdparty-user-agent, thirdparty-geolocation, thirdparty-calculated-scoring, thirdparty-deviceid, vnd.bbva.security-client-certificate, order-preferredaction, authenticationresponse');
    res.header('Access-Control-Expose-Headers', 'Accept, Content-Type, tsec, consumerrequestid, authenticationchallenge, authenticationtype, authenticationdata, authenticationstate, contactid, consumeruuid, consumerapplicationid, perimetertsec, authorization, responsewarningcode, responsewarningdescription, cuf, prefer, phoneNumber, Location, X-RHO-PARENTSPANID, X-RHO-TRACEID, preferredauthenticationtype, X-HTTP-Method-Override, Access-Control-Allow-Methods, iv-remote-address, thirdparty-x-forwarded-for, thirdparty-user-agent, thirdparty-geolocation, thirdparty-calculated-scoring, thirdparty-deviceid, vnd.bbva.security-client-certificate, order-preferredaction, authenticationresponse, Access-Control-Max-Age');
    res.header('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT, OPTIONS, PATCH');
    res.header('Access-Control-Max-Age', '86400');
    res.header('Connection', 'Keep-Alive');

    next();
});

const handleData = (res, data) => {
    if (data.messages && data.messages[0].type === 'ERROR') {
        res.status(400).send(data);
    } else {
        res.send(data);
    }
}


const doFilter = (params, str) => params
    .filter((item) => item
        .includes(str))
        .map(item => item
            .replace(str, '')
            .split(',')
            .slice(0, 20)
            .join('-')).join('=')

const send = (req, res, extra) => {
    const url = req.url.split('?');
    const params = url[1].split('&');
    const filter = [doFilter(params, 'filter='),doFilter(params, 'filter[]=')].filter(Boolean)
    const folder = url[0].replace(mockBase.invalidFolderCharacters, '');
    const _path = `${MOCK_PATH}/${locale}/${userID}${folder}${filter.length?('/'+filter):''}${extra || ''}.json`;
    res.header('Access-Control-Allow-Origin', req.headers.origin);

    const data = require(_path);
    data ? handleData(res, data) : res.status(403).send(mockBase.notFoundError);
}

// ##### GET 

app.get('*', function(req, res) {
    console.log('GET, req', req.url);

    send(req, res);
});

// ##### login

const auth = (req, res) => {
    let data;
    const path = (id) => `${MOCK_PATH}/mio${req.url}/${id}.json`;
    res.header('Access-Control-Allow-Origin', req.headers.origin);

    console.log('path(userID)', path(userID));
    try {
        data = require(path(userID));
    } catch (e) {
        data = require(path('default'));
    }

    console.log('data', data);
    if (data['http-status']) {
        res.status(data['http-status']).send(data);
    } else {
        res.send(data);
    }
}

app.post('/auth/login', (req, res) => {
    const reqData = JSON.parse(Object.keys(req.body)[0])
    userID = reqData.email;
    locale = 'mio';
    console.log('userID', userID);

    auth(req, res);
});

app.post('/auth/logout', (req, res) => {
    userID = null;
    auth(req, res);
})

app.post('*', (req, res) => {
    console.log('POST, req', req.url);
    console.log('POST, body', JSON.stringify(req.body, null, 2));

    send(req, res);
});

// start the server
app.listen(port);
console.log('Server started! At http://localhost:' + port);