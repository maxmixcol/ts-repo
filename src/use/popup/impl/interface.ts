import { IconDefinition } from 'components/theme/icons';
import { ICollectionModel } from 'model/Collection.model';
import { IUserExchangeModel } from "model/UserExchange.model";

export interface IPopupChangeData {
  exchange: IUserExchangeModel,
  isNew: boolean;
}
export interface IPopupCollectionData {
  code: number;
  name: string;
}
export interface IPopupCollectionEditData {
  collection: ICollectionModel;
}
export interface IPopupCollectionShareData {
  collection: ICollectionModel;
}
export interface IPopupNewCollectionData {
  code: number;
  name: string;
}
export interface IStep {
  label: string;
  icon: IconDefinition;
}

