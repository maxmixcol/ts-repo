import React, { FC, memo, useEffect, useMemo, useState} from 'react'
import useFetch from 'use/fetch/useFetch'
import useAsyncEffect from 'use-async-effect'
import { flags, nav } from 'setup/constants'
import { useQueryAll } from 'use/fetch/useQuery'
import { IAlertModel } from 'model/Alert.model'
import { DASHBOARD_ALERTS_QUERY } from 'graphql/query'
import { Modal } from 'components/modal/Modal'
import { useModalAlertWelcome } from 'use/modal/useModal'
import useStorage from 'use/storage/useStorage'
import { AlertWelcomeHeaderStyled } from 'compose/popup/alertWelcome/styled'
import { ButtonStyled, ButtonsWrapper } from 'components/message/styled'
import useI18n from 'use/i18n/useI18n'
import DateModel, { IDateFormat } from 'model/Date.model'
import environment from 'setup/environment'

const labelIds = {
  resultCancel: 'popup.change.result.buttonCancel',
}
export const PopupAlertWelcomeCompose: FC = () => {
  const { post } = useFetch()
  const flag = flags.popupAlertWelcome
  const { getFlag, setFlag } = useStorage()
  const { isOpen, open: openAlert, close } = useModalAlertWelcome()
  const [ urls, setUrls ] = useState([])
  const [ html, setHtml ] = useState('')
  const { resultCancel } = useI18n().tm(labelIds)
  const alertsQuery = useQueryAll<IAlertModel>('allAlerts', DASHBOARD_ALERTS_QUERY)
  const { title, urlContent, urlTrigger } = (alertsQuery.list && alertsQuery.list[0]) || {} 
  const day = `${getFlag<string>(flag)}`

  useEffect(() => {
    const today = DateModel.format(new Date().getTime(), IDateFormat.db)
    const diff = DateModel.dateDiffUnit(day, 'days')
    if (diff > 0) {
      alertsQuery.get({
        dev: environment.isDevelopment,
        date: today
      })
    }
    setFlag<string>(flag, today)
  }, [])

  useEffect(() => {
    if (urlTrigger) {
      setUrls([ urlTrigger, urlContent ])
    }
    if (html) {
      openAlert()
    }
  }, [urlTrigger, html])


  const ModalWrapper:FC<IWrapperProps> = useMemo(() => ({ html, title }) => {
    const onClose = () => {
      setHtml('')
      close()
    }
    return (
      <Modal open={isOpen && !!html} handleClose={onClose}>
        <AlertWelcomeHeaderStyled>{ title }</AlertWelcomeHeaderStyled>
        <p dangerouslySetInnerHTML={{ __html: html }} />
        <ButtonsWrapper>
          <ButtonStyled onClick={onClose}>{resultCancel}</ButtonStyled>
        </ButtonsWrapper>
      </Modal>
    )
  }, [html, isOpen])
  
  

  useAsyncEffect(async () => {
    if (urls.length) {
      const [ urlTrigger, urlContent ] = urls
      const { show } = await post(nav.publicAlert(urlTrigger))

      if (show) {
        const resultHtml = await post(nav.publicAlert(urlContent))
        setHtml(resultHtml)
      }
    }
  }, [urls.join()])

  return isOpen ? <ModalWrapper html={html} title={title} /> : <></>
}

interface IWrapperProps {
  html: string;
  title: string;
}
