import { useState } from 'react';
import icons from 'components/theme/icons';
import { IChangeCollectionListProps } from 'compose/popup/change/ChangeCollection';
import { ChangeAction, ChangeStateIndex } from 'model/UserChange.model';
import UserExchangeModel from 'model/UserExchange.model';
import { IApiChangeOutput } from './saveData';
import { useModalChange, useModalResultChange } from 'use/modal/useModal';
import { ResultChangeCompose } from 'compose/popup/change/ResultChangeCompose';
import { IPopupChangeResult, ITotalLink } from 'compose/popup/interface';

const labelIds = {
  buttonAccept: 'popup.change.button.accept',
  buttonCancel: 'popup.change.button.cancel',
  buttonFinish: 'popup.change.button.finish',
  buttonInit: 'popup.change.button.init',
  buttonReject: 'popup.change.button.reject',
  buttonUpdate: 'popup.change.button.update',
  buttonExit: 'popup.change.button.exit',
  resultAccept: 'popup.change.result.buttonAccept',
  resultCancel: 'popup.change.result.buttonCancel',
  totalError: 'popup.change.totalError',
  confirmSave: 'confirm.saveChange',
  yes: 'popup.confirm.yes',
  no: 'popup.confirm.no',
  warning: 'popup.change.result.warning',
  warningReceived: 'popup.change.result.warningReceived'
}
const {
  NEW, ACCEPT, CANCEL, MODIFY, REJECT, RECEIVED
} = ChangeAction

interface ITotalData extends Pick<IApiChangeOutput, 'save'> {
  state: ChangeStateIndex;
  isSelf: boolean;
  isEdited: boolean;
  offered: IChangeCollectionListProps[];
  requested: IChangeCollectionListProps[];
  userName: string;
}

const totalData = (
  t: any,
  tm: any,
  props: ITotalData,
  setSaving: Function
): Pick<IPopupChangeResult, 'actionLinksTotal' | 'total'> => {
  const labels = tm(labelIds)
  const { open: openResult, close: closeResult } = useModalResultChange()
  const { close } = useModalChange()

  const {
    state,
    isSelf,
    isEdited,
    offered,
    requested,
    userName,
    save,
  } = props

  const [ received ] = useState<{action: ChangeAction}>({action: RECEIVED})
  const [ message ] = useState<{text: string}>({text: ''})
  const totalOffered: number = offered && UserExchangeModel.parseTotal(offered)
  const totalRequested: number = requested && UserExchangeModel.parseTotal(requested)
  const disabled = totalRequested === 0

  const onClickAction = (resultAction: ChangeAction, actionLabel: string) => openResult({
    element: ResultChangeCompose,
    elementProps: {
      selectedResultChecks: () => received.action,
      resultAction,
      resultText: t(resultAction === RECEIVED ? labelIds.warningReceived : labelIds.warning, {
        action: actionLabel,
        name: userName
      }),
      onChangeResultMessage: (msg: string) => {
        message.text = msg
      },
      onChangeResultChecks: (e: ChangeAction) => {
        received.action = e
      }
    },
    accept: () => {
      setSaving(true)
      save(resultAction === RECEIVED ? received.action : resultAction, message.text)
      closeResult()
    },
    cancel: closeResult,
    labels: {
      accept: resultAction === RECEIVED ? labels.resultAccept : labels.yes,
      cancel: resultAction === RECEIVED ? labels.resultCancel : labels.no
    }
  })

  const b = {
    accept: { action: ACCEPT, disabled, icon: icons.changeStart, text: labels.buttonAccept, onClick: () => onClickAction(ACCEPT, labels.buttonAccept) },
    cancel: { action: CANCEL, icon: icons.clear, text: labels.buttonCancel, onClick: () => onClickAction(CANCEL, labels.buttonCancel) },
    finish: { action: RECEIVED, icon: icons.changeFinish, text: labels.buttonFinish, onClick: () => onClickAction(RECEIVED, labels.buttonFinish) },
    init: { action: NEW, disabled, icon: icons.changeInit, text: labels.buttonInit, onClick: () => onClickAction(NEW, labels.buttonInit) },
    reject: { action: REJECT, icon: icons.clear, text: labels.buttonReject, onClick: () => onClickAction(REJECT, labels.buttonReject) },
    update: { action: MODIFY, icon: icons.changeUpdate, text: labels.buttonUpdate, onClick: () => onClickAction(MODIFY, labels.buttonUpdate) },
    exit: { variant: 'outlined', icon: icons.close, text: labels.buttonExit, onClick: close },
  }

  const actionLinksTotal: ITotalLink[] = {
    [ChangeStateIndex.NEW]: [b.exit, b.init],
    [ChangeStateIndex.INIT]: 
      isSelf
        ? [b.exit, b.cancel].concat(isEdited ? [b.update] : [])
        : [b.exit, b.reject].concat(isEdited ? [b.update] : [b.accept]),
    [ChangeStateIndex.ACCEPTED]: [b.exit, b.cancel, b.finish],
    [ChangeStateIndex.RECEIVED]: [b.exit, b.finish]
  }[state] || []

  return {
    total: {
      offered: totalOffered,
      requested: totalRequested,
      errorMessage: disabled && isEdited ? labels.totalError : null
    },
    actionLinksTotal,
  }
}

export default totalData
