import React, { FC } from 'react'
import { FormControlLabel, Radio, RadioGroup } from 'components/core'
import { Icon } from 'components/styled'
import icons from 'components/theme/icons'

export interface IProps {
  selected: string;
  items: IFilter[];
  onChange: (value: any) => void;
  className?: string;
}

export interface IFilter {
  key: string;
  label: string;
  selected?: boolean;
}

const checkedIcon = <Icon icon={icons.check} />
const icon = <Icon icon={icons.checkEmpty} />

export const FormRadioGroup: FC<IProps> = ({
  selected,
  onChange,
  items,
  className
}) => {
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    onChange(event.target.value)
  }
  
  return (
    <RadioGroup
      className={className}
      value={selected}
      onChange={handleChange}>
      {items.map(({ key, label }: IFilter) => (
        <FormControlLabel key={key}
          label={label}
          value={key}
          control={(
            <Radio
              tabIndex={-1}
              checked={selected === key}
              checkedIcon={checkedIcon}
              icon={icon}
              />)} />)
      )}
    </RadioGroup>
  )
}
