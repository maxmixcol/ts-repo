import React, { FC, memo } from 'react'

import { ButtonAvatarChangeVStyled } from 'components/card/change/styled'

import useI18n from 'use/i18n/useI18n'

import DateModel from 'model/Date.model'
import UserExchangeModel, { IUserExchangeModel } from 'model/UserExchange.model'
import CollectionModel from 'model/Collection.model'
import SortUtility from 'use/filter/SortUtility'
import SortTypeEnum from 'use/filter/store/model/SortTypeEnum'

import { AvatarCompose } from 'compose/img/AvatarCompose'
import { CardChangeV } from 'components/card/change/CardChangeV'
import { ExchangeFull } from 'components/card/change/ExchangeFull'
import { DASHBOARD_CHANGES_SEARCH_QUERY } from 'graphql/query'
import { ExchangeTotal } from 'compose/card/change/styled'


const labelIds = {
  labelMine: 'pages.dashboard.change.offer',
  labelHis: 'pages.dashboard.change.search',
  lastLogin: 'popup.user.lastLogin'
}

export const CardChangeDashboardNewChangeCompose: FC<IUserExchangeModel> = (data) => {
  const { tm } = useI18n()
  const labels = tm(labelIds)
  const { user, exchanges } = data
  const { username = '', lastLogin } = user || {}

  const total = UserExchangeModel.getTotal(exchanges)
   
  const sortedExchanges = exchanges.slice().sort(SortUtility.sortBy([
    { name: 'collection.name', type: SortTypeEnum.String, asc: true }
  ]))
  const lastLoginFormatted = `${labels.lastLogin}: ${DateModel.dateDiff(lastLogin * 1000)}`

  return (
    <CardChangeV
      name={username}
      avatar={(
      <AvatarCompose
        user={user}
        button={ButtonAvatarChangeVStyled}
        refetch={[DASHBOARD_CHANGES_SEARCH_QUERY, 'DashboardChangesSearchQuery']}
      />)}
      lastLogin={lastLoginFormatted}>
        <>
          {sortedExchanges.map((ob, i) => {
            const { collection, mine, his } = ob
            const { name, code } = collection
            const image = CollectionModel.getImage(code)
            return (
              <ExchangeFull
                key={i}
                image={image}
                name={name}
                mine={mine.length} his={his.length} />
            )
          })
        }<ExchangeTotal mine={total.mine} his={total.his} labelMine={labels.labelMine} labelHis={labels.labelHis}/>
        </>
    </CardChangeV>
  )
}
