import React, { FC, memo } from 'react'

import { CardChange } from 'components/card/change/CardChange'

import useI18n from 'use/i18n/useI18n'

import DateModel from 'model/Date.model'
import UserExchangeModel, { IUserExchangeModel } from 'model/UserExchange.model'
import UserChangeModel from 'model/UserChange.model'
import moment from 'moment'

import { Exchange } from 'components/card/change/Exchange'
import { AvatarCompose } from 'compose/img/AvatarCompose'
import { ButtonAvatarChangeStyled } from 'components/card/change/styled'
import { DASHBOARD_HOME_CHANGES_QUERY } from 'graphql/query'

const CardChangeDashboardHomeComposeMemo: FC<IUserExchangeModel> = (data) => {
  const { t } = useI18n()
  const { change, user, exchanges } = data
  const { username = '' } = user || {}

  const { text, date } = UserChangeModel.parseChat(change.chat)
  const total = UserExchangeModel.getTotal(exchanges)

  const stateActual = t(`state.actual.${change.stateId}`)
  const mine = t(`pages.dashboard.change.offer`)
  const his = t(`pages.dashboard.change.search`)
  const dateStr = date || DateModel.dateDiff(moment(change.endDate).toISOString(), true)

  return (
    <CardChange
      avatar={<AvatarCompose
        user={user}
        button={ButtonAvatarChangeStyled}
        refetch={[DASHBOARD_HOME_CHANGES_QUERY, 'DashboardHomeChangesQuery']}/>}
      name={username}
      chat={text}
      date={dateStr}
      stateActual={stateActual}>
      <Exchange mine={total.mine} his={total.his} labelMine={mine} labelHis={his}/>
    </CardChange>
  )
}

export const CardChangeDashboardHomeCompose = memo(CardChangeDashboardHomeComposeMemo)