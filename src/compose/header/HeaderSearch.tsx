import { routes } from 'setup/constants'
import React, { FC } from 'react'
import { SearchIcon } from 'components/common/common.styled'

export const HeaderSearch: FC = ({
}) => {
  return (
    <SearchIcon linkTo={routes.publicSearch} />
  )
}