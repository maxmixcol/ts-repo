import { Color } from '@material-ui/lab'
import { FC } from 'react'

interface ILabels {
  accept: string;
  cancel: string;
}
export interface IConfirmModel {
  accept?: () => void;
  cancel?: () => void;
  text: string;
  severity?: Color;
  duration?: number;
  labels?: ILabels;
}

export interface IResultModel<T> extends Omit<IConfirmModel, 'text'> {
  element: FC<T>;
  elementProps: T;
  disabled?: boolean;
}