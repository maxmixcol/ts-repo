import { IconDefinition } from 'components/theme/icons';
import { Tab } from '@material-ui/core';
import { TabContext, TabList } from '@material-ui/lab';
import { Icon } from 'components/styled';
import { TabPanelStyled } from 'compose/popup/change/styled';
import React, { FC, useState } from 'react';

interface ContentTab {
  id: string;
  icon?: IconDefinition;
  button: string;
  content: React.ReactNode;
}
interface IProps {
  tabs?: ContentTab[];
  onChangeTab?: (id: string) => void;
};

export const Tabs: FC<IProps> = ({
  tabs,
  onChangeTab,
}) => {
  const [tab, setTab] = useState('change')

  const switchTab = (event, newValue) => {
    setTab(newValue)
    onChangeTab && onChangeTab(newValue)
  }
  return (
    <TabContext value={tab}>
      <TabList onChange={switchTab}>
        {tabs.map(({ button, id, icon }, i) => (
          <Tab label={button} value={id} icon={icon && <Icon icon={icon} />} />
          ))}
      </TabList>
      {tabs.map(({ content, id }, i) => (
        <TabPanelStyled value={id} key={i}>{ content }</TabPanelStyled>))}
    </TabContext>
  )
}
