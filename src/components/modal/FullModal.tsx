import React, { FC, memo, useEffect } from 'react';
import { IChildren } from 'utils/interface';

import { Slide } from '@material-ui/core';
import { DialogStyled, ModalBodyStyled } from './styled';
import { TransitionProps } from '@material-ui/core/transitions';
import { CloseIcon } from 'components/common/common.styled';
import { NavLeft, NavRight } from 'components/header/styled';
import { HeaderDialog } from 'components/header/HeaderDialog';
import { ButtonIconWrapperStyled } from 'components/button/styled';
import useLayout from 'use/layout/useLayout';

export interface IFullModalProps extends IChildren {
  close?: () => void;
  title: React.ReactNode;
  open?: boolean;
  headerDark?: boolean;
  fullScreen?:boolean;
  className?: string;
}
const Transition = React.forwardRef(function Transition(
  props: TransitionProps & { children?: React.ReactElement },
  ref: React.Ref<unknown>,
) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export const FullModal: FC<IFullModalProps> = ({
  children,
  title = '',
  fullScreen,
  close,
  open = false,
  headerDark = false,
  className
}) => {
  const { openModal , closeModal } = useLayout()
  useEffect(() => {
    openModal()
  }, [open])
  
  return(
  <DialogStyled
    className={className}
    fullScreen={fullScreen}
    aria-labelledby="transition-modal-title"
    aria-describedby="transition-modal-description"
    open={open}
    onClose={() => {
      closeModal()
      close()
    }}
    fullWidth={true}
    maxWidth={false}
    PaperProps={{ className }}
    TransitionComponent={Transition}
  ><>
    <HeaderDialog dark={headerDark}>
      <NavLeft>
      { title }
      </NavLeft>
      <NavRight right>
        <ButtonIconWrapperStyled onClick={() => {
          closeModal()
          close()}
          }>
          <CloseIcon />
        </ButtonIconWrapperStyled>
      </NavRight>
    </HeaderDialog>
    <ModalBodyStyled>{children}</ModalBodyStyled>
  </>
  </DialogStyled>
  )
}
