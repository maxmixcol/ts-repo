import { gql } from '@apollo/client';

export const JOIN_COLLECTION_MUTATION = gql`
mutation JoinCollectionMutation($params: UserCollectionSettingsFilter!) {
  joinCollection(
    params: $params
  )
}
`
export const FAV_COLLECTION_MUTATION = gql`
  mutation FavCollectionMutation($params: UserCollectionSettingsFilter!) {
    favCollection(
      params: $params
    )
  }
`
export const SLEEP_COLLECTION_MUTATION = gql`
  mutation SleepCollectionMutation($params: UserCollectionSettingsFilter!) {
    sleepCollection(
      params: $params
    )
  }
`
export const DELETE_COLLECTION_MUTATION = gql`
  mutation DeleteCollectionMutation($params: UserCollectionSettingsFilter!) {
    deleteCollection(
      params: $params
    )
  }
`

export const ADD_LIKE_MUTATION = gql`
  mutation AddLikeMutation($params: FeedbackFilter!) {
    addLike(
      params: $params
    )
  }
`
export const DELETE_LIKE_MUTATION = gql`
  mutation DeleteLikeMutation($id: Int) {
    deleteLike(
      id: $id
    )
  }
`

export const NEW_CHANGE_MUTATION = gql`
  mutation NewChangeMutation($params: ChangeUpdateFilter!) {
    newChange(
      params: $params
    )
  }
`

export const UPDATE_CHANGE_MUTATION = gql`
  mutation UpdateChangeMutation($params: ChangeUpdateFilter!) {
    updateChange(
      params: $params
    )
  }
`
export const UPDATE_CHANGE_UPDATE_CACHE = gql`
  mutation UpdateChangeUpdateCache($params: ChangeUpdateFilter!) {
    getChange(
      params: $params
    )
  }
`

export const SIGNUP_MUTATION = gql`
  mutation signup(
    $email: String!,
    $password: String!
  ) {
    signup(
      email: $email,
      password: $password
    ) {
      _id
      email
      password
      createdAt
      updatedAt
    }
  }
`;
export const LOGIN_MUTATION = gql`
  mutation login(
    $email: String!,
    $password: String!
    ) {
      login(
        email: $email,
        password: $password
      ) {
        _id
        email
        password
        createdAt
        updatedAt
      }
  }
`;
export const LOGOUT_MUTATION = gql`
  mutation logout{
    logout
  }
`;
export const ADD_PRODUCT_MUTATION = gql`
  mutation addProduct(
    $subtitle: String!
    $title: String!
    $price: String!
    $url: String!
  ) {
    addProduct(
      subtitle: $subtitle
      title: $title
      price: $price
      url: $url
    ) {
      _id
    }
  }
`;
export const UP_PRODUCT_MUTATION = gql`
  mutation upProduct(
    $_id: String!
    $price: String
    $subtitle: String
    $title: String
    $url: String
  ) {
    upProduct(
      _id: $_id
      price: $price
      subtitle: $subtitle
      title: $title
      url: $url
    ) {
      _id
    }
  }
`;
export const DEL_PRODUCT_MUTATION = gql`
  mutation delProduct($_id: String!) {
    delProduct(_id: $_id) {
      _id
    }
  }
`;
