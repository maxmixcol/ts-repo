import styled, { css } from 'styled-components'
import { AlbumStyled } from 'compose/popup/change/styled'
import { ElementBoxShareStyled } from '../share/styled'

export const EditContainerStyled = styled.div`
  border: 1px solid;
  
  overflow: scroll;
  margin: 1rem;
  width: calc(100% - 2rem);

  ${({ theme }) => css`
    ${theme.breakpoints.down('xs')} {
      margin: 0.2rem;
      width: calc(100% - 0.4rem);
    }
  `}
`
export const AlbumEditStyled = styled(AlbumStyled).attrs(props => ({
  item: ElementBoxShareStyled
}))`
  display: grid;

  grid-template-columns: repeat(3, 1fr);
  grid-column-gap: 3px;
  grid-row-gap: 3px;

  height: 8rem;
  overflow: auto;
  margin: 0;
`