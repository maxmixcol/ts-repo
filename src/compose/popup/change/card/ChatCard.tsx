import React, { FC } from 'react';
import { IChangeChat } from 'model/UserChange.model';
import { IChildrenOpt, IMenuLink } from 'utils/interface';
import { ChatWrite } from '../chat/styled';
import { BlockLarge, CardActionsStyled, TextareaStyled } from '../styled';
import { Chat } from '../chat/Chat';

interface IUserCard extends IChildrenOpt {
  allowChat: boolean;
  header: React.ReactNode;
  label: string;
  message: string;
  chatList: IChangeChat[];
  showWrapper: boolean;
  actionLinks?: IMenuLink[];
  onChange: (e:any) => any;
  onFocus?: () => void;
  onBlur?: () => void;
}
export const ChatCard: FC<IUserCard> = ({
  allowChat,
  actionLinks,
  header,
  label,
  message,
  chatList,
  showWrapper,
  onChange,
  onFocus,
  onBlur,
  children
}) => {
  const chat = (
    <>
      <Chat chatList={chatList} />
    { allowChat && 
      <ChatWrite>
        <TextareaStyled value={message} label={label}
          onBlur={e => onBlur && onBlur()}
          onFocus={e => onFocus && onFocus()}
          onChange={e => onChange && onChange(e.target.value)} />
      </ChatWrite>}
      { children }
      </>
  )
  return showWrapper
    ? <BlockLarge>
        <CardActionsStyled variant='invert' color='secondary'
          content={header} actionLinks={actionLinks}>
          {chat}
        </CardActionsStyled>
      </BlockLarge>
    : chat
}