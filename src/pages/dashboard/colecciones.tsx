import React, { memo, useEffect } from 'react'
import { navigate } from 'gatsby'
import { LinearProgress } from '@material-ui/core'
import { DASHBOARD_COLLECTIONS_QUERY } from 'graphql/query'

import { routes } from 'setup/constants'

import { useQueryAll } from 'use/fetch/useQuery'
import useAuth from 'use/auth/useAuth'
import useCollectionFilter from 'use/filter/impl/useCollectionFilter'
import useI18n from 'use/i18n/useI18n'

import { DashboardView } from 'components/typography/view/dashboard/DashboardView'
import { CardCollectionDashboardCollectionsCompose } from 'compose/card/collection/CardCollectionDashboardCollectionsCompose'
import { FilterCompose } from 'compose/filter/FilterCompose'
import { HeaderTabs } from 'components/header/types/HeaderTabs'
import { BackIcon, ListVertical } from 'components/common/common.styled'
import { EmptyListCompose } from 'compose/empty/EmptyListCompose'
import { PopupCollectionCompose } from 'compose/popup/PopupCollectionCompose'
import { ChangeState } from 'model/UserChange.model'
import { IUserCollectionExModel } from 'model/UserCollectionEx.model'
import { useModalCollection, useModalCollectionEdit, useModalCollectionShare } from 'use/modal/useModal'
import { PopupCollectionShareCompose } from 'compose/popup/PopupCollectionShareCompose'
import { PopupCollectionEditCompose } from 'compose/popup/PopupCollectionEditCompose'


const labelIds = {
	userCollectionsTitle: 'pages.dashboard/colecciones.title',
	newCollectionTitle: 'pages.dashboard/colecciones/nueva.title',
  numCollections: 'filter.numCollections',
}
const DashboardColeccionesPage = ({ location }) => {
	const userId = useAuth().user
	const labels = useI18n().tm(labelIds)

	const { open: openCollection, isOpen: isCollectionOpen } = useModalCollection()
  const { isOpen: isCollectionEditOpen } = useModalCollectionEdit()
	const { isOpen: isCollectionShareOpen } = useModalCollectionShare()
	const onClickCollection = (item: IUserCollectionExModel) => {
		const { code, name } = item.userCollection.collection
		openCollection({ code, name }, {
      refetch: [DASHBOARD_COLLECTIONS_QUERY, 'DashboardCollectionsQuery']
    })
	}

	const { init, process, filterProps } = useCollectionFilter()
	init()

	const collectionsQuery = useQueryAll<IUserCollectionExModel>('userCollectionEx',
    DASHBOARD_COLLECTIONS_QUERY, 
    { onCompleted: (e) => {
      if (collectionsQuery.list?.length === 0) {
        navigate(routes.userCollectionsJoin)
      }
    }})

  const processedProps = process(collectionsQuery.list || [])

  const tabCollections = { label: labels.userCollectionsTitle, to: routes.userCollections, selected: true }
  const tabNewCollection = { label: labels.newCollectionTitle, to: routes.userCollectionsJoin, selected: false }
  const headerTabs = [tabCollections, tabNewCollection]

	useEffect(() => {
    if (userId) {
      collectionsQuery.get({
        userId,
        state: ChangeState.COMPLETE,
        elements: false,
        userElements: true,
        changes: true
      })
    }
	}, [ userId ])
  
  const emptyCollections = !collectionsQuery.loading && collectionsQuery.list?.length === 0
  const loading = collectionsQuery.loading

	return (
		<DashboardView route={routes.userCollections}
      location={location}
			header={
				<>
					<BackIcon linkTo={routes.userDashboard} />
					<HeaderTabs items={headerTabs} />
				</>}>
			{loading && <LinearProgress />}
			{emptyCollections
				? <EmptyListCompose id='emptyCollections' />
				: <FilterCompose
					{...filterProps}
					{...processedProps}
          loading={loading}
				>{list => {
          return (
					<ListVertical
						list={list}
						onClick={item => onClickCollection(item)}
						item={CardCollectionDashboardCollectionsCompose}>
					</ListVertical>)}
        }</FilterCompose>}
			{ isCollectionOpen && <PopupCollectionCompose />}
      { isCollectionEditOpen && <PopupCollectionEditCompose />}
      { isCollectionShareOpen && <PopupCollectionShareCompose />}
		</DashboardView>
	)
}

export default memo(DashboardColeccionesPage)
