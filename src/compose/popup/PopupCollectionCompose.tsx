import React, { FC } from 'react'
import icons from 'components/theme/icons'
import { CardItem } from './_shared/CardItem'

import {
  ListStyled,
} from './_shared/styled'

import { PopupCollection } from 'components/popup/collection/PopupCollection'
import usePopupCollection from 'use/popup/impl/usePopupCollection'
import { IButtonListProps, IButtonTypes } from 'components/button/ButtonList'
import { HeroColor } from 'components/hero/Hero'
import { DescriptionStyled } from 'components/popup/collection/styled'

export const PopupCollectionCompose: FC = () => {
  const { labels, result, fullModal } = usePopupCollection()
  const {
    collection,
    actions,
    refreshing,
    showProgress,
    values
  } = result

  const listHistorical = [
    labels.changesInProgress && { label: labels.changesInProgress, onClick: actions.changesInProgress },
    labels.changesCompleted && { label: labels.changesCompleted, onClick: actions.changesCompleted },
    labels.changesCancelled && { label: labels.changesCancelled, onClick: actions.changesCancelled },
  ].filter(Boolean)

  const buttons: IButtonListProps[] = actions ? [
    { children: labels.edit, icon: icons.edit, onClick: actions.edit, type: IButtonTypes.primary },
    listHistorical.length && { children: labels.historical, list: listHistorical, icon: icons.historical },
    { children: labels.fav, icon: icons.fav, onClick: actions.fav, toggle: values.fav },
    { children: labels.sleep, message: labels.sleepMessage, icon: icons.sleep, onClick: actions.sleep, toggle: values.sleep },
    { children: labels.share, icon: icons.share, onClick: actions.share },
    { children: labels.index, icon: icons.index, onClick: actions.index },
    { color: HeroColor.danger, children: labels.delete, icon: icons.delete, onClick: actions.delete }
  ].filter(Boolean) : []

  return (
    <>{fullModal.open &&
      <PopupCollection
        {...fullModal}
        collection={collection}
        description
        buttons={buttons}
        showProgress={showProgress}
        refresh={refreshing}>
        <ListStyled>
          <CardItem name={labels.year} value={values.year} />
          <CardItem name={labels.countFaults} color={HeroColor.danger} value={values.countFaults} />
          <CardItem name={labels.countRepeated} color={HeroColor.success} value={values.countRepeated} />
        </ListStyled>
      </PopupCollection>      
    }
    </>
  )
}
