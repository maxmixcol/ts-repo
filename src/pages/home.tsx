import React from 'react'
import { navigate } from 'gatsby'
import { isBrowser } from 'utils/helpers'
import IndexPage from 'pages'


const HomePage = ({ location }) => {  
  return <IndexPage location={location} />
}

export default HomePage