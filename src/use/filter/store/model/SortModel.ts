import SortUtility from "use/filter/SortUtility"
import { ISortModelMap } from "utils/interface"
import { IFilterOptionMap } from "./FilterModel"
import SortTypeEnum from "./SortTypeEnum"


export default class SortModel {

  /*
  static setSortOrder(arr: Array<SortModel>, first: String) {
    return arr.sort((x,y) => x.name === first ? -1 : y.name === first ? 1 : 0)
  }
  */

  static sortOptions = (map: IFilterOptionMap) => {
    return SortUtility.sortObject(
      Object.keys(map).map(key => map[key]),
      { name: 'whatever', asc: true, type: SortTypeEnum.String }, 'title'
    )
  }
  static updateSortOptions = ({
    sortMap,
    sortLabels
  }: {
    sortMap: ISortModelMap,
    sortLabels: any
  }) => {
    const sortFilterOptions = {}
    Object.keys(sortMap).forEach(key => {
      sortFilterOptions[key] = { title: sortLabels[key], value: key }
    })
    return SortModel.sortOptions(sortFilterOptions)
  }
}
