/**
 * React, Gatsby, Jest, TypeScript, Apollo - Starter
 * https://github.com/eduard-kirilov/gatsby-ts-apollo-starter
 * Copyright (c) 2020 Eduard Kirilov | MIT License
 */
import React from 'react';
import { shallow } from 'enzyme';

import { Footer } from './Footer';
import { withTheme } from 'setup/withTheme';

describe('Footer Container', () => {
  describe('Footer', () => {
    const mount = shallow(
      withTheme( 
        <Footer />
      )
    );
    it('<Footer /> is authorized test should work', () => {
      expect(mount.find('Footer')).toHaveLength(0);
    });
  });
});
