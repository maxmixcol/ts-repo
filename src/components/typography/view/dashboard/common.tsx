import React, { FC } from 'react'
import { IChildren } from 'utils/interface'
import { ListButtonStyled } from 'components/filter/styled'
import { CustomLink } from 'components/button/CustomLink'
import { Main } from 'components/common/common'
import { PageCompose } from 'compose/layout/PageCompose'
import { PrivateCompose } from 'compose/private/PrivateCompose'
import { StatusBarCompose } from 'compose/statusbar/StatusBarCompose'
import useLayout from 'use/layout/useLayout'
import { Header } from 'components/header/Header'


export interface IActionProps extends IChildren {
  linkTo: string;
}

interface IDashboardProps extends IChildren {
  header?: React.ReactNode,
  route: string
}

export const DashboardView: FC<IDashboardProps> = ({ route, header, children }) => {
  const { page } = useLayout(route)

  return (
    <PrivateCompose route={route}>
      <Header>
        { header }
      </Header>
      <Main>
        { children }
      </Main>
      <StatusBarCompose selected={route} />
    </PrivateCompose>
  )
}

export const JoinNewCollectionButton: FC<IActionProps> = ({
  linkTo,
  children
}) => {
  return (
    <CustomLink to={linkTo}>
      <ListButtonStyled>
        {children}
      </ListButtonStyled>
    </CustomLink>
  )
}

export const SearchNewChangesButton: FC<IActionProps> = ({
  linkTo,
  children
}) => {
  return (
    <CustomLink to={linkTo}>
      <ListButtonStyled>
        {children}
      </ListButtonStyled>
    </CustomLink>
  )
}