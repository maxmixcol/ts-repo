import React, { FC } from 'react';
import { AddressState, IAddress } from 'model/UserInfo.model';
import { Card } from '@material-ui/core';
import useI18n from 'use/i18n/useI18n';
import { AddressCardStyled } from './styled';
import { FlagCompose } from 'compose/FlagCompose';

interface IAddressProps extends Partial<IAddress> {
  state: AddressState;
}

const labelIds = {
  error: 'popup.change.address.error',
  loaded: 'popup.change.address.loaded',
  loading: 'popup.change.address.loading',
  noAddress: 'popup.change.address.noAddress',
}

export const AddressCompose: FC<IAddressProps> = ({
  state,
  name,
  address,
  country,
}) => {

  const { tm } = useI18n()
  const labels = tm(labelIds)

  if (state === AddressState.NEW) {
    return <></>
  }

  const label = {
    [AddressState.LOADING]: labels.loading,
    [AddressState.ERROR]: labels.error,
    [AddressState.LOADED]: address ? labels.loaded : labels.noAddress
  }[state]

  return (
    <AddressCardStyled>
      { address && <FlagCompose country={country} />}
      <span>{ label }</span> 
      { address && (
        <>
          <span>{name}</span>
          <span>{address}</span>
        </>
      )}
    </AddressCardStyled>
  )
}
