import React, { FC } from 'react';
import { LeftRight } from 'components/layout/LeftRight';
import { TElement, IElementMap } from 'utils/interface';
import { AlbumThreeRowStyled, MaxiListWrapper } from './styled';
import { IAlbumProps } from './Album';

interface IProps {
  selected?: TElement[];
  unselected?: TElement[];
  elements?: IElementMap;
  element?: React.FC<IAlbumProps>;
  select?: (o: string, i: number) => void;
  unselect?: (o: string, i: number) => void;
}
export const AlbumSelection: FC<IProps> = ({
  selected = [],
  unselected = [],
  elements,
  element,
  select,
  unselect
}) => {
  const Element: React.FC<IAlbumProps> = element || AlbumThreeRowStyled
  return (
    <MaxiListWrapper>
      <LeftRight 
        left={<Element elements={elements} items={unselected} onClick={select}/>}
        right={<Element variant='selected' elements={elements} items={selected} onClick={unselect}/>} />
    </MaxiListWrapper>
  );
};
