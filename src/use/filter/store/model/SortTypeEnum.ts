enum SortTypeEnum {
    Custom = 'custom',
    Date = 'date',
    Digits = 'digits',
    Number = 'number',
    String = 'string',
}

export default SortTypeEnum