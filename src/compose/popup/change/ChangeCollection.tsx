import React, { FC, useState } from 'react'
import { TElement, IReadonly, IElementMap } from 'utils/interface'

import { Album } from './Album'
import { AlbumBigCardStyled, AlbumStyled, AlbumThreeRowStyled, CardToolbarStyled, ToolbarLabelStyled } from './styled'
import { ChangeNum } from './ChangeNum'
import { AlbumSelection } from './AlbumSelection'
import { ViewMode } from './ViewModeSelector'

interface IElements {
  elements?: IElementMap;
}
interface ISelection {
  code: number;
  selected: TElement[];
  unselected: TElement[];
}

export interface IChangeCollectionListProps extends ISelection {
  name: string;
  variant?: ViewMode;
}
export interface INumberLabel {
  labelFn: (num: number) => string;
  sortFn: (list: TElement[]) => TElement[];
}
export interface ISelect {
  onSelect: (item: ISelection) => void;
}
export type IChangeCollectionProps = IChangeCollectionListProps & IReadonly & ISelect & INumberLabel & IElements

const { SMALL, BIG } = ViewMode


export const ChangeCollection: FC<IChangeCollectionProps> = ({
  code,
  name,
  selected,
  unselected,
  elements,
  readonly,
  variant = SMALL,
  onSelect,
  labelFn,
  sortFn
}) => {
  const [ opened, setOpened ] = useState(false)
  const [ color, setColor ] = useState('')
  const toggle = () => {
    setColor(opened ? '' : 'info')
    setOpened(!opened)
  }

  const select = (o: string, i: number) => {
    const item = unselected[i]
    const newArr = unselected.slice()
    newArr.splice(i, 1)
    onSelect({
      code,
      unselected: newArr,
      selected: sortFn(selected.slice().concat(item))
    })
  }

  const unselect = (o: string, i: number) => {
    const item = selected[i]
    const newArr = selected.slice()
    newArr.splice(i, 1)
    onSelect({
      code,
      selected: newArr,
      unselected: sortFn(unselected.slice().concat(item))
    })
  }

  const onClickToolbar = toggle
  const showFullList = !readonly
  const title = 
    <>
      { name }
      <ChangeNum
        small={unselected.length || undefined}
        big={selected.length} />
    </>

  const MiniList = {
    [SMALL]: AlbumStyled,
    [BIG]: AlbumBigCardStyled
  }[variant]

  const selectionElement = {
  [SMALL]: AlbumThreeRowStyled,
  [BIG] : AlbumThreeRowStyled
}[variant]

  return (
    <CardToolbarStyled title={title} color={color} onClick={onClickToolbar}>
      
      { showFullList 
        ? <AlbumSelection
            select={select}
            unselect={unselect}
            element={selectionElement}
            selected={selected}
            unselected={unselected}
            elements={elements} />
        :
        <>
          <ToolbarLabelStyled>{ labelFn(selected.length) }</ToolbarLabelStyled>
          { !!selected.length && <MiniList items={selected} elements={elements}/>}
        </>
      }
    </CardToolbarStyled>
  )
}
