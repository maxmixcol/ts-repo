
import React, { FC } from 'react'
import { StatusBar } from 'components/statusbar/StatusBar'
import { drawers } from 'setup/constants'
import useMenu from 'use/menu/useMenu'

export interface IPage {
  selected?: string;
}
export const StatusBarCompose: FC<IPage> = ({
  selected
}) => {

  const { drawer } = useMenu(drawers.status, selected)
  const { index, links } = drawer

  return (
    <StatusBar
      index={index}
      links={links}/>
  )
}