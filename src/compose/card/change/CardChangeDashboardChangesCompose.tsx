import React, { FC, memo } from 'react'

import { ButtonAvatarChangeVStyled, StateBadge } from 'components/card/change/styled'

import useI18n from 'use/i18n/useI18n'

import DateModel from 'model/Date.model'
import UserExchangeModel, { IUserExchangeModel } from 'model/UserExchange.model'
import { CardChangeV } from 'components/card/change/CardChangeV'
import UserChangeModel from 'model/UserChange.model'
import { ExchangeFullCompose } from './ExchangeFullCompose'
import { AvatarCompose } from 'compose/img/AvatarCompose'
import { DASHBOARD_CHANGES_QUERY } from 'graphql/query'
import { ExchangeTotal } from 'compose/card/change/styled'

const labelIds = {
  self: 'state.self',
  labelMine: 'pages.dashboard.change.offer',
  labelHis: 'pages.dashboard.change.search',
  stateFull: 'state.full',
  statePending: 'state.pending',
  lastLogin: 'popup.user.lastLogin'
}

const STATE_COLORS = {
  1: 'info',
  2: 'info',
  3: 'info',
  5: 'info',
  6: 'info',
  7: 'success',
  8: 'danger',
  9: 'danger',
  10: 'danger',
  11: 'danger',
  12: 'danger',
  13: 'danger'
}

const CardChangeDashboardChangesComposeMemo: FC<IUserExchangeModel> = (data) => {
  const { t } = useI18n()

  const { change, user, exchanges } = data
  const { stateId, him } = change
  const { username = '', lastLogin } = user || {}

  const { text, date } = UserChangeModel.parseChat(change.chat)
  const total = UserExchangeModel.getTotal(exchanges)

  // const self = UserChangeModel.isSelf(change, user.id)
  const name1 = t(labelIds.self)
  const name2 = `${username.slice(0,3)}${username.length>3?'...':''}` || ''
   
  const labelMine = t(labelIds.labelMine)
  const labelHis = t(labelIds.labelHis)
  const stateFull = t(`${labelIds.stateFull}.${stateId}`, {
    user1: him ? name1 : name2,
    user2: him ? name2 : name1
  })
  const stateLabels = [{
    color: STATE_COLORS[stateId],
    label: stateFull,
    first: stateId === 1
  }]
  if (stateId < 7) {
    stateLabels.push({
      color: 'rose',
      label: t(`${labelIds.statePending}.${stateId}`),
      first: false
    })
  }

  const state = stateLabels.map(({ color, label, first }, i) => {
    const points = first ? '' : '-'
    const badge = <StateBadge color={color} size="xs">{label}</StateBadge>
    return (<span key={i}>{points}{badge}</span>)
  })

  const lastLoginFormatted = `${t(labelIds.lastLogin)}: ${DateModel.dateDiff(lastLogin * 1000)}`
  const dateFormatted = DateModel.dateDiff(`${change.endDate}`)
  return (
    <CardChangeV
      name={username}
      chat={text}
      avatar={<AvatarCompose
        user={user}
        button={ButtonAvatarChangeVStyled}
        refetch={[DASHBOARD_CHANGES_QUERY, 'DashboardChangesQuery']} />}
      lastLogin={lastLoginFormatted}
      date={dateFormatted}
      state={state}>
        <>
          {exchanges.map((ob, i) => <ExchangeFullCompose key={i} exchange={ob}/>)}
          <ExchangeTotal mine={total.mine} his={total.his} labelMine={labelMine} labelHis={labelHis}/>
        </>
    </CardChangeV>
  )
}


export const CardChangeDashboardChangesCompose = memo(CardChangeDashboardChangesComposeMemo)