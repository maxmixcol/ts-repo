import React, { FC, memo } from 'react';
import icons from 'components/theme/icons';

import { IChildrenOpt } from 'utils/interface';
import { ActionWrapper, CardBlockStyled } from '../styled';

import {
  CardHeaderChangeStyled,
  CardBodyStyled,
  CardFooterStyled,
  ChatChangeStyled,
  DateChangeStyled,
  IconStyled,
  InfoWrapperStyled,
  StateStyled,
  CardAvatarWrapper,
} from './styled';

import { NameWrapper } from 'components/common/NameWrapper';
export interface IChangeCard extends IChildrenOpt {
  name?: string;
  date?: string;
  lastLogin?: string;
  state?: React.ReactNode;
  chat?: string;
  avatar: React.ReactNode;
}

export const CardChangeV: FC<IChangeCard> = ({
  name = '',
  chat = '',
  avatar = '',
  date = '',
  lastLogin = '',
  state = '',
  children = ''
}) => {

  return (
    <CardAvatarWrapper>
      {avatar}
      <ActionWrapper>
        <CardBlockStyled>
          <CardHeaderChangeStyled>
            <InfoWrapperStyled>
              <NameWrapper big name={name}>
                { name }
                { lastLogin && <DateChangeStyled>{lastLogin}</DateChangeStyled>}
              </NameWrapper>
            </InfoWrapperStyled>
            { state && <StateStyled>{state}</StateStyled>}
            { date && <DateChangeStyled>{date}</DateChangeStyled>}
            <IconStyled icon={icons.unfold}/>
          </CardHeaderChangeStyled>
          {chat && <CardBodyStyled>
            <ChatChangeStyled>{chat}</ChatChangeStyled>
          </CardBodyStyled>}
        </CardBlockStyled>
        <CardFooterStyled>
          {children}
        </CardFooterStyled>
      </ActionWrapper>
    </CardAvatarWrapper>
  )
}