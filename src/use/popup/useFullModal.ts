import { IFullModalProps } from "components/modal/FullModal";
import { useEffect } from "react";
import { useModalStore } from "use/modal/useModal";
import { IAllStringProps } from "utils/interface";




export interface IFullModal {
	close?: () => void;
	isOpen: boolean;
}

interface IModal<T> extends IFullModal {
	open?: (data?: T) => void;
	data: T;
}

export interface IProcess {
	refresh?: () => void;
	labels: any;
}
export interface IFullModalIn<T, V> {
	labelIds: IAllStringProps;
	modalId: string;
	headerDark?: boolean;
	process?: (data: T) => V & IProcess;
}

export interface IFullModalOut<V> {
	fullModal: Partial<IFullModalProps>;
	labels: IAllStringProps;
	result: V | undefined;
}

export default function useFullModal<T, V>({
	modalId,
	headerDark,
	process
}: IFullModalIn<T, V>): IFullModalOut<V> {

	const { isOpen, close, data }: IModal<T> = useModalStore<T>(modalId)
	const processed = isOpen ? process(data) : undefined

	useEffect(() => {
		if (isOpen && processed?.refresh) {
			processed.refresh()
		}
	}, [isOpen])

	return {
		fullModal: {
			open: isOpen,
			close,
			headerDark
		},
		labels: processed?.labels || {},
		result: processed
	}
}