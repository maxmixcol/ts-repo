import React, { FC } from 'react'
import { navigate } from 'gatsby'

import { routes } from 'setup/constants'
import { RawButtonWithIcon } from 'components/button/RawButtonWithIcon'
import { CustomLink } from 'components/button/CustomLink'
import { PrimaryButton, PrimaryEmptyButton } from 'components/common/common.styled'
import images from 'components/theme/images'
import icons from 'components/theme/icons'
import { LinearProgressStyled } from 'components/status/styled'
import {
  FullModalStyled,
  HeroStyled,
  ListStyled,
  PrimaryEmptyButtonStyled
} from './_shared/styled'
import usePopupUser from 'use/popup/impl/usePopupUser'
import { ButtonWrapper } from 'components/button/styled'
import { NameWrapper } from 'components/common/NameWrapper'
import { BigNameUserStyled, LikeButtonStyled } from 'compose/popup/user/styled'
import { CardItem, CardItemButton } from 'compose/popup/_shared/CardItem'
import { getSize } from 'utils/helpers'
import { Icon } from 'components/styled'
import { ChangeFilter } from 'model/UserChange.model'
import useChangeFilter from 'use/filter/impl/useChangeFilter'
import { HeroColor } from 'components/hero/Hero'
import { CardPopupStyled } from 'components/card/styled'

const { COLLECTOR, COLLECTION, STATE, SEARCH } = ChangeFilter

export const PopupUserCompose: FC = () => {
  const { labels, result, fullModal } = usePopupUser()
  const { init, updateFilter } = useChangeFilter()
  init()

  const { 
    values = {},
    showProgress,
    name,
    image,
    iduser,
    doLike,
  } = result
  
  const routeUser = routes.publicUser.replace(':id', `${iduser}`)
  const likeLabel = values.likes === 1 ? labels.like : labels.likes
  const onClickChangesWith = () => {
    updateFilter(STATE, { input: '' })
    updateFilter(COLLECTION, { input: '' })
    updateFilter(SEARCH, { disabled: true })
    updateFilter(COLLECTOR, { input: iduser, options: [{ title: name, value: `${iduser}` }] })

    fullModal.close()
    navigate(routes.userChanges)
  }

  return (
    <>{ fullModal.open &&
    <FullModalStyled
      {...fullModal}
      title={<BigNameUserStyled size={getSize(name, 10, 20)}>{ name }</BigNameUserStyled>}
    >
      { showProgress && <LinearProgressStyled /> }
      <HeroStyled image={image || images.avatar} name={name} reduced={!image}>
        <NameWrapper big name={name} />
      </HeroStyled>
      <CardPopupStyled>
          <>
            <ListStyled>
              <CardItem name={labels.lastLogin} value={values.lastLogin} />
              <CardItem name={labels.numCollections} value={values.numCollections} />
              <CardItem name={labels.likes} value={values.likes} />
              { !!values.numChangesNok && 
                <CardItem name={labels.numChangesNok} value={values.numChangesNok} color={HeroColor.danger} />}
              <CardItemButton name={labels.numChangesWith} value={values.numChangesWith} onClick={onClickChangesWith} />
              <CardItem name={labels.numChangesStarted} value={values.numChangesStarted} />
              <CardItem name={labels.numChangesFinished} value={values.numChangesFinished} />
              <CardItem name={labels.lastChange} value={values.lastChange} />
              <CardItem name={labels.lastChangeFinished} value={values.lastChangeFinished} />
              <CardItem name={labels.unanswered} value={values.unanswered} />
            </ListStyled>
            <ButtonWrapper>
              <VisitButton to={routeUser} label={labels.visit} />
              <LikeButton
                mine={!!values.mine}
                disabled={showProgress}
                label={likeLabel}
                onClick={doLike} />
            </ButtonWrapper>
          </>
      </CardPopupStyled>
    </FullModalStyled>
  }
  </>
  )
}

interface IVisitProps {
  label: string;
  to: string;
}

const VisitButton: FC<IVisitProps> = ({ label, to }) => (
  <CustomLink to={to}>
    <RawButtonWithIcon item={PrimaryEmptyButton} icon={icons.community} >
      { label }
    </RawButtonWithIcon>
  </CustomLink>
)


interface ILikeProps {
  label: string;
  mine: boolean;
  onClick: () => void;
  disabled?: boolean;
}

const LikeButton: FC<ILikeProps> = ({ label, onClick, mine, disabled }) => (
  <LikeButtonStyled
    disabled={disabled}
    item={mine ? PrimaryEmptyButtonStyled : PrimaryButton }
    onClick={onClick}>
    <Icon icon={mine ? icons.unlike : icons.like} /> {label}
  </LikeButtonStyled>
)