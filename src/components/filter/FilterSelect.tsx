import React, { FC } from 'react'
import { InputAdornment, MenuItem } from '@material-ui/core'
import { AutocompleteStyled, TextFieldStyled } from './styled'
import { Icon } from 'components/styled'
import { IconDefinition } from 'components/theme/icons';

type TChange = (any) => void;
export interface IFilterSelectProps  {
  icon?: IconDefinition;
  iconRight?: IconDefinition;
  label: string;
  value: any;
  options: IOption[];
  onChange: TChange;
  variant: 'autocomplete' | 'dropdown';
}

export interface IOption {
  title: string;
  value: any;
}

const onChangeAutocomplete = (onChange: TChange) => (e: any, selected: IOption, reason: string) => {
  onChange(selected.value)
}
const onChangeDropdown = (onChange: TChange) => (e: any) => {
  onChange(e.target.value)
}

export const FilterSelect: FC<IFilterSelectProps> = ({
  icon,
  iconRight,
  label,
  value,
  options,
  onChange,
  variant = 'autocomplete'
}) => {
  const optionSelected = options.find(option => option.value === value)
  const startAdornment = icon && <InputAdornment position="start"><Icon icon={icon} /></InputAdornment>

  return {
    autocomplete: (
      <AutocompleteStyled
        color="default"
        disableClearable
        popupIcon={iconRight ? <Icon icon={iconRight} /> : ''}
        includeInputInList
        value={optionSelected}
        options={options}
        onChange={onChangeAutocomplete(onChange)}
        getOptionLabel={(option: IOption) => option.title}
        renderInput={(params) => (
        <TextFieldStyled
          {...params}
          InputProps={{...params.InputProps, ...{ startAdornment }}}
          label={label} />
        )}
      />
    ),
    dropdown: (
      <TextFieldStyled
        select
        margin='dense'
        variant='outlined'
        label={label}
        value={optionSelected?.value}
        SelectProps={{
          IconComponent: 'i'
        }}
        InputProps={{
          startAdornment
        }}
        onChange={onChangeDropdown(onChange)}
      >{options.map(({ title, value }) => (
          <MenuItem key={value} value={value}>{title}</MenuItem>
      ))}
      </TextFieldStyled>
    )
  }[variant]
}
