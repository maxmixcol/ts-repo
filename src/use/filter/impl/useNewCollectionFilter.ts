import { TFilterOptionUpdate } from 'compose/filter/FilterCompose'
import CollectionModel from 'model/Collection.model'
import { ISearchCollectionModel } from 'model/SearchCollection.model'
import { IUserCollectionModel } from 'model/UserCollection.model'
import { useEffect, useState } from 'react'
import { filters } from 'setup/constants'
import useI18n from 'use/i18n/useI18n'
import { mapArrayBy, mapBy } from 'utils/helpers'
import { ISortModelMap } from 'utils/interface'
import SortUtility from '../SortUtility'
import FilterModel, { IFilterModelMap, TFilterMapper } from '../store/model/FilterModel'
import SortModel from '../store/model/SortModel'
import SortTypeEnum from '../store/model/SortTypeEnum'
import useFilter from '../useFilter'

interface ISearchData {
  name: string;
}
interface ISearchCollectionModelParsed extends ISearchCollectionModel {
  searchData: ISearchData
}

const filterId = filters.newCollection

/* SORT */

enum NewCollectionSort {
  TOP = 'top',
  CAT = 'category',
} 
const SORT_MAP: ISortModelMap = {
  [NewCollectionSort.TOP]: [
    { name: 'collectors.recent', type: SortTypeEnum.Number, asc: false },
    { name: 'collectors.total', type: SortTypeEnum.Number, asc: false },
    { name: 'collection.name', type: SortTypeEnum.String, asc: true }
  ],
  [NewCollectionSort.CAT]: [
    { name: 'collectors.total', type: SortTypeEnum.Number, asc: false },
    { name: 'collection.name', type: SortTypeEnum.String, asc: true }
  ]
}

const labelIds = {
  byCollection: 'filter.by.collection',
  byYear: 'filter.by.year',
  bySubject: 'filter.by.subject',
  byKey: 'filter.by.key',
  topCollections: 'filter.label.collectionList.top',
  searchCollection: 'filter.by.searchCollection'
}
enum NewCollectionFilter {
  YEAR = 'year',
  SUBJECT = 'subject',
  SEARCH = 'search',
} 
//////////

const { YEAR, SUBJECT, SEARCH } = NewCollectionFilter

const FILTER_MAP: IFilterModelMap = {
  [YEAR]: { name: YEAR, label: labelIds.byYear, input: '' },
  [SUBJECT]: { name: SUBJECT, label: labelIds.bySubject, input: '' },
  [SEARCH]: { name: SEARCH, label: labelIds.searchCollection, input: '' },
}
const getSearchData = (item: ISearchCollectionModel): ISearchData => {
  const name = FilterModel.flatName(item.collection.name)

  return { name }
}

const updateOptions = (list: ISearchCollectionModelParsed[]): TFilterOptionUpdate[] => {
  const searchOptions = {}
  const yearOptions = {}
  const subjectOptions = {}
  const keyOptions = {}
  list.forEach(item => {
    const { collection: ob, collectors, searchData } = item
    const subject = CollectionModel.filterSubject(ob.subject)

    yearOptions[ob.year] = { title: `${ob.year}`, value: ob.year }
    subjectOptions[subject] = { title: subject, value: subject }
    keyOptions[ob.key] = { title: ob.key, value: ob.key }

    subjectOptions[ob.subject] = { title: ob.subject, value: ob.subject }
    searchOptions[ob.code] = searchData.name
  })
  return [
    { name: YEAR, options: FilterModel.EMPTY.concat(SortModel.sortOptions(yearOptions)) },
    { name: SUBJECT, options: FilterModel.EMPTY.concat(SortModel.sortOptions(subjectOptions)) },
    { name: SEARCH, query: (value: any, input: any) => searchOptions[value].indexOf(input) !== -1 },
  ]
}

const filterMapper: TFilterMapper<ISearchCollectionModel> = (
  { collection },
  { name, input, query }
) => {
  return {
    [SEARCH]: query && input && query(collection.code, input),
    [SUBJECT]: CollectionModel.filterSubject(collection.subject) === input,
    [YEAR]: collection.year === input,
  }[name]
}

const useNewCollectionFilter = () => {
  const { t } = useI18n()

  const {
    getFilter,
    getInit,
    onChange,
    updateFilter,
    values,
  } = useFilter(filterId)

  const init = () => {
    useEffect(() => { getInit(FILTER_MAP, t)}, [])
  }

  const process = (list: ISearchCollectionModel[] = [], userCollections: IUserCollectionModel[]) => {
    let listIds = list.map(({ collection: ob }) => ob.code).sort()
    let colCodes = mapBy(userCollections, 'collection.code')
    const [ parsedList, setParsedList ] = useState<ISearchCollectionModel[]>()

    useEffect(() => {
      if (list.length) {
        const parsedList = list
          .filter(({ collection }) => !colCodes[collection.code])
          .map(item => ({ ...item, ...{ searchData: getSearchData(item) }}))

        updateOptions(parsedList).forEach(o => updateFilter(o.name, o))
        setParsedList(parsedList)
      }
    }, [ listIds.join(), Object.keys(colCodes).join() ])
    
    return {
      model: getFilter || [],
      list: parsedList
    }
  }

  const categorize = (completeList: ISearchCollectionModel[] = []) => {
    const { subject, year, search } = values()

    const doSort = (sort, list) => (list.slice().sort(SortUtility.sortBy(sort)))

    const mapBySubject = mapArrayBy(completeList, 'collection.subject')
    const subMap = subject ? mapBySubject : getSubMap(mapBySubject, CollectionModel.filterSubject)

    const sortedSubjects = Object.keys(subMap).map(subject => ({
      id: subject,
      items: doSort(SORT_MAP[NewCollectionSort.CAT], subMap[subject]),
    })).sort((a, b) => b.items.length - a.items.length)
    
    const top = subject || year || search
      ? []
      : [{
        id: t(labelIds.topCollections),
        items: doSort(SORT_MAP[NewCollectionSort.TOP], completeList).slice(0, 100)
      }]
    return top.concat(sortedSubjects)
  } 
  return {
    init,
    process,
    categorize,
    filterProps: {
      onChange,
      filterId,
      updateOptions,
      filterMapper,
      searchName: SEARCH,
    }
  }
}

export default useNewCollectionFilter


function getSubMap(map: [key: string, value: ISearchCollectionModel[]], filterFn: (key: string) => string) {
  const result = {}
  Object.keys(map).forEach(key => {
    const token = filterFn(key)
    if (!result[token]) {
      result[token] = map[key]
    } else {
      result[token].concat(map[key])
    }
  })
  return result
}