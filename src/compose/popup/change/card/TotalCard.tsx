import React, { FC } from 'react';
import { IMenuLink } from 'utils/interface';
import { CardActionsTotalStyled } from '../styled';

interface ITotalActions {
  actionLinks?: IMenuLink[];
}

export const TotalCard: FC<ITotalActions> = ({
  actionLinks,
  children
}) => (
  <CardActionsTotalStyled
    variant='raw'
    actionLinks={actionLinks}>
    { children }
  </CardActionsTotalStyled>
)