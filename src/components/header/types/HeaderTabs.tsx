import { CustomLink } from 'components/button/CustomLink'
import React, { FC } from 'react'
import { HeaderTabsStyled, HeaderTabStyled, HeaderTabsWrapperStyled } from '../styled'

export interface ITab {
  label: string;
  to: string;
  selected?: boolean;
}
export interface IHeaderTabsProps {
  items: ITab[];
}

export const HeaderTabs: FC<IHeaderTabsProps> = ({
  items,
}) => {

  return (
    <HeaderTabsWrapperStyled>
      <HeaderTabsStyled
        value={0}
        onChange={() => {}}
      >{items.map(o =>
        <CustomLink to={o.to} key={o.to}
          ><HeaderTabStyled selected={o.selected} label={o.label} /></CustomLink>)}
      </HeaderTabsStyled>
    </HeaderTabsWrapperStyled>
  )
}
