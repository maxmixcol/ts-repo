import { IPopupUserResult } from "compose/popup/interface";
import { ADD_LIKE_MUTATION, DELETE_LIKE_MUTATION } from "graphql/mutation";
import { POPUP_USER_QUERY } from "graphql/query";
import DateModel from "model/Date.model";
import UserInfoExModel, { IUserInfoExModel } from "model/UserInfoEx.model";
import { modals } from "setup/constants";
import useAuth from "use/auth/useAuth";
import { useControlledMutation, useQueryAll } from "use/fetch/useQuery";
import useI18n from "use/i18n/useI18n";
import { useModalUser } from "use/modal/useModal";
import useFullModal, { IProcess } from "../useFullModal";

const modalId = modals.user
const labelIds = {
  lastChange: 'popup.user.lastChange',
  lastChangeFinished: 'popup.user.lastChangeFinished',
  lastLogin: 'popup.user.lastLogin',
  like: 'popup.user.like',
  likes: 'popup.user.likes',
  numCollections: 'popup.user.numCollections',
  numChangesStarted: 'popup.user.numChangesStarted',
  numChangesFinished: 'popup.user.numChangesFinished',
  numChangesWith: 'popup.user.numChangesWith',
  numChangesNok: 'popup.user.numChangesNok',
  unanswered: 'popup.user.unanswered',
  visit: 'popup.user.visit'
}


const usePopupUser = () => useFullModal<IUserInfoExModel, IPopupUserResult>({
  labelIds,
  modalId,
  headerDark: true,
  process
})

export default usePopupUser


// process data

function process (data: IUserInfoExModel): IPopupUserResult & IProcess {
  const { tm } = useI18n()
  const { refetch } = useModalUser()
  const userId = useAuth().user
  const labels = tm(labelIds)
  const { username: name = '', avatar, iduser, unanswered = '' } = data || {}
  const variables = {
    userIds: [iduser],
    userId
  }
  // DATA MANAGEMENT
  const refresh = () => iduser && get(variables)
  
  const { get, loading, list } = useQueryAll<IUserInfoExModel>('userInfoEx', POPUP_USER_QUERY)
  const [ addLike, addResult ] = useControlledMutation({
    name: 'addLike',
    mutation: ADD_LIKE_MUTATION,
    refetch,
    success: refresh,
  })
  const [ deleteLike, deleteResult ] = useControlledMutation({
    name: 'deleteLike',
    mutation: DELETE_LIKE_MUTATION,
    refetch,
    success: refresh,
  })
  const image = UserInfoExModel.getAvatar(avatar)
  const item = list && list[0]

  const showDate = date => date && DateModel.dateDiff(date * 1000)
  const doLike = () => {
    if (item.mine) {
      deleteLike({ variables: { id: item.mine } })
    } else {
      addLike({ variables: {
        params: 
          { from: userId, to: iduser } }
      });
    }
  }

  const values = item ? {
    lastLogin: showDate(item.lastLogin),
    numCollections: item.numCollections || 0,
    numChangesStarted: item.numChangesStarted || 0,
    numChangesFinished: item.numChangesFinished || 0,
    numChangesWith: item.numChangesWith || 0,
    numChangesNok: item.numChangesNok || 0,
    likes: item.likes || 0,
    mine: item.mine,
    lastChange: showDate(item.lastChange),
    lastChangeFinished: showDate(item.lastChangeFinished),
    unanswered
  } : {}

  return {
    refresh,
    labels,
    values,
    showProgress: loading || addResult.loading || deleteResult.loading,
    name,
    image,
    iduser,
    doLike,
  }
}