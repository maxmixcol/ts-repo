import { IElementModel } from 'model/Element.model';
import { IUserElementModelEx } from 'model/UserElement.model';
import React, { FC } from 'react';
import { getSize } from 'utils/helpers';
import { IClassName } from 'utils/interface';
import { ElementBoxWrapper } from './styled';

export const ElementBox: FC<IElementModel & IUserElementModelEx & IClassName> = ({
  className,
  order,
  name,
  group,
  amount,
}) => {

  return (
    <ElementBoxWrapper className={className} size={getSize(name, 10, 20)}>
      <i>{ order }</i>
      <strong dangerouslySetInnerHTML={{ __html: name }} />
      { group && <span dangerouslySetInnerHTML={{ __html: group }} />}
      { amount > 1 ? <em>x{ amount }</em> : <></>}
    </ElementBoxWrapper>
  );
};
