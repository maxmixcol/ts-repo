import React, { FC } from 'react'
import { IChildrenOpt } from 'utils/interface'
import { PercentFullStyled, PercentStyled, PercentWrapperStyled } from './common.styled'

interface IPercent extends IChildrenOpt {
  value: number;
  big?: boolean;
}

export const PercentBar: FC<IPercent> = ({
  value,
  children = ''
}) => (
  <PercentWrapperStyled>
    <PercentStyled value={value} />
    <PercentFullStyled />
    <span>{value * 100}% {children}</span>
  </PercentWrapperStyled>
)