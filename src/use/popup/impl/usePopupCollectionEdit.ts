import { modals } from 'setup/constants'
import useI18n from 'use/i18n/useI18n'
import useFullModal, { IProcess } from '../useFullModal'
import { useQueryAll } from 'use/fetch/useQuery'
import UserCollectionExModel, { IUserCollectionExModel } from 'model/UserCollectionEx.model'
import { useEffect } from 'react'
import useAuth from 'use/auth/useAuth'
import { IPopupCollectionEditData } from './interface'
import { IPopupCollectionEditResult } from 'compose/popup/interface'
import { POPUP_COLLECTION_EDIT_QUERY } from 'graphql/query'


const modalId = modals.collectionEdit
const labelIds = {
  text: 'popup.collection.share.text',
  title: 'popup.collection.share.title',
  brand: 'common.url',
  faults: 'popup.collection.share.faults',
  repeated: 'popup.collection.share.repeated',
  loading: 'popup.collection.share.loading',
  description: 'popup.collection.share.description',
}

const usePopupCollectionEdit = () => useFullModal<IPopupCollectionEditData, IPopupCollectionEditResult>({
  labelIds,
  modalId,
  process
})

export default usePopupCollectionEdit

function process (data: IPopupCollectionEditData): IPopupCollectionEditResult & IProcess {
  const { t, tm } = useI18n()
  const labels = tm(labelIds)
  const userId = useAuth().user
  const { collection } = data || {}
  const { code, name } = collection || {}

  // DATA MANAGEMENT
  const elementsQuery = useQueryAll<IUserCollectionExModel>('userCollectionEx', POPUP_COLLECTION_EDIT_QUERY)
  const { elements = [] } = (elementsQuery.list || [])[0] || {}
  
  useEffect(() => {
    elementsQuery.get({
      codes: [code],
      userId,
      elements: true,
      userElements: true,
    })
  }, [code])

  const { mappedElements = {}, mappedUserElements = {} } = (UserCollectionExModel.toElementMaps(elementsQuery.list) || {})[code] || {}
  const showProgress = elementsQuery.loading

  return {
    showProgress,
    editContainerProps: {
      elements,
      albumProps: {
        elements: mappedElements,
        userElements: mappedUserElements,
        onClick: (a, b) => console.log('click', a, b) 
      },
      labels: {
        title: name,
        footer: labels.brand,
        faults: labels.faults,
        repeated: labels.repeated,
      }
    },
    loadingCollectionData: elementsQuery.loading,
    labels
  }
}