import styled, { css } from 'styled-components'
import { Hero } from 'components/hero/Hero'
import { FullModal } from 'components/modal/FullModal'
import { grayColor, successColor, whiteColor } from 'components/theme/constants'
import { randomColor } from 'utils/helpers'
import { CardRaised } from 'components/card/custom/CardRaised'
import { useMediaQuery } from '@material-ui/core'
import { PrimaryEmptyButton } from 'components/common/common.styled'
import { colorText } from 'components/common/styled'

export const CardRaisedStyled = styled(CardRaised)`
  margin-top: 0;
  border-radius: 0;
  height: auto;
  min-height: 70vh;
  padding-bottom: 1rem;
  margin: 0;
  ${({ theme }) => css`
    ${theme.breakpoints.up('md')} {
        width: calc(100% - 15rem);
        margin-top: 3rem;
        margin-bottom: 1rem;
        height: auto;
        box-shadow: none;
        background: transparent;
        .MuiAvatar-root {
          display: none;
        }
    }
  `}
`
export const FullModalStyled = styled(FullModal).attrs(({ theme, fullScreen }) => {
  const matches = useMediaQuery(theme.breakpoints.up('sm')) 
  return {
    fullScreen: !matches || fullScreen
  }
})`
  header.MuiPaper-root {
    box-shadow: none;
    color: ${whiteColor};
  }
`
export const HeroStyled = styled(Hero)`
  height: 40vh;

  margin-top: 0rem;
  background-color: ${({ name }) => randomColor(name)};

  &:after, &:before {
    display: none;
  }
  ${({ theme }) => css`
    ${theme.breakpoints.up('md')} {
      height: 15rem;
      width: 23rem;
      margin: 1rem;
    }
    ${theme.breakpoints.up('sm')} {
      width: 18rem;
    }
  `}

  ${({ reduced }) => reduced ? css`
    margin: 0rem;
    background-size: contain;
    background-repeat: no-repeat;
  ` : ''}
`

export const ListStyled = styled.ul`
  width: 100%;
  max-width: 20rem;
  margin: 0 auto;
  padding: 0;

  li:first-child {
    border-top-right-radius: 4px;
    border-top-left-radius: 4px;
  }
  li:last-child {
    margin-bottom: 0;
    border-bottom-right-radius: 4px;
    border-bottom-left-radius: 4px;
  }
`

export const ColorListItemStyled = styled.li`
  position: relative;
  display: flex;
  justify-content: space-between;
  padding: 0.4rem 0.8rem;
  margin-bottom: -1px;
  border: 1px solid ${grayColor[0]};
  ${props => colorText(props)}
`

export const PrimaryEmptyButtonStyled = styled(PrimaryEmptyButton)`
  color: ${successColor[0]}
`