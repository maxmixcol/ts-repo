import React from "react"
import { graphql } from "gatsby"
import { EmptyView } from "components/typography/view/common"
import { ILayout } from "components/layout/Layout"
import { MD } from "components/layout/styled"

export default function Template({
  data, // this prop will be injected by the GraphQL query below.
}) {
  const { markdownRemark } = data // data.markdownRemark holds your post data
  const { frontmatter, html } = markdownRemark
  return (
    <EmptyView variant={ILayout.CARD}>
      <h1>{frontmatter.title}</h1>
      <MD
          dangerouslySetInnerHTML={{ __html: html }}
      />
    </EmptyView>
  )
}
export const pageQuery = graphql`
  query($id: String!) {
    markdownRemark(id: { eq: $id }) {
      html
      frontmatter {
        slug
        title
      }
    }
  }
`