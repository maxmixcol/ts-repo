import React, { FC } from 'react';
import useI18n from 'use/i18n/useI18n';
import { IChildrenOpt } from 'utils/interface';
import { ChangeNumStyled, ChangeTotalsButtonsWrapperStyled, ChangeTotalStyled, ChangeTotalsWrapperStyled, ChangeTotalTitleStyled } from 'compose/popup/change/total/styled';
import { TotalError } from 'compose/popup/change/styled';

const labelIds = {
  totalOffered: 'popup.change.totalOffered',
  totalRequested: 'popup.change.totalRequested'
}

export interface IChangeTotalProps extends IChildrenOpt {
  offered: number;
  requested: number;
  errorMessage: string;
}


export const ChangeTotalCompose: FC<IChangeTotalProps> = ({
  offered,
  requested,
  errorMessage,
  children = ''
}) => {
  const labels = useI18n().tm(labelIds)

  return (
    <ChangeTotalStyled>
      <ChangeTotalsWrapperStyled>
        <ChangeTotalTitleStyled>
          {labels.totalRequested}
          <ChangeNumStyled
            error={!!errorMessage}
            big={requested}/></ChangeTotalTitleStyled>
        <ChangeTotalTitleStyled>
          {labels.totalOffered}
          <ChangeNumStyled
            big={offered}/></ChangeTotalTitleStyled>
      </ChangeTotalsWrapperStyled>
      { errorMessage && <TotalError>{ errorMessage }</TotalError>}
      <ChangeTotalsButtonsWrapperStyled>
        { children }
      </ChangeTotalsButtonsWrapperStyled>
    </ChangeTotalStyled>
  )
}