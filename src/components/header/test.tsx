import React, { FC } from 'react';
import { shallow } from 'enzyme';

import { Header } from './Header';
import { Button } from 'components/core';

import { IHeaderProps } from './Header'
import { withTheme } from 'setup/withTheme';

describe('Header Container', () => {
  const HeaderTest: FC<IHeaderProps> = ({ children }) => {
    return withTheme(<Header>{children}</Header>)
  }
  describe('Header container', () => {
    const mount = shallow(<HeaderTest><Button /></HeaderTest>);

    it('<Header /> test should work', () => {
      expect(mount.find('Header')).toHaveLength(0);
    });
    it('<Button /> test should work', () => {
      expect(mount.find('button')).toHaveLength(1);
    });
  });
});
