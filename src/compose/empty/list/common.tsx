import icons from 'components/theme/icons'
import React, { FC } from 'react'
import { routes } from 'setup/constants'
import useI18n from 'use/i18n/useI18n'

interface IProps {
  pre: string;
}


export const Empty: FC<IProps> = ({
  pre
 }) => {
  const { tm } = useI18n()
  const { title, subtitle, subtitle2, brand } = tm({
    title: `${pre}.text1`,
    subtitle: `${pre}.text2`,
    subtitle2: `${pre}.text3`,
    brand: 'common.brand'
  })

  return (
    <>
      { title !== `${pre}.text1` && <b>{ title }</b>}
      { subtitle !== `${pre}.text2` && <>
        <br/><br/>{ subtitle } <i>{ brand }</i>{ subtitle2 !== `${pre}.text3` && subtitle2 }</>}
    </>
  )
}


export const empty = {
  changes: {
    pre: 'empty.changes',
    href: routes.userChangeSearch,
    icon: icons.userChanges,
    content: <Empty pre={'empty.changes'} />
  },
  collections: {
    pre: 'empty.collections',
    href: routes.userCollectionsJoin,
    icon: icons.add,
    content: <Empty pre={'empty.collections'} />
  },
  newCollections: {
    pre: 'empty.newCollections',
    href: null,
    icon: icons.add,
    content: <Empty pre={'empty.newCollections'} />
  },
  register: {
    pre: 'empty.register',
    icon: icons.profile,
    content: <Empty pre={'empty.register'} />
  },
  searchChanges: {
    pre: 'empty.searchChanges',
    href: routes.userCollections,
    icon: icons.userCollections,
    content: <Empty pre={'empty.searchChanges'} />
  },
  userElements: {
    pre: 'empty.userElements',
    href: routes.userCollections,
    icon: icons.userCollections,
    content: <Empty pre={'empty.userElements'} />
  }
}