import React, { FC } from 'react'
import { Radio, RadioGroup } from 'components/core'
import { HiddenIcon, Icon } from 'components/styled'
import icons from 'components/theme/icons'
import { FilterSelect } from 'components/filter/FilterSelect'
import { IFilterModel } from 'use/filter/store/model/FilterModel'
import { FormControlLabelStyled, UncheckedIconStyled } from './styled'

export interface IProps {
  locked: string;
  items: IFilterModel[];
  onChangeLocked: (value: string) => void;
  onChangeValue: (name: string, input: any) => void;
}

export interface IFilter {
  key: string;
  label: string;
  selected?: boolean;
}


const checkedIcon = <Icon icon={icons.lock} />
const icon = <UncheckedIconStyled icon={icons.lock} />

export const FormSelectGroup: FC<IProps> = ({
  locked,
  items,
  onChangeLocked,
  onChangeValue,
}) => {
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    onChangeLocked(event.target.value)
  }
  return (
    <RadioGroup
      value={locked}
      onChange={handleChange}>
      {items.map(({ name, label, options, input }: IFilterModel) => {
        const select = options && (<FilterSelect
          label={label}
          value={input}
          onChange={value => onChangeValue(name, value)}
          options={options}
          />)
    
        return (<FormControlLabelStyled key={name}
          label={select}
          value={name}
          control={<Radio checkedIcon={checkedIcon} icon={icon}/>}
          />)
      })}
    </RadioGroup>
    )
}
