import React, { useEffect } from 'react'
import { routes } from 'setup/constants'

import useI18n from 'use/i18n/useI18n'
import useAuth from 'use/auth/useAuth'
import { usePageQuery } from 'use/fetch/useQuery'

import { CardCollectionDashboardHomeCompose } from 'compose/card/collection/CardCollectionDashboardHomeCompose'
import { CardChangeDashboardHomeCompose } from 'compose/card/change/CardChangeDashboardHomeCompose'
import { DashboardView } from 'components/typography/view/dashboard/DashboardView'
import { JoinNewCollectionButton, SearchNewChangesButton } from 'components/typography/view/dashboard/common'
import { HeaderLogo, HeaderMenuAndUser, ShowListButton, Title } from 'components/common/common'
import { NavLeft, NavRight } from 'components/header/styled'
import { LoadMoreButton } from 'components/filter/common'
import { HeaderTitle, ListHorizontal } from 'components/common/common.styled'
import { PopupChangeCompose } from 'compose/popup/PopupChangeCompose'
import { useModalChange, useModalCollection, useModalCollectionEdit, useModalCollectionShare, useModalUser } from 'use/modal/useModal'
import { PopupUserCompose } from 'compose/popup/PopupUserCompose'
import { EmptyDashboardCompose } from 'compose/empty/EmptyDashboardCompose'
import { DASHBOARD_HOME_COLLECTIONS_QUERY, DASHBOARD_HOME_CHANGES_QUERY, DASHBOARD_HOME_CHANGES_COMPLETED_QUERY } from 'graphql/query'
import { HeaderMenu } from 'compose/header/HeaderMenu'
import { ChangeState, IUserChangeModel } from 'model/UserChange.model'
import { PopupCollectionCompose } from 'compose/popup/PopupCollectionCompose'
import { IUserExchangeModel } from 'model/UserExchange.model'
import { mapperDashboardActiveChanges, mapperDashboardCollections } from 'model/mapper'
import { IUserCollectionExModel } from 'model/UserCollectionEx.model'
import { useLayoutStore } from 'use/layout/useLayout'
import { PopupAlertWelcomeCompose } from 'compose/popup/PopupAlertWelcomeCompose'
import { PopupCollectionShareCompose } from 'compose/popup/PopupCollectionShareCompose'
import { PopupCollectionEditCompose } from 'compose/popup/PopupCollectionEditCompose'
import { HeaderUser } from 'compose/header/HeaderUser'
import { HeaderSearch } from 'compose/header/HeaderSearch'

const labelIds = {
  pageTitle: 'pages.dashboard.title',
  changesTitle: 'pages.dashboard.header.mychanges',
  collectionsTitle: 'pages.dashboard.header.mycollections',
  showMore: 'pages.dashboard.button.showlist',
  loadMore: 'filter.loadmore',
  joinCollection: 'pages.dashboard.button.joincollection',
  findNew: 'pages.dashboard.button.findnew',
}

const DashboardHomePage = ({ location }) => {
  const userId = useAuth().user
  const {
    pageTitle,
    changesTitle,
    collectionsTitle,
    showMore,
    loadMore,
    joinCollection,
    findNew
  } = useI18n().tm(labelIds)
  const { updatePageTitle } = useLayoutStore()
  useEffect(() => {
    updatePageTitle(pageTitle)
  }, [])

  const { isOpen: isUserOpen } = useModalUser()
  const { open: openChange, isOpen: isChangeOpen } = useModalChange()
  const onClickChange = (exchange: IUserExchangeModel) => {
    openChange({ exchange, isNew: false }, {
      refetch: [DASHBOARD_HOME_CHANGES_QUERY, 'DashboardHomeChangesQuery']
    })
  } 
	const { isOpen: isCollectionShareOpen } = useModalCollectionShare()
  const { open: openCollection, isOpen: isCollectionOpen } = useModalCollection()
  const onClickCollection = (item: IUserCollectionExModel) => {
    collectionsQuery.get({
      userId,
      userElements: true
    }, 0)
    const { code, name } = item.userCollection.collection
    openCollection({ code, name }, {
      refetch: [DASHBOARD_HOME_COLLECTIONS_QUERY, 'DashboardHomeCollectionsQuery']
    })
  }

  const collectionsQuery = usePageQuery<IUserCollectionExModel>('userCollectionExPage', DASHBOARD_HOME_COLLECTIONS_QUERY)
  const activeChangesQuery = usePageQuery<IUserExchangeModel>('userExchangesPage', DASHBOARD_HOME_CHANGES_QUERY, { direction: 'DESC' })  
  const completedChangesQuery = usePageQuery<IUserChangeModel>('userChangesPage', DASHBOARD_HOME_CHANGES_COMPLETED_QUERY, { per_page: 1, direction: 'DESC' })

  useEffect(() => {
    if (userId) {
      activeChangesQuery.get({
        userId,
        state: ChangeState.PROGRESS
      })
      completedChangesQuery.get({
        userId,
        state: ChangeState.COMPLETE
      })
      collectionsQuery.get({
        userId,
        userElements: true
      })
    }
  }, [ userId ])

  const loading = collectionsQuery.loading || activeChangesQuery.loading || completedChangesQuery.loading

  const emptyCollections = !loading && collectionsQuery.list?.length === 0
  const emptyChanges = !loading && activeChangesQuery.list?.length === 0 && completedChangesQuery.list?.length === 0
  const emptyUserElements = !loading && false

  const showChanges = collectionsQuery.list?.length > 0
  const showUser = !emptyCollections

  return (
    <DashboardView
      location={location}
      route={routes.userDashboard}
      header={
        <>
          <NavLeft><HeaderLogo /></NavLeft>
          <HeaderTitle>{pageTitle}</HeaderTitle>
          <NavRight>
            { showUser && <HeaderUser />}
            <HeaderMenu />
          </NavRight>
        </>
      }>
      { !collectionsQuery.loading &&
        (emptyChanges ||emptyCollections || emptyUserElements)
        ? <EmptyDashboardCompose 
            emptyChanges={emptyChanges}
            emptyCollections={emptyCollections}
            emptyUserElements={emptyUserElements}
          />
        : '' }
      { showChanges && <>
        <Title value={changesTitle}>
          <ShowListButton linkTo={routes.userChanges}>{showMore}</ShowListButton>
        </Title>
        <ListHorizontal
          list={activeChangesQuery.list?.map(mapperDashboardActiveChanges)}
          onClick={item => onClickChange(item)}
          item={CardChangeDashboardHomeCompose}>
          <SearchNewChangesButton linkTo={routes.userChangeSearch}>
            {findNew}
          </SearchNewChangesButton>
          { !!activeChangesQuery.fetchMore && <LoadMoreButton onClick={activeChangesQuery.fetchMore}>{loadMore}</LoadMoreButton>}
        </ListHorizontal>
        </>}
      <Title value={collectionsTitle}>
        <ShowListButton linkTo={routes.userCollections}>{showMore}</ShowListButton>
      </Title>
      <ListHorizontal
        list={collectionsQuery.list?.map(mapperDashboardCollections)}
        onClick={item => onClickCollection(item)}
        item={CardCollectionDashboardHomeCompose}>
        <JoinNewCollectionButton linkTo={routes.userCollectionsJoin}>
          {joinCollection}
        </JoinNewCollectionButton>
        { !!collectionsQuery.fetchMore && <LoadMoreButton onClick={collectionsQuery.fetchMore}>{loadMore}</LoadMoreButton>}
      </ListHorizontal>
      { isChangeOpen && <PopupChangeCompose />}
      { isCollectionOpen && <PopupCollectionCompose />}
      { isCollectionShareOpen && <PopupCollectionShareCompose />}
      { isUserOpen && <PopupUserCompose />}
      <PopupAlertWelcomeCompose />
    </DashboardView>
  )
}
export default DashboardHomePage
