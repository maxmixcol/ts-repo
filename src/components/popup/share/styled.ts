import styled, { css } from 'styled-components'
import { MiniCard } from 'components/popup/change/element/styled'
import { AlbumStyled, MaxiListWrapper } from 'compose/popup/change/styled'
import { dangerColor, facebookColor, grayColor, hexToRgb, successColor, twitterColor, whatsappColor, whiteColor } from 'components/theme/constants'
import { ElementBox } from 'components/popup/change/element/ElementBox'
import UserElementModel, { IUserElementModelTypeEnum } from 'model/UserElement.model'
import shadows from 'components/theme/shadows'
import { buttonBackground } from 'components/common/styled'
import { Text1Styled } from 'compose/popup/cookies/styled'
import { CardCollectionSkeleton } from 'components/card/styled'

const shareWidth = '62rem'

export type IElementType = 'fault' | 'repeated' | ''
const getElementBackgroundColor = (variant: IUserElementModelTypeEnum) => {
  return {
    [IUserElementModelTypeEnum.FAULT]: dangerColor[8],
    [IUserElementModelTypeEnum.REPEATED]: successColor[8],
    [IUserElementModelTypeEnum.NONE]: 'inherit'
  }[variant]
}
export const ElementBoxShareStyled = styled(ElementBox).attrs(props => ({
  variant: UserElementModel.getTypeVariant(props.type)
}))`
  height: 2.35rem;
  line-height: 0.5rem;
  font-size: 0.5rem;
  overflow: hidden;
  padding: 1px;
  & strong {
    font-size: 0.52rem;
    width: 2.5rem;
  }
  & span {
    font-size: 0.4rem;
    margin: 0 -1px;
  }
  ${({ variant }) => css`
    background-color: ${getElementBackgroundColor(variant)};
    border: 1px solid;
  `}
`
export const ElementBoxMiniCardShareStyled = styled(ElementBoxShareStyled)`
  height: 1rem;
  width: auto;
  font-size: 0.5rem;
  line-height: 0.4rem;
  padding: 2px;
  
  & strong {
    display: none;
  }
  & span {
    display: none;
  }
  border-color: ${grayColor[6]};

`
export const AlbumShareStyled = styled(AlbumStyled).attrs(props => ({
  item: ElementBoxShareStyled
}))`
  display: grid;

  grid-template-columns: repeat(3, 1fr);
  grid-column-gap: 3px;
  grid-row-gap: 3px;

  height: 8rem;
  overflow: auto;
  margin: 0;
`
export const AlbumShareTextStyled = styled(AlbumStyled).attrs(props => ({
  item: ElementBoxMiniCardShareStyled
}))`
  display: flex;
  flex-wrap: wrap;

  margin: 0;

  > li {

  }
`
export const HiddenCanvasStyled = styled.div`
  position: absolute;
  width: ${shareWidth};
  background-color: ${whiteColor};
  left: 10000px;
  ${({ hidden }) => hidden && css`
    display: none;
  `}
`
export const ImgContainerStyled = styled.div`
  overflow: scroll;
  width: 100%;
  & img {
    width: auto;
  }
`

export const MiniCardShareStyled = styled(MiniCard)`
  height: 2.35rem;
  line-height: 0.5rem;
  font-size: 0.5rem;
  overflow: hidden;
`
export const ShareDescriptionStyled = styled(Text1Styled)`
  padding: 1rem;
  text-align: justify;
`
export const ShareButtonsWrapperStyled = styled.div`
  padding: 1rem;
  display: flex;
  list-style: none;

  & button {
    margin: 0 0 0 0.5rem;
    height: 2rem;
    width: 2rem;
    border-radius: 50%;
    border: 0;
    color: ${whiteColor};

    &.facebook {
      ${buttonBackground({ color: 'facebook' })};
    }
    &.twitter {
      ${buttonBackground({ color: 'twitter' })};
    }
    &.whatsapp {
      ${buttonBackground({ color: 'whatsapp' })};
    }
  }
`
export const ShareContainerStyled = styled.div`
  border: 1px solid;
`
export const ShareContainerWrapperStyled = styled.div`
  position: relative;
  height: 60vh;
  overflow: auto;
  border: 1px solid ${grayColor[0]};

`

export const ShareTitleStyled = styled.p`
  width: ${shareWidth};
  padding: 0.5rem 1rem;
  margin: 0;
  font-style: italic;
`

export const ShareWrapperStyled = styled.ul`
  width: ${shareWidth};
  border-top: 1px solid ${grayColor[0]};
  margin: 0 0.2rem;
  padding: 1rem;
  list-style: none;

  ${({ variant }) => variant === 'list' ?
    css`
      display: flex;
      flex-direction: column;
      & > li {
        margin-bottom: 0.75rem;
        &:last-of-type {
          margin-bottom: 0rem;
        }
      }
    `
    : 
    css`
      display: grid;
      grid-template-columns: repeat(8,1fr);
      grid-column-gap: 3px;
      grid-row-gap: 3px;
      & > li {
        width: 7rem;
      }
    `
  } 

`
export const MaxiListWrapperShare = styled(MaxiListWrapper)`
  
  & > li {
    span {
      height: 2rem;
      overflow: hidden;
    }
  }
  ${({ variant }) => variant === 'list' ?
     css`
      margin-top: 0.2rem;
     `
     : css`
        margin-top: 0.2rem;
        margin-bottom: 0.2rem;
        &, & > ul {
          height: auto;
          max-height: 10rem;
        }
    `}
`

export const GroupStyled = styled.p`
  margin: 0;
  height: 0.6rem;
  width: 14rem;
  font-size: 0.5rem;
`
