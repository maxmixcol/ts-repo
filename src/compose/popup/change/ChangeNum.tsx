import React, { FC } from 'react';
import { Icon } from 'components/styled';
import icons from 'components/theme/icons';
import { NumberWrapper, NumberSmall, NumberBig, NumberWrapperTotal } from './styled';


interface IProps {
  big: number;
  small?: number;
  error?: boolean;
  className?: string;
}
export const ChangeNum: FC<IProps> = ({
  big,
  small = -1,
  error,
  className
}) => {
  const isOnlyTotal = small < 0
  const Wrapper = isOnlyTotal
    ? NumberWrapperTotal
    : NumberWrapper

  return (
    <Wrapper className={className} red={error}>
      { !isOnlyTotal && 
        <>
          <NumberSmall>{ small }</NumberSmall>
          <Icon icon={icons.numSep} />
        </>}
      <NumberBig>{ big }</NumberBig>
    </Wrapper>
  );
};
