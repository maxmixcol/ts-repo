export const getChange = (state:any) => {
  return state.exchange.change;
}
export const getExchanges = (state:any) => {
  return state.exchange.exchanges;
}