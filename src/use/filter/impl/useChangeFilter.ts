import { TFilterOptionUpdate } from "compose/filter/FilterCompose";
import { ChangeFilter, ChangeState } from "model/UserChange.model";
import { IUserExchangeModel } from "model/UserExchange.model";
import moment, { Moment } from "moment";
import { useEffect, useState } from "react";
import { filters } from "setup/constants";
import useI18n from "use/i18n/useI18n"
import { ISortModelMap } from "utils/interface";
import FilterModel, { IFilterModelMap, TFilterMapper } from "../store/model/FilterModel";
import SortModel from "../store/model/SortModel";
import SortTypeEnum from "../store/model/SortTypeEnum";
import useFilter from "../useFilter";
interface ISearchData {
  changeDays: number;
  loginDays: number;
  name: string;
}
interface IUserExchangeModelParsed extends IUserExchangeModel {
  searchData: ISearchData
}

const filterId = filters.change

/*
- ordenar por: estado/fecha/nombre usuario
- buscar por: nombre de usuario
- filtros:
  - rango de fechas
  - en progreso/completados/cancelados
*/

/* SORT */
const sortLabelIds = {
  state: 'filter.sort.state',
  date: 'filter.sort.date',
  access: 'filter.sort.access',
  collector: 'filter.sort.collector',
}
enum ChangeSort {
  STATE = 'state',
  DATE = 'date',
  ACCESS = 'access',
  COLLECTOR = 'collector'
} 
const SORT_MAP: ISortModelMap = {
  [ChangeSort.STATE]: [
    { name: 'change.stateId', type: SortTypeEnum.Number, asc: true },
  ],
  [ChangeSort.ACCESS]: [
    { name: 'searchData.loginDays', type: SortTypeEnum.Date, asc: false },
    { name: 'searchData.changeDays', type: SortTypeEnum.Date, asc: false },
    { name: 'change.stateId', type: SortTypeEnum.Number, asc: true },
  ],
  [ChangeSort.DATE]: [
    { name: 'searchData.changeDays', type: SortTypeEnum.Date, asc: false },
    { name: 'searchData.loginDays', type: SortTypeEnum.Date, asc: false },
    { name: 'change.stateId', type: SortTypeEnum.Number, asc: true },
  ],
  [ChangeSort.COLLECTOR]: [
    { name: 'searchData.name', type: SortTypeEnum.String, asc: true },
    { name: 'change.stateId', type: SortTypeEnum.Number, asc: true },
  ]
}
/* FILTER */
const labelIds = {
  byCollector: 'filter.by.collector',
  byCollection: 'filter.by.collection',
  byState: 'filter.by.state',
  searchChange: 'filter.by.searchChange',
  stateInProgress: 'filter.label.changeState.inprogress',
  stateCompleted: 'filter.label.changeState.complete',
  stateCancelled: 'filter.label.changeState.cancelled'
}
const { SORT, SEARCH, STATE, COLLECTOR, COLLECTION } = ChangeFilter

const FILTER_MAP: IFilterModelMap = {
  [STATE]: { name: STATE, label: labelIds.byState, input: ChangeState.PROGRESS, server: true },
  [COLLECTOR]: { name: COLLECTOR, label: labelIds.byCollector, input: '', hideFilter: true },
  [COLLECTION]: { name: COLLECTION, label: labelIds.byCollection, input: '' },
  [SEARCH]: { name: SEARCH, label: labelIds.searchChange, input: '' },
  [SORT]: { name: SORT, label: '', input: ChangeSort.ACCESS },
}
const getSearchData = (item: IUserExchangeModel, now: Moment): ISearchData => {
  const loginDays = moment(item.user.lastLogin * 1000).diff(now, 'days')
  const changeDays = moment(item.change.endDate).diff(now, 'days')
  const name = FilterModel.flatName(item.user.username)

  return { loginDays, changeDays, name }
}
const updateOptions = (list: IUserExchangeModelParsed[], t: any): TFilterOptionUpdate[] => {
  const searchOptions = {}
  const stateOptions = [
    { title: t(labelIds.stateInProgress), value: ChangeState.PROGRESS },
    { title: t(labelIds.stateCompleted), value: ChangeState.COMPLETE },
    { title: t(labelIds.stateCancelled), value: ChangeState.CANCELLED }
  ]
  const collectionOptions = {}
  const collectorOptions = {}

  list.forEach(item => {
    const { user, exchanges, searchData } = item
    collectorOptions[user.iduser] = { title: user.username, value: user.iduser }
    exchanges.forEach(({ collection }) => {
      if (collection && collection.code) {
        collectionOptions[collection.code] = { title: collection.name, value: collection.code }
      }
    })
    
    searchOptions[user.iduser] = searchData.name
  })
  return [
    { name: STATE, options: stateOptions },
    { name: COLLECTOR, options: FilterModel.EMPTY.concat(SortModel.sortOptions(collectorOptions)) },
    { name: COLLECTION, options: FilterModel.EMPTY.concat(SortModel.sortOptions(collectionOptions)) },
    { name: SEARCH, query: (value: any, input: any) => searchOptions[value].indexOf(input) !== -1 },
  ]
}
const filterMapper: TFilterMapper<IUserExchangeModel> = (
  { change, user, exchanges },
  { name, input, query }
) => {
  return {
    [SEARCH]: query && input && query(user.iduser, input),
    [STATE]: true,
    // [STATE]: ({ // no se si es necesario
    //   [ChangeState.PROGRESS]: change.stateId < 7,
    //   [ChangeState.COMPLETE]: change.stateId >= 7 && change.stateId < 10,
    //   [ChangeState.CANCELLED]: change.stateId > 10
    // })[input],
    [COLLECTOR]: user.iduser === input,
    [COLLECTION]: !!exchanges.find(e => e.collection?.code === input),
  }[name]
}


const useChangeFilter = () => {
  const { t, tm } = useI18n()
  const sortLabels = tm(sortLabelIds)

  const {
    getFilter,
    getInit,
    onChange,
    updateFilter,
    values,
  } = useFilter(filterId)

  const init = () => {
    useEffect(() => { getInit(FILTER_MAP, t, ChangeSort.ACCESS)}, [])
  }

  const process = (list: IUserExchangeModel[]) => {
    let listIds = list.map(({ change }) => change.id).sort()
    const [ parsedList, setParsedList ] = useState<IUserExchangeModelParsed[]>()

    useEffect(() => {
      const sortFilterOptions = SortModel.updateSortOptions({
        sortMap: SORT_MAP, sortLabels
      })
      const now = moment()
      const parsedList = list
        .filter(item => item.user) // there are changes with a user that is not anymore
        .map(item => ({ ...item, ...{ searchData: getSearchData(item, now) }}))
      updateFilter(SORT, { options: sortFilterOptions })
      updateOptions(parsedList, t).forEach(o => updateFilter(o.name, o))
      setParsedList(parsedList)
    }, [ listIds.join() ])
    
    return {
      model: getFilter || [],
      list: parsedList
    }
  }

  const { state, user } = values()
  
  useEffect(() => {
    if (state) {
      updateFilter(SEARCH, { disabled: false })
    }
    if (!state && !user) {
      updateFilter(STATE, { input: ChangeState.PROGRESS })
    }
  }, [ state, user ])

  return {
    init,
    process,
    filterProps: {
      onChange,
      filterId,
      updateOptions,
      filterMapper,
      sortMap: SORT_MAP,
      sortName: SORT,
      searchName: SEARCH
    },
    updateFilter,
    values
  }  
}

export default useChangeFilter