import { IUserElementModel } from "./UserElement.model";

export interface IElementModel {
  code: number; // CodigoC
  order: string; // Orden
  number: number; // Numero
  name: string; // Nombre
  nameEx: string; // NombreL
  group: string; // Grupo
  type: string; // Tipo
  comment: string; // Comentario
  comment2: string; // Comentario2
  imgDate: string; // Fimg
  userData: IUserElementModel;
}

export default class ElementModel {

/*
  Elemento
  {
    "CodigoC":214
    "Numero":1,
    "Nombre":"\u00c1lbum vac\u00edo",
    "Orden":"AlbVacio",
    "Grupo":"",
    "Tipo":"Albumes y sobres",
    "Comentario":"",
    "Comentario2":"",
    "NombreL":"",
    "Fimg":"0000-00-00"
  }
*/
  // static getTitle = (element: IElementModel) => {
  //   var PROPS = {
  //     Orden: function (v) {
  //       return v + ': ';
  //     },
  //     Comentario: function (v) {
  //       var ed = parseInt((v || '').replace('Edición ', ''));
  //       if (ed && ed !== 1) {
  //         return ('(Ed.' + ed + ')');
  //       }
  //       return '';
  //     },
  //     Nombre: function (v) {
  //       return v || '';
  //     },
  //     Grupo: function (v) {
  //       return v ? '<br/>(' + v + ')' : '';
  //     }
  //   };

  //   return {
  //     CodigoC: item.CodigoC,
  //     Numero: item.Numero,
  //     Nombre: item.Nombre + (item.Comentario2 ? ' (' + item.Comentario2 + ')' : ''),
  //     Orden: item.Orden,
  //     Grupo: item.Grupo,
  //     Tipo: item.Tipo,
  //     Comentario: item.Comentario
  //   };
  // }
}