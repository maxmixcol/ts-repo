import styled, { css } from 'styled-components'
import { StepContent, Stepper } from 'components/core'
import { TextDefault } from 'components/typography'
import { RawButtonWithIcon } from 'components/button/RawButtonWithIcon'

export const StepIconWrapper = styled.div`
  background-color: #ccc;
  z-index: 1;
  color: #fff;
  width: 2rem;
  height: 2rem;
  display: flex;
  border-radius: 50%;
  justify-content: center;
  align-items: center;

  ${({ active }) => active && css`
    background-image: linear-gradient( 136deg, rgb(242,113,33) 0%, rgb(233,64,87) 50%, rgb(138,35,135) 100%);
    box-shadow: 0 4px 10px 0 rgba(0,0,0,.25);`}

  ${({ completed }) => completed && css`
    background-image: linear-gradient( 136deg, rgb(242,113,33) 0%, rgb(233,64,87) 50%, rgb(138,35,135) 100%);
    `}
`
export const StepContentStyled = styled(StepContent)`
  
`
export const StepperStyled = styled(Stepper)`
  background: transparent;
  padding: 0;
  .MuiStep-root {
    font-size: 0.7rem;
  }
  .MuiStepContent-root {
    margin-bottom: 1rem;
  }
`
export const TextDefaultStyled = styled(TextDefault)`
`
export const ButtonWithIconStyled = styled(RawButtonWithIcon)`
  margin-top: 1rem;
`