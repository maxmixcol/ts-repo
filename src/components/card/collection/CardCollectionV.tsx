import { PercentBar } from 'components/common/PercentBar';
import React, { FC, memo } from 'react';
import { CardFooterStyled } from '../change/styled';
import { CardBlockStyled } from '../styled';
import { ICardCollection } from './CardCollection';
import CollectionName from './CollectionName';

import {
  ActionBlockWrapper,
  AvatarCollectionVStyled,
  CardCategoryWhite,
  CardWrapperV,
} from './styled';


const CardCollectionVMemo: FC<ICardCollection> = ({
  image,
  name,
  fav,
  sleep,
  children = ''
}) => {

  return (
    <CardWrapperV>
      <AvatarCollectionVStyled src={image} alt={name} />
      <ActionBlockWrapper>
        <CardBlockStyled>
          <CollectionName name={name} variant={'vertical'} fav={fav} sleep={sleep} />
        </CardBlockStyled>
        <CardFooterStyled>
          <CardCategoryWhite>
            {children}
          </CardCategoryWhite>
        </CardFooterStyled>
      </ActionBlockWrapper>
    </CardWrapperV>
  );
};

export const CardCollectionV = memo(CardCollectionVMemo);
