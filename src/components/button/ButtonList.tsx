import React, { FC, memo, useEffect, useState } from 'react';
import { ButtonListItemStyled, ButtonListWrapper, SwitchStyled } from './styled';
import { RawButtonWithIcon, IButtonProps } from 'components/button/RawButtonWithIcon';
import { PrimaryButton, SecondaryButton, TransparentButton } from 'components/common/common.styled';
import { Text1 } from 'components/typography';
import { HeroColor } from 'components/hero/Hero';
import { ISimpleButtonProps } from 'utils/interface';

export enum IButtonTypes {
  'default',
  'primary',
  'secondary'
}

export interface IButtonListProps extends IButtonProps {
  color?: HeroColor;
  toggle?: boolean;
  message?: string;
  type?: IButtonTypes;
  list?: ISimpleButtonProps[];
}
interface IProps {
  buttons: IButtonListProps[];
}
const ButtonListMemo: FC<IProps> = ({
  buttons
}) => {
  return (
    <ButtonListWrapper>
      { buttons.map(({ toggle, message, children, type, list, color, ...rest}, i: number) => (
      <ButtonListItemStyled color={color} key={i}>
        { list 
          ? (
            <>
              <RawButtonWithIcon item={Text1} {...rest} >{ children }</RawButtonWithIcon>
              <ul>{list.map(({ onClick, label }) =>
                <li key={label}>
                    <RawButtonWithIcon item={getItem(type)} onClick={onClick} {...rest}>
                      { label }
                    </RawButtonWithIcon>
                </li>
                )}
              </ul>
            </>
          )
        : 
        <RawButtonWithIcon  item={getItem(type)} {...rest} >
          { message
            ? <em>{children} <small>{message}</small></em> 
            : children
          }
          { toggle !== undefined && <CustomSwitch checked={toggle} />}
        </RawButtonWithIcon>
      }
      </ButtonListItemStyled>))}
    </ButtonListWrapper>
  );
};

export const ButtonList = memo(ButtonListMemo)

function getItem(type: IButtonTypes = IButtonTypes.default) {
  return {
    [IButtonTypes.default]: TransparentButton,
    [IButtonTypes.primary]: PrimaryButton,
    [IButtonTypes.secondary]: SecondaryButton,
  }[type]
}

interface ISwitchProps {
  checked: boolean;
  disabled?: boolean;
}
const CustomSwitch: FC<ISwitchProps> = ({
  checked,
  disabled = false
}) => {
  const [ isDisabled, setDisabled ] = useState(disabled)
  
  useEffect(() => {
    setDisabled(false)
  }, [ checked ])
  const handleClick = e => {
    e.preventDefault()
    setDisabled(true)
  }
  return <SwitchStyled checked={checked} disabled={isDisabled} onClick={handleClick} />
}