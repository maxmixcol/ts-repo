import { PrimaryEmptyButton } from 'components/common/common.styled'
import React, { FC } from 'react'

import useAuth from 'use/auth/useAuth'
import useI18n from 'use/i18n/useI18n'

interface ILogin {
  onClick: () => void;
}
export const HeaderLogin: FC<ILogin> = ({
  onClick
}) => {
  const user = useAuth().user
  const label = useI18n().t('menu.login')
  return (
    <>{ !user && <PrimaryEmptyButton onClick={onClick}>{ label }</PrimaryEmptyButton>}</>
  )
}