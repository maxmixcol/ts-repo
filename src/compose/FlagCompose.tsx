import React, { FC } from 'react';
import { Tooltip } from 'components/core';
import useI18n from 'use/i18n/useI18n';
import UserInfoModel from 'model/UserInfo.model';

interface IProps {
  country: string;
  className?: string;
}

export const FlagCompose: FC<IProps> = ({ country, className }) => { 
  const { t } = useI18n()
  const name = t(`list.country.${country}`)
  const src = UserInfoModel.getFlag(country)
  return (
    <Tooltip title={name}>
      <img className={className} title={name} src={src} />
    </Tooltip>
  );
}
