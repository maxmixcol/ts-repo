import styled, { css } from 'styled-components';
import { infoColor } from 'components/theme/constants';

export const AlertWelcomeHeaderStyled = styled.h3`
  margin: 0;
  color: ${infoColor[0]};
`
