export const getFiltered = (id: string) => (state:any) => state.filter.filtered[id]
export const getSorted = (id: string) => (state:any) => state.filter.sorted[id]
