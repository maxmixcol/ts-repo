import { UserDataButton } from 'components/card/custom/styled';
import { ChangeTitle } from 'compose/popup/change/total/styled';
import React, { FC, useState } from 'react';
import { IChildren, IMappedElementMap, IMenuLink, IReadonly } from 'utils/interface';
import { ChangeCollection, IChangeCollectionListProps, INumberLabel, ISelect } from '../ChangeCollection';
import { ActionLinksWrapperStyled, BlockLarge, CardActionsStyled, ViewModeSelectorStyled } from '../styled';
import { ViewMode } from '../ViewModeSelector';

interface IChangeCardProps {
  actionLinks: IMenuLink[];
  header: React.ReactNode;
  requested: IChangeCardCollection & ISelect;
  offered: IChangeCardCollection & ISelect;
  elementData: IMappedElementMap;
  showViewMode: boolean;
  showWrapper: boolean;
}
interface IUserBlock {
  title: string;
  list: IChangeCollectionListProps[];
}

export type IChangeCardCollection  = IUserBlock & ISelect;
export type IChangeData = IChangeCardProps & IChildren & IReadonly & INumberLabel;

const { SMALL, BIG } = ViewMode

export const ChangeCard: FC<IChangeData> = ({
  actionLinks,
  header,
  requested,
  offered,
  elementData,
  readonly,
  showViewMode,
  showWrapper,
  children,
  sortFn,
  labelFn,
}) => {
  const [ mode, setMode ] = useState<ViewMode>(SMALL)
  const onChange = () => {
    const newMode = mode === SMALL ? BIG : SMALL
    setMode(newMode)
  }
  
  const UserBlock: FC<IUserBlock & ISelect> = ({
    title,
    onSelect,
    list
  }) => (
    <>
      <ChangeTitle>{title}</ChangeTitle>
      {list.map((col) => (
        <ChangeCollection
          key={col.code}
          code={col.code}
          name={col.name}
          variant={mode}
          selected={sortFn(col.selected)}
          unselected={sortFn(col.unselected)}
          elements={elementData[col.code]?.mappedElements}
          readonly={readonly}
          onSelect={onSelect}
          sortFn={sortFn}
          labelFn={labelFn}/>
      ))}
    </>
  )

  const change = (
    <>
      <UserBlock title={requested.title} list={requested.list}
        onSelect={requested.onSelect} />
      <UserBlock title={offered.title} list={offered.list}
        onSelect={offered.onSelect} />
    </>
  )

  const collections = (
    <>
    { children }
    { showViewMode && <ViewModeSelectorStyled mode={mode} onChange={onChange}/>}
    { change }
    </>)

  return showWrapper
    ? (
      <BlockLarge>
        <CardActionsStyled variant='invert' color='secondary' buttonColor='info'
          content={header}
          anchorLinks='top'
          actionLinks={actionLinks}>
          { collections }
        </CardActionsStyled>
      </BlockLarge>
    )
    : <>
        <ActionLinksWrapperStyled>
          {actionLinks.map((item, i) => (
            <UserDataButton key={i}
              color='info'
              icon={item.icon}
              disabled={item.disabled}
              onClick={item.onClick}
              variant={item.variant}
            >{item.text}</UserDataButton>
            ))
          }
        </ActionLinksWrapperStyled>
        { collections }
      </>
}

