import React, { FC } from 'react'

import {
  ContArrow,
  ContLeft,
  ContRight,
  LeftRightWrapper,
} from './styled'


interface IProps {
  left: React.ReactNode;
  right: React.ReactNode;
}

export const LeftRight: FC<IProps> = ({
  left,
  right
}) => {
  return (
    <LeftRightWrapper>
      <ContLeft>{left}</ContLeft>
      <ContArrow />
      <ContRight>{right}</ContRight>
    </LeftRightWrapper>
  )
}
