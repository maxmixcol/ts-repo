/**
 * React, Gatsby, Jest, TypeScript, Apollo - Starter
 * https://github.com/eduard-kirilov/gatsby-ts-apollo-starter
 * Copyright (c) 2020 Eduard Kirilov | MIT License
 */
import React, { FC, memo, useEffect } from 'react';
import { IChildren } from 'utils/interface';

import { Fade } from '@material-ui/core';
import { DialogStyled, ModalWrapper } from './styled';
import useLayout from 'use/layout/useLayout';

interface IProps {
  handleClose?: () => void;
  open?: boolean;
}

const ModalMemo: FC<IProps & IChildren> = ({
  children,
  handleClose,
  open = false,
}) => {
  const { openModal , closeModal } = useLayout()
  useEffect(() => {
    openModal()
  }, [open])

  return (
    <DialogStyled
      aria-labelledby="transition-modal-title"
      aria-describedby="transition-modal-description"
      open={open}
      onClose={() => {
        closeModal()
        handleClose()
      }}
    >
      <Fade in={open}>
        <ModalWrapper>{children}</ModalWrapper>
      </Fade>
    </DialogStyled>
  )
}

export const Modal = memo(ModalMemo);
