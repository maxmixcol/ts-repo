export const isFirstLoad = (state:any) => state.layout.firstLoad
export const isSpinner = (state:any) => state.layout.spinner
export const getTitle = (state:any) => state.layout.pageTitle