import React, { FC, Suspense } from 'react'

import usePopupCollectionShare from 'use/popup/impl/usePopupCollectionShare'
import { PopupCollection } from 'components/popup/collection/PopupCollection'
import { LinearProgressStyled } from 'components/status/styled'
import {  ShareButtonsWrapperStyled, ShareDescriptionStyled } from 'components/popup/share/styled'
import { ShareButton } from 'components/popup/share/ShareButton'
import { IShareContainerProps, ShareContainer } from 'components/popup/share/ShareContainer'
import useAuth from 'use/auth/useAuth'
import { IShareContainerComposeProps } from 'compose/popup/share/ShareContainerComposeLazy'

export const PopupCollectionShareCompose: FC = () => {
  const userId = useAuth().user
  const { labels, result, fullModal } = usePopupCollectionShare()
  const {
    collection,
    shareContainerProps,
    loadingCollectionData,
    handleImage,
    url,
    text
  } = result
  const { code, name } = collection || {}

  const showProgress = loadingCollectionData || !url
  const params = {
    url,
    text
  }

  return (
    <>{fullModal.open &&
      <PopupCollection {...fullModal}
        showProgress={showProgress}
        contentHero={(
          <>
            <ShareDescriptionStyled>{ labels.description }</ShareDescriptionStyled>
            { url && (
                <ShareButtonsWrapperStyled>
                  <ShareButton variant='facebook' {...params} />
                  <ShareButton variant='whatsapp' {...params} />
                  <ShareButton variant='twitter' {...params} />
                </ShareButtonsWrapperStyled>
              )
            }
          </>
        )}
        collection={collection}
        noPadding
        buttons={[]}
        title={labels.title}>
          <Share 
            composeProps={{
              code,
              userId,
              handleImage,
              loadingLabel: labels.loading,
              loadingCollectionData
            }}
            containerProps={shareContainerProps}
          />
      </PopupCollection>
    }
  </>)
}
interface IShareProps {
  composeProps: IShareContainerComposeProps;
  containerProps: IShareContainerProps;
}

const Share = React.memo((props: IShareProps) => {
  const ShareContainerComposeLazy = React.lazy(() =>
    import('compose/popup/share/ShareContainerComposeLazy')
      .then(({ ShareContainerComposeLazy }) => ({ default: ShareContainerComposeLazy }))
    )

  return (
    <Suspense fallback={<LinearProgressStyled />}>
      <ShareContainerComposeLazy {...props.composeProps}>
        <ShareContainer {...props.containerProps} />
      </ShareContainerComposeLazy>
    </Suspense>
  )
}, (prev, actual) => {
  return prev.composeProps.loadingCollectionData === actual.composeProps.loadingCollectionData
})
