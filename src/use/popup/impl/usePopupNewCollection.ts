import { navigate } from 'gatsby'
import { ISearchCollectionModel } from 'model/SearchCollection.model'
import { modals, nav } from 'setup/constants'
import useI18n from 'use/i18n/useI18n'
import useFullModal, { IProcess } from '../useFullModal'
import { JOIN_COLLECTION_MUTATION } from 'graphql/mutation'
import useAuth from 'use/auth/useAuth'
import { useModalCollectionNew } from 'use/modal/useModal'
import { useControlledMutation } from 'use/fetch/useQuery'
import { useState } from 'react'
import { IPopupNewCollectionResult } from 'compose/popup/PopupNewCollectionCompose'

const modalId = modals.collection
const labelIds = {
  collectors: 'popup.collection.collectors',
  index: 'popup.collection.index',
  join: 'popup.collection.join',
  fav: 'popup.collection.addFav',
  recent: 'popup.collection.recent',
  serverError: 'server.database',
  year: 'popup.collection.year',
}




const usePopupNewCollection = () => useFullModal<ISearchCollectionModel, IPopupNewCollectionResult>({
  labelIds,
  modalId,
  process
})

export default usePopupNewCollection



function process (data: ISearchCollectionModel): IPopupNewCollectionResult & IProcess {
  const { t, tm } = useI18n()
  const labels = tm(labelIds)
  const userId = useAuth().user
  const { refetch, close } = useModalCollectionNew()
  const edit = () => navigate(nav.userCollectionEditRoute(code))
  const { collection, collectors } = data || {}
  
  const { code, name, year } = collection || {}
  const { total, recent } = collectors || {}

  const [ favToggle, setFavToggle ] = useState(false)
  const toggleFav = () => setFavToggle(!favToggle)
  const params = { code, userId, value: favToggle ? 1 : 0 }

  // DATA MANAGEMENT
  const [ joinCollection, joinResult ] = useControlledMutation({
    name: 'joinCollection',
    mutation: JOIN_COLLECTION_MUTATION,
    refetch,
    success: () => {
      close()
      edit()
    }
  })

  const join = () => {
    joinCollection({ variables: { params }})
  }

  const values = {
    year: year || null,
    collectors: total || 0,
    recent: recent || 0,
    fav: favToggle,
  }

  const actions = {
    fav: toggleFav,
    join,
    index: () => navigate(nav.publicCollection({ name })),
  }

  return {
    values,
    actions,
    collection,
    showProgress: joinResult.loading,
    labels
  }
}