import { useState, useEffect } from "react"
import { navigate } from 'gatsby'
import { POPUP_COLLECTION_QUERY } from "graphql/query"
import { modals, nav, routes } from "setup/constants"

import UserCollectionModel from "model/UserCollection.model"
import { IUserCollectionExModel } from "model/UserCollectionEx.model"
import useAuth from "use/auth/useAuth"
import { useControlledMutation, useQueryAll } from "use/fetch/useQuery"
import useI18n from "use/i18n/useI18n"
import { useModalCollection, useModalCollectionEdit, useModalCollectionShare, useModalConfirm } from "use/modal/useModal"
import useFullModal, { IProcess } from "../useFullModal"

import { DELETE_COLLECTION_MUTATION, FAV_COLLECTION_MUTATION, SLEEP_COLLECTION_MUTATION } from "graphql/mutation"
import { IUserExchangeModel } from "model/UserExchange.model"
import { ChangeFilter, ChangeState, StatesCancelled, StatesCompleted, StatesInProgress } from "model/UserChange.model"
import useChangeFilter from "use/filter/impl/useChangeFilter"
import { IPopupCollectionData } from "use/popup/impl/interface"
import { IPopupCollectionResult } from "compose/popup/interface"

const modalId = modals.collection
const labelIds = {
  changesInProgress: 'popup.collection.changes.inProgress',
  changesCompleted: 'popup.collection.changes.completed',
  changesCancelled: 'popup.collection.changes.cancelled',
  confirmDelete: 'confirm.deleteCollection',
  countExchanges: 'popup.collection.countExchanges',
  countFaults: 'popup.collection.countFaults',
  countRepeated: 'popup.collection.countRepeated',
  delete: 'popup.collection.delete',
  edit: 'popup.collection.edit',
  fav: 'popup.collection.fav',
  historical: 'popup.collection.historical',
  index: 'popup.collection.index',
  join: 'popup.collection.join',
  share: 'popup.collection.share.title',
  sleep: 'popup.collection.sleep',
  sleepMessage: 'popup.collection.sleepMessage',
  year: 'popup.collection.year',
  serverError: 'server.database'
}

const { STATE, COLLECTOR, COLLECTION } = ChangeFilter
const { PROGRESS, COMPLETE, CANCELLED } = ChangeState



const usePopupCollection = () => useFullModal<IPopupCollectionData, IPopupCollectionResult>({
  labelIds,
  modalId,
  process
})

export default usePopupCollection


function process (data: IPopupCollectionData): IPopupCollectionResult & IProcess {
  const { t, tm } = useI18n()
  const labels = tm(labelIds)
  const userId = useAuth().user
  const { refetch, close: closeCollection } = useModalCollection()
  const { open: openConfirm, close: closeConfirm } = useModalConfirm()
  const { open: openCollectionEdit } = useModalCollectionEdit()
  const { open: openCollectionShare } = useModalCollectionShare()
  const { updateFilter } = useChangeFilter()
  const [ refreshing, setRefreshing ] = useState(false)
  const [ favToggle, setFavToggle ] = useState(false)
  const [ sleepToggle, setSleepToggle ] = useState(false)
  const { code, name } = data || {}
  const variables = {
    codes: [code],
    userId,
    changes: true,
    userElements: true
  }

  const refresh = () => code && get(variables)
  const toggleFav = () => setFavToggle(!favToggle)
  const toggleSleep = () => setSleepToggle(!sleepToggle)
  // DATA MANAGEMENT
  const { get, loading, list } = useQueryAll<IUserCollectionExModel>('userCollectionEx', POPUP_COLLECTION_QUERY)
  const fail = () => {
    setRefreshing(true)
    setTimeout(() => {
      setRefreshing(false)
    })
  }
  const [ favCollection, favResult ] = useControlledMutation({
    name: 'favCollection',
    mutation: FAV_COLLECTION_MUTATION,
    refetch,
    success: toggleFav,
    fail
  })

  const [ sleepCollection, sleepResult ] = useControlledMutation({
    name: 'sleepCollection',
    mutation: SLEEP_COLLECTION_MUTATION,
    refetch,
    success: toggleSleep,
    fail
  })

  const [ deleteCollection, deleteResult ] = useControlledMutation({
    name: 'deleteCollection',
    mutation: DELETE_COLLECTION_MUTATION,
    refetch,
    okMessage: true,
    success: closeCollection
  })

  const onClickDelete = () => {
    openConfirm({
      accept: () => {
        deleteCollection({ variables: { params: { code, userId, value: 1 } }})
        closeConfirm()
      },
      cancel: closeConfirm,
      // text: t(labelIds.confirmDelete),
      text: t('server.database'),
      severity: 'info'
    })
  }

  const { userCollection, countRepeated, countFaults, exchanges } = list && list[0] || {}
  const { collection, priority = 1 } = userCollection || {}
  
  const numExchanges = getNumExchanges(exchanges)
  const values = userCollection
    ? {
      year: collection.year || null,
      fav: favToggle,
      sleep: sleepToggle,
      countRepeated,
      countFaults,
    } : {}

  const changesLabels = {
    changesInProgress: numExchanges.inProgress && t(labels.changesInProgress, { num: numExchanges.inProgress }),
    changesCompleted: numExchanges.completed && t(labels.changesCompleted, { num: numExchanges.completed }),
    changesCancelled: numExchanges.cancelled && t(labels.changesCancelled, { num: numExchanges.cancelled })
  }

  useEffect(() => {
    const { fav, sleep } = UserCollectionModel.parsePriority(priority)
    setFavToggle(fav)
    setSleepToggle(sleep)
  }, [ priority ])

  const goToChanges = (state: ChangeState) => {
    updateFilter(STATE, { input: state })
    updateFilter(COLLECTOR, { input: '' })
    updateFilter(COLLECTION, { input: code })
    navigate(routes.userChanges)
  }
  const actions = {
    edit: () => navigate(nav.userCollectionEditRoute(code)),
    // edit: () => {
    //   closeCollection()
    //   openCollectionEdit({ collection })
    // },
    changesInProgress: () => goToChanges(PROGRESS),
    changesCompleted: () => goToChanges(COMPLETE),
    changesCancelled: () => goToChanges(CANCELLED),
    index: () => navigate(nav.publicCollection({ name })),
    // share: () => navigate(nav.userCollectionShareRoute(code)),
    share: () => {
      closeCollection()
      openCollectionShare({ collection })
    },  
    fav: () => {
      setSleepToggle(false)
      favCollection({ variables: { params: { code, userId, value: favToggle ? 0 : 1 } }})
    },
    sleep: () => {
      setFavToggle(false)
      sleepCollection({ variables: { params: { code, userId, value: sleepToggle ? 0 : 1 } }})
    },
    delete: onClickDelete
  }
  
  return {
    refresh,
    labels: {
      ...labels,
      ...changesLabels
    },
    values,
    actions,
    collection,
    showProgress: loading || favResult.loading || sleepResult.loading || deleteResult.loading,
    refreshing
  }
}


function getNumExchanges(exchanges: IUserExchangeModel[] = []) {
  const states = {}
  const toStateInit = (num) => states[num] = 0
  const toStateAdd = (num) => states[num]++
  const reduceStates = (tot, state) => { tot+=states[state]; return tot }
  StatesInProgress.forEach(toStateInit)
  StatesCompleted.forEach(toStateInit)
  StatesCancelled.forEach(toStateInit)
  exchanges.forEach((exchange) => toStateAdd(exchange.change.stateId))

  return {
    inProgress: StatesInProgress.reduce(reduceStates, 0),
    completed: StatesCompleted.reduce(reduceStates, 0),
    cancelled: StatesCancelled.reduce(reduceStates, 0),
  }
}