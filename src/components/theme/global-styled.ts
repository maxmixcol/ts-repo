import { blackColor, mainBackground, whiteColor } from "./constants";
import images from "./images";

/**
 * React, Gatsby, Jest, TypeScript, Apollo - Starter
 * https://github.com/eduard-kirilov/gatsby-ts-apollo-starter
 * Copyright (c) 2020 Eduard Kirilov | MIT License
 */
export const HelmetGlobalStyled: any = `
  body {
    margin: 0;
    overflow-x: hidden;
    background: ${mainBackground.background};
  }
  .splash {
    height: 100vh;
    width: 100%;
    top: 0;
    position: fixed;
    z-index: 9999;
    background: url(${images.mainBackground});
    background-color: ${blackColor};
    color: ${whiteColor};
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
  }
  .splash::after {
    position: absolute;
    z-index: -1;
    width: 100%;
    height: 100%;
    content: '';
    display: block;
    background: ${blackColor};
    opacity: .8;
  }
  svg {
    fill: currentColor;
  }
  .linear-status {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    z-index: 1204;
  }
  html.noscroll, .noscroll body {
    width: 100%;
    height: 100%;
    margin: 0;
    overflow: hidden;
    position: absolute;
    top: 0;
    left: 0;
  }
`;
