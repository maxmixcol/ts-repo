import icons from 'components/theme/icons';
import { IPopupChangeResult } from 'compose/popup/interface';
import { ChangeStateIds, ChangeStateIndex } from 'model/UserChange.model';

const { INIT, ACCEPTED, RECEIVED1, NOT_RECEIVED1, CANCELLED, COMPLETE } = ChangeStateIds
const stepperData = (state: ChangeStateIndex, t: any): Pick<IPopupChangeResult, 'stepNum' | 'stepList'> => {
  const stepNum = {
    NEW: 0,
    INIT: 0,
    ACCEPTED: 1,
    RECEIVED: 1,
    NOT_RECEIVED: 2,
    FINISHED: 2,
    CANCELLED: 1,
  }[state]
  const stepList = {
    NEW: [],
    INIT: [INIT, ACCEPTED, RECEIVED1],
    ACCEPTED: [INIT, ACCEPTED, RECEIVED1],
    RECEIVED: [ACCEPTED, RECEIVED1, COMPLETE],
    NOT_RECEIVED: [ACCEPTED, NOT_RECEIVED1, COMPLETE],
    FINISHED: [ACCEPTED, RECEIVED1, COMPLETE],
    CANCELLED: [INIT, CANCELLED],
  }[state]

  return {
    stepNum,
    stepList: stepList.map((stateId: number) => ({
      label: t(`state.actual.${stateId}`),
      icon: icons.arrowRight
    }))
  }
}

export default stepperData
