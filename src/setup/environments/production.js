import { isBrowser } from 'utils/helpers';
import environment from './base';

const base =  `${isBrowser() ? window.location.protocol : 'https:'}//acabaramos.com/`;
  // const base = 'https://acabaramoscom.globat.com/dev/';
const baseApi = `${base}api.php`;
const baseAuth = `${base}auth.php`;

const env = environment(base, baseApi, baseAuth);

const productionEnv = {
  ...env,
  // override anything that gets added from base.
};

export default productionEnv;
