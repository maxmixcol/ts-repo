import React, { memo, useEffect } from 'react'
import { routes } from 'setup/constants'
import { DASHBOARD_CHANGES_SEARCH_QUERY } from 'graphql/query'
import { LinearProgress } from '@material-ui/core'
import { useQueryAll } from 'use/fetch/useQuery'
import useNewChangeFilter from 'use/filter/impl/useNewChangeFilter'
import useI18n from 'use/i18n/useI18n'
import useAuth from 'use/auth/useAuth'
import { useModalChange, useModalUser } from 'use/modal/useModal'

import { IUserExchangeModel } from 'model/UserExchange.model'

import { FilterCompose } from 'compose/filter/FilterCompose'
import { CardChangeDashboardNewChangeCompose } from 'compose/card/change/CardChangeDashboardNewChangeCompose'
import { SingleListCompose } from 'compose/filter/SingleListCompose'
import { PopupChangeCompose } from 'compose/popup/PopupChangeCompose'
import { PopupUserCompose } from 'compose/popup/PopupUserCompose'
import { EmptyListCompose } from 'compose/empty/EmptyListCompose'

import { HeaderTabs } from 'components/header/types/HeaderTabs'
import { BackIcon } from 'components/common/common.styled'
import { DashboardView } from 'components/typography/view/dashboard/DashboardView'

const labelIds = {
	userChangesTitle: 'pages.dashboard/cambios.title',
	userChangeSearchTitle: 'pages.dashboard/cambios/buscar.title',
	numChanges: 'filter.numChanges',
}


const DashboardCambiosBuscarPage = ({ location }) => {
	const userId = useAuth().user
  const { t, tm } = useI18n()
  const labels = tm(labelIds)
  const { isOpen: isUserOpen } = useModalUser()
  const { isOpen: isChangeOpen } = useModalChange()

	const { init, values, process, filterProps } = useNewChangeFilter()
  init()

  const { offer } = values()
	const { get, list, loading } = useQueryAll<IUserExchangeModel>('allNewExchanges', DASHBOARD_CHANGES_SEARCH_QUERY)


	useEffect(() => {
    if (userId) {
      if (offer) {
        get({
          offer,
          userId
        })
      }
    }
	}, [userId, offer])

	const { open: openChange } = useModalChange()
	const onClickChange = (exchange: IUserExchangeModel) => {
		openChange({ exchange, isNew: true })
	}

  const emptyUserCollections = !loading && list?.length === 0
  const tabChanges = { label: labels.userChangesTitle, to: routes.userChanges }
  const tabSearchChanges = { label: labels.userChangeSearchTitle, to: routes.userChangeSearch, selected: true }
  const headerTabs = [tabChanges, tabSearchChanges]
  const showChanges = list?.length > 0
	const processedProps = process(list || [])

	return (
		<DashboardView route={routes.userChangeSearch}
      location={location}
			header={
				<>
					<BackIcon linkTo={routes.userDashboard} />
					<HeaderTabs items={headerTabs} />
				</>}>
			{!!loading && <LinearProgress />}
			{emptyUserCollections && <EmptyListCompose id='emptySearchChanges' />}
			{showChanges && processedProps.list &&
				<FilterCompose
					{...filterProps}
					{...processedProps}
          loading={loading}
				>{list => (!!list.length &&
					<SingleListCompose
						pageSize={10}
						list={list}
            labelNum={labelIds.numChanges}
						variant={'vertical'}
						onClick={item => onClickChange(item)}
						item={CardChangeDashboardNewChangeCompose}>
					</SingleListCompose>
				)}
				</FilterCompose>}
      { isChangeOpen && <PopupChangeCompose />}      
			{ isUserOpen && <PopupUserCompose />}
		</DashboardView>
	)
}

export default memo(DashboardCambiosBuscarPage)

