import environment from './base';

const base = 'http://localhost:7071/';
const baseApi = `${base}`;
const baseAuth = `${base}auth`;

const env = environment(base, baseApi, baseAuth);

const developmentEnv = {
  ...env,
  // override anything that gets added from base.
  api: {
    ...env.api,
  },
  path: {
    img: '/img/'
  },
  isProduction: false,
  isDevelopment: true,
};

export default developmentEnv;
