export interface IUserInfoModel {
  iduser: number; // iduser
  name: string; // Nombre
  surname: string; // Apellidos
  address: string; // Direccion
  city: string; // Ciudad
  place: string; // ProvinciaRegion
  postalCode: string; // Codpostal
  country: string; // Pais
  settings: string; // Caracteristicas
}

export interface IAddress {
  id: string;
  name: string;
  address: string;
  country: string;
}

export enum AddressState {
  NEW,
  LOADING,
  ERROR,
  LOADED,
}

export default class UserInfoModel {

  /*
  {
    "iduser":22,
    "Nombre":"Antonio",
    "Apellidos":"Corrales Garc\u00eda",
    "Direccion":"C\/ Torrelodones 15 Chalet 10",
    "Ciudad":"Torrelodones",
    "ProvinciaRegion":"Madrid",
    "Codpostal":"28250",
    "Pais":"ES",
    "Puntos":0,
    "Caracteristicas":"",
    "Mailing":"",
  }
*/
  static getFlag = (country: string) => `${process.env.GATSBY_MEDIA_ASSET}flags/${country}.png`
  static getAddress = ({
    iduser,
    name,
    surname,
    city,
    place,
    country,
    postalCode,
    address
  }: IUserInfoModel): Partial<IAddress> => {

    const scity = city?.trim() || ''
    const sprovince = place?.trim() || ''
    const scountry = country && country !== 'undefined' ? country : ''
    const cp = postalCode ? '(' + postalCode?.trim() + ')' : ''
    const saddress = [
      address || '',
      scity && scity.toLowerCase() !== sprovince.toLowerCase() ? capitalizeFirst(scity) : '',
      cp,
      sprovince ? capitalizeFirst(sprovince) : ''
    ].filter(Boolean).join(' - ')
    return {
      id: `${iduser}`,
      name: [name, surname].filter(Boolean).join(' ').trim(),
      country: scountry,
      address: saddress || ''
    }
  }
}

function capitalizeFirst (string: string) {
  return string.charAt(0).toUpperCase() + string.slice(1)
}