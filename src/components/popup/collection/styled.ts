import { TextStyled } from 'components/common/common.styled'
import { Description } from 'compose/Description'
import styled, { css } from 'styled-components'
import { CardRaisedStyled } from '../../../compose/popup/_shared/styled'

export const BigNameCollectionStyled = styled.span`
  font-size: 0.8rem;
  z-index: 1;
`
export const CardRaisedCollectionStyled = styled(CardRaisedStyled).attrs(
  { color: 'gray' }
)`
`
export const DescriptionStyled = styled(Description)`
  ${({ theme }) => css`
    ${theme.breakpoints.up('md')} {
      width: 25rem;
    }
    ${theme.breakpoints.up('sm')} {
      width: 20rem;
    }
  `}
  padding: 1rem;
  text-align: justify;
`