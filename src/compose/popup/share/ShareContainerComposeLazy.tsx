import useAsyncEffect from 'use-async-effect'
import html2canvas from 'html2canvas'
import React, { FC, useEffect, useRef, useState } from 'react';
import {
  HiddenCanvasStyled,
  ImgContainerStyled,
  ShareContainerWrapperStyled,
  ShareTitleStyled
} from 'components/popup/share/styled';
import useFetch from 'use/fetch/useFetch';
import { routes } from 'setup/constants';

const IMG_EXT = 'png'
export interface IShareContainerComposeProps {
  loadingCollectionData: boolean;
  loadingLabel: string;
  handleImage: (img: string) => void;
  userId: number,
  code: number
}

// const postImgData = async (post, imgData, params, onSuccess) => {
//   const { result, filename } = await post(routes.imgSave, {
//     imgData,
//     ...params 
//   })
//   if (result) {
//     onSuccess(filename)
//   }
// }

const postData = async (post, blob, params, onSuccess) => {
  const formData = new FormData(); //this will submit as a "multipart/form-data" request
  
  Object.keys(params).forEach(key => {
    formData.append(key, params[key])
  })
  formData.append('image_name', blob); //"image_name" is what the server will call the blob

  const { result, filename } = await post(routes.imgSave, formData)
  if (result) {
    onSuccess(filename)
  }
}

const saveImage = async (ref, onSuccess, post, params) => {
  const canvas = await html2canvas(ref.current, {
    scale: 1.5
  })

  // const url = canvas.toDataURL('image/jpeg', 0.5)
  canvas.toBlob(function(blob){
    postData(post, blob, params, (filename) => {
      onSuccess(canvas, filename)
    })
  }, `image/${IMG_EXT}`);
}

export const ShareContainerComposeLazy: FC<IShareContainerComposeProps> = ({
  loadingCollectionData,
  loadingLabel,
  handleImage,
  userId,
  code,
  children
}) => {
  const ref = useRef(null)
  const canvasRef = useRef(null)
  const { post } = useFetch()
  const [ hidden, setHidden ] = useState(false)
  const [ ready, setReady ] = useState(false)

  const params = {
    userId,
    CodigoC: code,
    ext: IMG_EXT
  }

  const onSuccess = (canvas, filename) => {
    setHidden(true)
    canvasRef.current.innerHTML = ''
    canvasRef.current.append(canvas)
    handleImage(filename)
  }

  useAsyncEffect(async () => {
    if (ready) {
      saveImage(ref, onSuccess, post, params )
    }
  }, [ready])

  useEffect(() => {
    if (ref.current && canvasRef.current && !loadingCollectionData && !hidden) {
      setReady(true)
    }
  }, [canvasRef.current, loadingCollectionData])

  return (
    <ShareContainerWrapperStyled>
      <HiddenCanvasStyled ref={ref} hidden={hidden}>
        { children }
      </HiddenCanvasStyled>
      <ImgContainerStyled ref={canvasRef} >
        <ShareTitleStyled>{ loadingLabel }</ShareTitleStyled>
      </ImgContainerStyled>
    </ShareContainerWrapperStyled>
  );
};


