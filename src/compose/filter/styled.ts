import styled, { css } from 'styled-components';
import icons from 'components/theme/icons';

import { FilterButton, FilterIconButton } from 'components/filter/common';
import { TextStyled as Text } from 'components/common/common.styled'
import { FormRadioGroup } from 'components/form/FormRadioGroup';
import { InputField } from 'components/form/InputField';

export const InputFieldStyled = styled(InputField)`
`

export const TextStyled = styled(Text)`
  margin-bottom: 1rem;
  display: block;
`
export const ButtonContainer = styled.div`
  width: 100%;
  display: inline-flex;
  justify-content: flex-end;
`
export const FilterRadioGroupStyled = styled(FormRadioGroup)`
  display: flex;
  flex-direction: row;
  ${({ theme }) => css`
    ${theme.breakpoints.up('xs')} {
      flex-wrap: nowrap;
    }
  `}

  & > * {
    white-space: nowrap;
  }
`
export const FilterButtonStyled = styled(FilterButton)`
  height: 2.5rem;
  margin: 0;
`

export const ApplyButtonStyled = styled(FilterButtonStyled).attrs(props => ({
  icon: icons.filterApply
}))`
`
export const OpenButtonStyled = styled(FilterButtonStyled).attrs(props => ({
  icon: icons.filterApply
}))`
  ${({ theme }) => css`
    ${theme.breakpoints.down('xs')} {
      position: absolute;
      right: 1rem;
      margin: 5rem 0 0;
      height: 2rem;
    }
  `}
`

export const ClearButtonStyled = styled(FilterIconButton)
  .attrs((props) => ({
  icon: icons.filterClear,
}))`
  ${({ disabled }) => disabled ? css`
    opacity: 0.2;
  ` : ''}
`
export const ChipContainerStyled = styled.div`
  display: inline;
  flex: 1;
`
