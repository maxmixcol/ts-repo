import { grayColor, whiteColor } from 'components/theme/constants';
import styled, { css } from 'styled-components';
import { ISize } from 'utils/interface';
const getNameSize = size => ({
  [ISize.S]: 0.75,
  [ISize.M]: 0.65,
  [ISize.L]: 0.6
}[size])
export const ElementBoxWrapper = styled.div`
  position: relative;
  background: ${grayColor[6]};
  font-size: 0.75rem;
  line-height: 0.75rem;
  width: 100%;
  height: 4rem;
  border: 1px solid ${grayColor[17]};
  border-radius: 4px;
  padding: 0.25rem;
  overflow: hidden;
  display: flex;
  flex-direction: column;
  & i {
    white-space: nowrap;
  }
  & strong {
    display: block;
    font-size: ${({ size }) => getNameSize(size)}rem;
  }
  & span {
    background: ${grayColor[15]};
    color: ${whiteColor};
    position: absolute;
    bottom: 0;
    width: 100%;
    margin-left: -4px;
    font-size: 0.6rem;
    padding: 1px 1px 0;
    white-space: nowrap;
  }
  & em {
    font-size: 0.45rem;
    background: white;
    float: right;
    position: absolute;
    bottom: 0;
    right: 0;
    padding: 1px;
  }
`
export const MiniCard = styled.span`
  height: 1rem;
  border: 1px solid ${grayColor[17]};
  border-radius: 3px;
  font-size: 0.7rem;
  padding: 0 2px;
  margin: 0;
  line-height: 1rem;
  background: ${grayColor[14]};
  min-width: 1rem;
  border-right-color: ${grayColor[15]};
  border-bottom-color: ${grayColor[15]};
  display: inline-block;
  white-space: nowrap;
`