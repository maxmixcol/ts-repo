import styled from 'styled-components';
import { Exchange } from 'components/card/change/Exchange';

export const ExchangeTotal = styled(Exchange)`
  transform: scale(1.2);
  position: relative;
`