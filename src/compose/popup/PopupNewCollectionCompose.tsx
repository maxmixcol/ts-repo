import React, { FC } from 'react'

import icons from 'components/theme/icons'
import { CardItem } from './_shared/CardItem'

import {
  ListStyled,
} from './_shared/styled'

import { ICollectionModel } from 'model/Collection.model'
import { PopupCollection } from 'components/popup/collection/PopupCollection'
import usePopupNewCollection from 'use/popup/impl/usePopupNewCollection'
import { IButtonTypes } from 'components/button/ButtonList'
import { DescriptionStyled } from 'components/popup/collection/styled'

interface IActionsNewCollection {
  fav: () => void;
  join: () => void;
  index: () => void;
}

export interface IPopupNewCollectionResult {
  collection: ICollectionModel;
  showProgress: boolean;
  actions: IActionsNewCollection;
  values: any;
}

export const PopupNewCollectionCompose: FC = () => {
  const { labels, result, fullModal } = usePopupNewCollection()
  const {
    collection,
    actions,
    showProgress,
    values
  } = result

  const buttons = [
    { children: labels.join, icon: icons.add, onClick: actions.join, type: IButtonTypes.primary },
    { children: labels.fav, icon: icons.fav, onClick: actions.fav, toggle: values.fav },
    { children: labels.index, icon: icons.index, onClick: actions.index },
  ]

  return (
    <>{fullModal.open &&
      <PopupCollection
        { ...fullModal}
        collection={collection}
        description
        buttons={buttons}
        showProgress={showProgress}>
        <ListStyled>
          <CardItem name={labels.year} value={values.year} />
          <CardItem name={labels.collectors} value={values.collectors} />
          <CardItem name={labels.recent} value={values.recent} />
        </ListStyled>
      </PopupCollection>
  }
  </>
  )
}
