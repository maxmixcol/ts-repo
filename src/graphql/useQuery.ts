import { DocumentNode, QueryResult, TypedDocumentNode, useQuery as u } from '@apollo/client';

export default function useQuery<TData = any, TVariables = any>(
  query: DocumentNode | TypedDocumentNode<TData, TVariables>,
  options?: any
): QueryResult<TData, TVariables> {
  return u(query, { variables: options });
}
