import React, { FC, memo, useState } from 'react';

import {
  ButtonStyled,
  ButtonsWrapper,
  InfoIcon,
  MessageCardWrapper,
  MessageInfoWrapper
} from 'components/message/styled';
import { IChildren } from 'utils/interface';

export interface IMessageBoxProps extends IChildren {
  closeButton?: string;
  onClose?: () => void;
  variant?: 'info' | 'card';
  className?: string;
}

const MessageBoxMemo: FC<IMessageBoxProps> = ({
  variant = 'info',
  closeButton,
  onClose,
  className,
  children
}) => { 

  const [ closed, setClosed ] = useState(false)
  const [ hidden, setHidden ] = useState(false)
  const close = () => {
    setHidden(true)
  }
  const animationEnd = () => {
    setClosed(true)
    onClose && onClose()
  }

  const Wrapper = {
    card: MessageCardWrapper,
    info: MessageInfoWrapper
  }[variant]

  const Icon = {
    info: InfoIcon
  }[variant]

  const hasButtons = !!closeButton
  
  return (
    <Wrapper
      className={className}
      closed={closed}
      hidden={hidden}
      onAnimationEnd={animationEnd}>
      { Icon && <Icon />}
      { children }
      { hasButtons && 
        <ButtonsWrapper>
          { closeButton && <ButtonStyled onClick={close}>{ closeButton }</ButtonStyled>}
        </ButtonsWrapper>
      }
    </Wrapper>
  );
};

export const MessageBox = memo(MessageBoxMemo);
