import styled, { css } from 'styled-components';
import { PrimaryEmptyButton, TextStyled } from 'components/common/common.styled';
import icons from 'components/theme/icons';

export const DescriptionWrapperStyled = styled.div`
  display: flex;
  width: 100%;
  flex-wrap: wrap;
  justify-content: space-around;
  position: relative;
`
export const DescriptionTextStyled = styled(TextStyled).attrs(props => ({
  smallText: true,
}))`
  text-align: justify;
  max-height: 4.66rem;
  overflow: hidden;
  &.opened {
    max-height: none;
    height: auto;
  }
`
export const MoreButton = styled(PrimaryEmptyButton).attrs(props => ({
  iconRight: icons.arrowRight
}))`
  &.MuiButton-outlined  {
    right: 1rem;
    background: white;
    position: absolute;
    bottom: 0rem;
    padding: 0;
  }

`

