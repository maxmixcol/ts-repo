
import React, { FC, memo } from 'react'
import { AdditionalGridStyled, AdditionalParagraphStyled } from 'components/card/collection/styled'
import { CardCollectionV } from 'components/card/collection/CardCollectionV'
import CollectionModel from 'model/Collection.model'
import UserCollectionModel from 'model/UserCollection.model'
import useI18n from 'use/i18n/useI18n'
import { IconDefinition } from 'components/theme/icons';
import { Icon } from 'components/styled'
import { IUserCollectionExModel } from 'model/UserCollectionEx.model'
import { ChangeStateIds } from 'model/UserChange.model'
import { HeroColor } from 'components/hero/Hero'


interface IAdditionalItem {
  icon?: IconDefinition;
  color: HeroColor;
  text: string;
}

const AdditionalItem: FC<IAdditionalItem> = ({ color, text, icon }) => (
  <AdditionalParagraphStyled color={color}>{icon && <Icon icon={icon} /> } {text}</AdditionalParagraphStyled>
)

const labelIds = {
  numFaults: 'pages.dashboard/colecciones.numFaults',
  numRepeated: 'pages.dashboard/colecciones.numRepeated',
  numExchanges: 'pages.dashboard/colecciones.numExchanges',
  numElements: 'pages.dashboard/colecciones.numElements',
  favorite: 'pages.dashboard/colecciones.favorite',
  sleep: 'pages.dashboard/colecciones.sleep',
  complete: 'pages.dashboard/colecciones.complete'
}

const CardCollectionDashboardCollectionsComposeMemo: FC<IUserCollectionExModel> = ({ 
  userCollection,
  exchanges,
  countFaults,
  countRepeated
 }) => {
  const { t } = useI18n()
  const { collection, priority, countElements } = userCollection
  const { code, name } = collection
  const { fav, sleep } = UserCollectionModel.parsePriority(priority)
  const image = CollectionModel.getImage(code)
  
  const countCompletedExchanges = exchanges.filter(({ change }) => change.stateId === ChangeStateIds.COMPLETE).length

  const numExchanges = t(labelIds.numExchanges, { num: countCompletedExchanges })
  const percentFaults = Math.round(countFaults/ countElements * 100)
  const percentRepeated = Math.round(countRepeated/ countElements * 100)

  const numFaults = t(labelIds.numFaults, { num: countFaults, percent: percentFaults })
  const numRepeated = t(labelIds.numRepeated, { num: countRepeated, percent: percentRepeated })

  return (
    <CardCollectionV
      image={image}
      name={name}
      fav={fav}
      sleep={sleep}
      select={() => {}}>
      <AdditionalGridStyled>
        <div>
          <AdditionalItem color={HeroColor.danger} text={numFaults} />
          <AdditionalItem color={HeroColor.success} text={numRepeated} />
        </div>
        { !!countCompletedExchanges && <AdditionalItem color={HeroColor.rose} text={numExchanges} />}
      </AdditionalGridStyled>
    </CardCollectionV>
  )
}





export const CardCollectionDashboardCollectionsCompose = memo(CardCollectionDashboardCollectionsComposeMemo)