import styled, { css } from 'styled-components'
import { MessageBox } from 'components/message/MessageBox'

export const MessageBoxStyled = styled(MessageBox)`
  margin-bottom: 1rem;
`
