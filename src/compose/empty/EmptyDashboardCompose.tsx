import React, { FC, useEffect } from 'react'
import useI18n from 'use/i18n/useI18n'
import useAuth from 'use/auth/useAuth'
import useStorage from 'use/storage/useStorage'
import { flags } from 'setup/constants'

import { CustomStepper } from 'components/list/stepper/CustomStepper'
import { MessageBoxStyled } from './dashboard/styled'

import { empty } from './list/common'

interface IProps {
  emptyChanges: boolean;
  emptyCollections: boolean;
  emptyUserElements: boolean;
}

const addStep = (ob, t, button: boolean = false, extra = {}) => ({
  ...{ label: t(`${ob.pre}.step`) },
  ...(button ? { button: t(`${ob.pre}.button`) } : {}),
  ...ob,
  ...extra,
})
export const EmptyDashboardCompose: FC<IProps> = ({
  emptyChanges,
  emptyCollections,
  emptyUserElements
 }) => {
  const flag = flags.firstTimeUser
  const { getFlag, setFlag } = useStorage()
  const { t } = useI18n()
  const { user } = useAuth()

  const firstTime = !getFlag(flag)

  const name = user.userName && ` ${user.userName}`
  const email = user.email
  const steps = []
  
  if (firstTime) {
    steps.push(addStep(empty.register, t, false, {
      content: t(`${empty.register.pre}.content`, { name, email }),
    }))
  }
  steps.push(
    addStep(empty.collections, t, true),
    addStep(empty.userElements, t, true),
    addStep(empty.changes, t, true)
  )

  useEffect(() => {
    setFlag(flag, true)
  }, [])

  let step = 0
  if (emptyCollections) {
    step = 0
  } else if (emptyUserElements) {
    step = 1
  } else if (emptyChanges) {
    step = 2
  }
  if (firstTime) {
    step++
  }

  steps[step].expanded = true
  
  return (
    <MessageBoxStyled variant="card">
      <CustomStepper variant="vertical" steps={steps} step={step} />
    </MessageBoxStyled>
  )
}
