import React, { FC } from 'react';
import icons from 'components/theme/icons';
import { ButtonIconWrapperStyled } from 'components/button/styled';
import { Icon } from 'components/styled';
import { SecondaryButton, SecondaryEmptyButton } from 'components/common/common.styled';

export enum ViewMode {
  SMALL = 'SMALL',
  BIG = 'BIG'
}

const { SMALL, BIG } = ViewMode

interface IProps {
  mode: ViewMode;
  onChange: (mode: ViewMode) => void;
  className: string;
}
export const ViewModeSelector: FC<IProps> = ({
  mode,
  onChange,
  className
}) => {
  
  const icon = {
    [SMALL]: icons.viewSmall,
    [BIG]: icons.viewBig
  }[mode]

  const Button = {
    [SMALL]: SecondaryButton,
    [BIG]: SecondaryEmptyButton
  }[mode]

  return (
    <Button onClick={onChange} className={className}>
      <Icon icon={icon} />
    </Button>
  );
};
