import { navigate } from 'gatsby'
import UserChangeModel from "model/UserChange.model";
import { modals, routes } from "setup/constants";
import useAuth from "use/auth/useAuth";
import useI18n from "use/i18n/useI18n";
import useFullModal, { IProcess } from "../useFullModal";
import { useModalChange } from "use/modal/useModal";
import stepperData from "./change/stepperData";
import chatData from "./change/chatData";
import totalData from "./change/totalData";
import changeData from "./change/changeData";
import saveData from "./change/saveData";
import { IPopupChangeData } from 'use/popup/impl/interface';
import { IPopupChangeResult } from 'compose/popup/interface';

/*
const actions = {
  [NEW]: ['init'],
  [INIT]: ['reject', 'accept'], //by him
  [`${INIT}_${SELF}`]: ['cancel', 'update'],
  [ACCEPTED]: ['cancel', 'receive'],
  [RECEIVED]: ['receive'], //by him (not by me)
  [NOT_RECEIVED]: ['receive'], //by him (not by me)
  [`${RECEIVED}_${SELF}`]: [],
  [FINISHED]: [],
}
*/
const modalId = modals.change

const labelIds = {
  changeTitle: 'popup.change.changeTitle',
  chatTitle: 'popup.change.chatTitle',
  chatLabel: 'popup.change.chatLabel',
  helpList3: 'popup.change.helpList3'
}

const paramLabelIds =  {
  title: 'popup.change.title',
  intro: 'popup.change.intro.read',
  introEditing: 'popup.change.intro.editing',
}



const usePopupChange = () => useFullModal<IPopupChangeData, IPopupChangeResult>({
  labelIds,
  modalId,
  headerDark: true,
  process
})

export default usePopupChange


// process data
function process (data: IPopupChangeData): IPopupChangeResult & IProcess {
  const { t, tm } = useI18n()
  const labels = tm(labelIds)
  const userId = useAuth().user
  const { close: closeChange, refetch } = useModalChange()
  const closeAll = () => {
    closeChange()
    navigate(routes.userDashboard)
  }
  const { isNew, exchange } = data || {} 
  const { user, change, exchanges } = exchange || {}
  const { chat = '', stateId = 0 } = change || {}
  const { username: name, iduser: otherUserId } = user || {}

  const isSelf = change && UserChangeModel.isSelf(change, otherUserId)
  const changeState = UserChangeModel.getState(stateId)
  const showUserData = stateId !== 0

  labels.intro = t(paramLabelIds.intro, { name })
  labels.introEditing = t(paramLabelIds.introEditing, { name })
  labels.title = t(paramLabelIds.title, { name })

  let saveChat

  const changeProps = changeData(t, tm, {
    state: changeState, exchanges, isNew, isSelf, userId, userName: name, otherUserId })
  const chatProps = chatData(tm, {
    state: changeState, chat, saveChat: (msg) => saveChat(msg), changeId: parseInt(change?.id)
  })
  const changeId = change && parseInt(change.id, 10)
  const updateProps = {
    changeId,
    userId,
    otherUserId,
    otherUserName: name,
    comment: chatProps.chatMessage || undefined,
  }
  const offered = changeProps.offered.list
  const requested = changeProps.requested.list

  const saveProps = saveData({
    changeId,
    offered,
    requested,
    updateProps,
    success: closeAll,
    successChat: (userChange) => {
      chatProps.refreshChat(userChange)
    },
    refetch,
  })

  saveChat = saveProps.saveChat

  const totalProps = totalData(t, tm, {
    state: changeState,
    isSelf,
    isEdited: changeProps.edited,
    offered,
    requested,
    userName: name,
    save: saveProps.save
  }, changeProps.setSaving)

  return {
    ...stepperData(changeState, t),
    ...chatProps,
    ...totalProps,
    ...changeProps,
    ...{
      labels,
      name,
      user,
      showUserData,
      showProgress: saveProps.showProgress || changeProps.loading
    }
  }
}




