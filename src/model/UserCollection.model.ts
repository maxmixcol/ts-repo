import { ICollectionModel } from "./Collection.model";

export enum CollectionState {
  ACTIVE = 'ACTIVE',
  FAVORITE = 'FAVORITE',
  SLEEP = 'SLEEP',
  ALL = 'ALL'
}
export interface IUserCollectionModel {
  id: number; // Coleuser.id
  iduser: number; // Chat
  collection: ICollectionModel; //CodigoC
  lastUpdated: string; // FechaMod
  priority: number;
  settings: string; // Caracteristicas
  countElements: number;
}

export default class UserCollectionModel {
/*
  {
    "id":119,
    "iduser":22,
    "CodigoC":5,
    "Fecha":"0000-00-00 00:00:00",
    "FechaMod":"2019-02-20 11:37:09",
    "Caracteristicas":"3NNN:SNN",
    "Comentarios":"",
    "EFaltas":"",
    "ERepes":""
  }
*/
  static parsePriority = (priority: number) => ({
    2: { fav: true, sleep: false },
    1: { fav: false, sleep: false },
    0: { fav: false, sleep: true }
  }[priority])

}
