/**
* React, Gatsby, Jest, TypeScript, Apollo - Starter
* https://github.com/eduard-kirilov/gatsby-ts-apollo-starter
* Copyright (c) 2020 Eduard Kirilov | MIT License
*/
import React, { FC } from 'react';
import { shallow } from 'enzyme';

import { Modal } from './Modal';
import { withTheme } from 'setup/withTheme';

interface IProps {
  handleClose?: () => void;
  open?: boolean;
}

describe('Modal Container', () => {
  const ModalTest: FC<IProps> = ({ open }) => {
    return withTheme(
        <Modal
          open={open}
        >
          children
        </Modal>
    );
  }
  describe('Layout container is open', () => {
    const mount = shallow(<ModalTest open={true}/>);
    it('<Modal /> test should work', () => {
      expect(mount.find('Modal')).toHaveLength(0);
    });
    it('<div>children</div> test should work', () => {
      expect(mount.find('div[aria-hidden="true"]')).toHaveLength(0);
    });
  });

  describe('Layout container is not open', () => {
    const mount = shallow(<ModalTest open={false}/>);
    it('<Modal /> test should work', () => {
      expect(mount.find('Modal')).toHaveLength(0);
    });
  });
});
