import React, { FC, memo, useMemo, useEffect } from 'react'
import { IChildren } from 'utils/interface'
import { loadHistoryState } from 'utils/helpers'
import useAuth from 'use/auth/useAuth'
import { useModalAlert, useModalAuth } from 'use/modal/useModal'
import { EmptyView } from 'components/typography/view/common'
import { PopupAuthCompose } from 'compose/popup/PopupAuthCompose'
import { IndexHeroCompose } from 'compose/hero/IndexHeroCompose'

interface IProps extends IChildren {
  route?: string;
}
const PrivateComposeMemo: FC<IProps> = ({
  route,
  children,
}) => {
  const { user, logout } = useAuth()
  const { open: openAlert } = useModalAlert()
  const { open: openAuth } = useModalAuth()
  const state = loadHistoryState()

  // const state = { err: 'server.notlogged', code: 'UNAUTHORIZED'}
  const showEmpty = !!(!user || state?.err)
  const noChildren = useMemo(() => (
    <EmptyView>
      <IndexHeroCompose page={route} />
      <PopupAuthCompose />
    </EmptyView>
  ), [showEmpty])

  useEffect(() => {
    setTimeout(() => {
      if (state && state.err) {
        if (state.code === 'UNAUTHORIZED') {
          logout()
          openAuth()
        }
        openAlert({
          errorMessage: state.err,
          severity: 'error'
        })
      } else if (!user) {
        openAuth()
      }
    }, 1)
  }, [showEmpty])

  return <>{showEmpty ? noChildren : children}</>
}

export const PrivateCompose = memo(PrivateComposeMemo)