
import { useDispatch, useSelector } from 'react-redux'
import { createSelector } from 'reselect'
import FilterModel, { IFilterModel, IFilterModelMap } from 'use/filter/store/model/FilterModel'
import { actions, get } from './store'

const { getFiltered, getSorted } = get
const { setFiltered, setSorted, update } = actions

export interface IValues {
  [key: string]: any;
};

const useFilter = (id: string) => {
  const dispatch = useDispatch()
  const setFilters = (filter: IFilterModel[]) => {
    dispatch(setFiltered({ id, filter }))
  }
  const setSort = (sortId: string) => {
    dispatch(setSorted({ id, sort: sortId }))
  }
  const updateFilter = (name: string, filter: Partial<IFilterModel>) => {
    dispatch(update({ id, name, filter }))
  }

  const selectFilterByName = name => createSelector(
    getFiltered(id),
    (filters: IFilterModel[] = []) => filters.find(filter => filter.name === name)
  )

  const selectFilterOptionsByName = name => createSelector(
    selectFilterByName(name),
    (filter: IFilterModel) => (filter || {}).options
  )

  const values = ():IValues => useSelector(createSelector(
    getFiltered(id),
    (filters: IFilterModel[] = []) => FilterModel.getValues(filters)
  ))

  const onChange = (name: string, values: IValues) => {
    const input = values[name]
    updateFilter(name, { input, disabled: undefined })
    if (name === 'sort') {
      setSort(input)
    }
  }

  const getFilter = useSelector(getFiltered(id))

  const getInit = (filterMap: IFilterModelMap, t: any, defaultSort?: string) => {
    const actual = getFilter
    if (!actual || !actual.length) {
      setFilters(Object.values(filterMap).map(filter => ({
        ...filter, ...{ label: t(filter.label) }
      })))
      if (defaultSort) setSort(defaultSort)
    }
  }

  return {
    getFilterOptionsByName: name => useSelector(selectFilterOptionsByName(name)),
    getFilterByName: name => useSelector(selectFilterByName(name)),
    getFilter,
    getSort: useSelector(getSorted(id)),
    getInit,
    onChange,
    updateFilter,
    setFilters,
    setSort,
    values
  }
}

export default useFilter