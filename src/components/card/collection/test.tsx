/**
* React, Gatsby, Jest, TypeScript, Apollo - Starter
* https://github.com/eduard-kirilov/gatsby-ts-apollo-starter
* Copyright (c) 2020 Eduard Kirilov | MIT License
*/
import React, { FC } from 'react';
import { shallow } from 'enzyme';

import { CardCollection } from './CardCollection';
import { withTheme } from 'setup/withTheme';

describe('CardCollection', () => {
  const CardCollectionTest: FC = () => {
    return withTheme(
      <CardCollection
        select={() => {}}
        image={''}
        name="name"
      />
    );
  };

  describe('CardCollection container initial', () => {
    const mount = shallow(<CardCollectionTest />);
  it('CardCollection Test should work', () => {
      expect(mount.find('CardCollection')).toHaveLength(0);
    });
  });
});