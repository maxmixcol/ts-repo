import React, { FC } from 'react'
import {
  Toolbar,
} from 'components/core'

import {
  HeaderDialogWrapper,
  HeaderDialogWrapperDark,
} from './styled'
import { IChildren } from 'utils/interface'

export interface IHeaderProps extends IChildren {
  dark: boolean;
}
export interface IHeaderType {
  value: string;
  linkTo: string;
  size?: string;
}

export const HeaderDialog: FC<IHeaderProps> = ({
  dark = false,
  children
}) => {
  const Header = dark ? HeaderDialogWrapperDark : HeaderDialogWrapper
  return (
    <Header>
      <Toolbar>{children}</Toolbar>
    </Header>
  )
}
