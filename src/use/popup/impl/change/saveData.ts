import { IChangeCollectionListProps } from 'compose/popup/change/ChangeCollection';
import { GET_USER_CHANGE } from 'graphql/cache';
import { UPDATE_CHANGE_MUTATION, NEW_CHANGE_MUTATION } from 'graphql/mutation';
import { ChangeAction } from 'model/UserChange.model';
import UserExchangeModel from 'model/UserExchange.model';
import { TRefetch } from 'use/fetch/interface';
import { useControlledMutation } from 'use/fetch/useQuery';
import { useLayoutStore } from 'use/layout/useLayout';
export interface IApiChangeOutput {
  save: (action: ChangeAction, message: string) => void;
  saveChat: (message: string) => void;
  showProgress: boolean;
}

interface IUpdateChange {
  changeId?: number;
  userId: number;
  otherUserId: number;
  otherUserName: string;
  comment?: string;
}

export interface IApiChangeInput {
  changeId: number;
  offered: IChangeCollectionListProps[];
  requested: IChangeCollectionListProps[];
  updateProps: IUpdateChange;
  success: Function;
  successChat: Function;
  refetch: TRefetch;
}

const saveData = ({
  changeId,
  offered,
  requested,
  updateProps,
  success,
  successChat,
  refetch
}: IApiChangeInput): IApiChangeOutput => {
  const { openSpinner, closeSpinner } = useLayoutStore()
  const onSuccess = () => {
    success()
    closeSpinner()
  }
  const onFail = () => {
    closeSpinner()
  }
  const [ updateChange, updateResult ] = useControlledMutation({
    name: 'updateChange',
    mutation: UPDATE_CHANGE_MUTATION, 
    okMessage: true,
    refetch,
    success: onSuccess,
    fail: onFail
  })

  const [ newChange, newResult ] = useControlledMutation({
    name: 'newChange',
    mutation: NEW_CHANGE_MUTATION,
    okMessage: true,
    refetch,
    success: onSuccess,
    fail: onFail
  })

  const [ updateChat, chatResult ] = useControlledMutation({
    name: 'updateChange',
    mutation: UPDATE_CHANGE_MUTATION, 
    refetch,
    success: () => {
      const cache = chatResult.client.cache
      const uuid = `UserChange:${changeId}`
      const userChange = cache.readFragment({
        id: uuid,
        fragment: GET_USER_CHANGE, 
      })
      successChat(userChange)
      closeSpinner()
    },
    fail: onFail
  })

  const getParams = (action: ChangeAction) => ({
    ...updateProps,
    ...{
    action,
    offered: UserExchangeModel.serializeCols(offered),
    requested: UserExchangeModel.serializeCols(requested)
  }})

  const save = (action: ChangeAction, msg: string) => {

    const params = getParams(action)
    params.comment = msg

    openSpinner()
    if (action === ChangeAction.NEW) {
      newChange({ variables: { params }})
    } else {
      updateChange({ variables: { params }})
    }
  }

  const saveChat = (msg: string) => {
    const params = getParams(ChangeAction.CHAT)
    params.comment = msg
    openSpinner()
    updateChat({ variables: { params }})
  }

  return {
    save,
    saveChat,
    showProgress: updateResult.loading || newResult.loading || chatResult.loading
  }
}

export default saveData
