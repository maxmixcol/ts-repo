import { typeDefs } from './src/graphql/schema'

export { createPages } from './gatsby-create-pages'


export const createSchemaCustomization = ({ actions }) => {
  const { createTypes } = actions
  
  createTypes(typeDefs)
}