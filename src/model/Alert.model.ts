export interface IAlertModel {
  id: number;
  page: string;
  title: string;
  urlContent: string;
  urlTrigger: string;
}