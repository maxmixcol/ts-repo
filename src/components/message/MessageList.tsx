import React, { FC } from 'react';

import {
  ListWrapper
} from 'components/message/styled';
import { IChildren } from 'utils/interface';

interface IProps {
  items: any[];
  variant?: 'number';
}



export const MessageList: FC<IProps> = ({
  variant,
  items,
}) => { 

  const ItemWrapper = {
    number: NumberItem
  }[variant]

  
  return (
    <ListWrapper>
      {items.map((item, i) => <ItemWrapper key={i} index={i}>{item}</ItemWrapper>)}
    </ListWrapper>
  );
};



interface iNumber extends IChildren {
  index: number;
}
const NumberItem: FC<iNumber> = ({
  index,
  children
}) => <li><b>{ index+1 })</b> { children }</li>