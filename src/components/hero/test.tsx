import React, { FC } from 'react';
import { shallow } from 'enzyme';

import { Hero } from './Hero';
import { Button } from 'components/core';

import { IHeroProps } from './Hero'
import { withTheme } from 'setup/withTheme';

describe('Hero Container', () => {
  const HeroTest: FC<IHeroProps> = ({ children }) => {
    return withTheme(<Hero>{children}</Hero>);
  }
  describe('Hero container', () => {
    const mount = shallow(<HeroTest><Button /></HeroTest>);

    it('<Hero /> test should work', () => {
      expect(mount.find('Hero')).toHaveLength(0);
    });
    it('<Button /> test should work', () => {
      expect(mount.find('button')).toHaveLength(1);
    });
  });
});
