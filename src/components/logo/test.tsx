/**
 * React, Gatsby, Jest, TypeScript, Apollo - Starter
 * https://github.com/eduard-kirilov/gatsby-ts-apollo-starter
 * Copyright (c) 2020 Eduard Kirilov | MIT License
 */
import React from 'react';
import { shallow } from 'enzyme';

import { Logo } from './Logo';
import { withTheme } from 'setup/withTheme';

describe('Logo Container', () => {
  describe('Logo', () => {
    const mount = shallow(withTheme(
        <Logo />
    ));
    it('<Logo /> is authorized test should work', () => {
      expect(mount.find('Logo')).toHaveLength(0);
    });
  });
});
