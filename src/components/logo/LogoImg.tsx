import images from 'components/theme/images'
import React, { FC } from 'react'
import { Avatar } from 'components/core'


export const LogoImg: FC = ({
}) => {
  return (
    <Avatar src={images.logo} />
  )
}
