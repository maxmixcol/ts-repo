import { TFilterOptionUpdate } from "compose/filter/FilterCompose";
import { IUserCollectionExModel } from "model/UserCollectionEx.model";
import { useEffect, useState } from "react";
import { filters } from "setup/constants";
import useI18n from "use/i18n/useI18n"
import { ISortModelMap } from "utils/interface";
import FilterModel, { IFilterModelMap, TFilterMapper } from "../store/model/FilterModel";
import SortModel from "../store/model/SortModel";
import SortTypeEnum from "../store/model/SortTypeEnum";
import useFilter from "../useFilter";

interface FilterLastState {
  index: number;
  input: string;
  flat: string;
}

interface ISearchData {
  priority: number;
  name: string;
  year: number;
}
interface IUserCollectionExModelParsed extends IUserCollectionExModel {
  searchData: ISearchData
}

const filterId = filters.collection

/* SORT */
const sortLabelIds = {
  fav: 'filter.sort.fav',
  year: 'filter.sort.year',
}
enum CollectionSort {
  FAV = 'fav',
  YEAR = 'year',
} 
const SORT_MAP: ISortModelMap = {
  [CollectionSort.FAV]: [
    { name: 'searchData.priority', type: SortTypeEnum.Number, asc: false },
    { name: 'searchData.name', type: SortTypeEnum.String, asc: true }
  ],
  [CollectionSort.YEAR]: [
    { name: 'searchData.year', type: SortTypeEnum.Number, asc: false },
    { name: 'searchData.name', type: SortTypeEnum.String, asc: true }
  ]
}

/* FILTER */
const labelIds = {
  byCollection: 'filter.by.collection',
  byYear: 'filter.by.year',
  bySubject: 'filter.by.subject',
  searchCollection: 'filter.by.searchCollection'
}
enum CollectionFilter {
  YEAR = 'year',
  SUBJECT = 'subject',
  SEARCH = 'search',
  SORT = 'sort',
} 
const { YEAR, SUBJECT, SEARCH, SORT } = CollectionFilter

const FILTER_MAP: IFilterModelMap = {
  [YEAR]: { name: YEAR, label: labelIds.byYear, input: '' },
  [SUBJECT]: { name: SUBJECT, label: labelIds.bySubject, input: '' },
  [SEARCH]: { name: SEARCH, label: labelIds.searchCollection, input: '' },
  [SORT]: { name: SORT, label: '', input: CollectionSort.FAV },
}
const getSearchData = (item: IUserCollectionExModel): ISearchData => {
  const priority = item.userCollection.priority
  const name = FilterModel.flatName(item.userCollection.collection.name)
  const year = item.userCollection.collection.year

  return { priority, name, year }
}
const updateOptions = (list: IUserCollectionExModelParsed[]): TFilterOptionUpdate[] => {
  const searchOptions = {}
  const yearOptions = {}
  const subjectOptions = {}
  list.forEach(item => {
    const { userCollection, searchData } = item
    const { collection: ob } = userCollection
    yearOptions[ob.year] = { title: `${ob.year}`, value: ob.year }
    subjectOptions[ob.subject] = { title: ob.subject, value: ob.subject }
    searchOptions[ob.code] = searchData.name
  })
  return [
    { name: YEAR, options: FilterModel.EMPTY.concat(SortModel.sortOptions(yearOptions)) },
    { name: SUBJECT, options: FilterModel.EMPTY.concat(SortModel.sortOptions(subjectOptions)) },
    { name: SEARCH, query: (value: any, input: any) => searchOptions[value].indexOf(input) !== -1 },
  ]
}
const filterMapper: TFilterMapper<IUserCollectionExModel> = (
  { userCollection },
  { name, input, query }
) => {
  return {
    [SEARCH]: query && input && query(userCollection.collection.code, input),
    [SUBJECT]: userCollection.collection.subject === input,
    [YEAR]: userCollection.collection.year === input,
  }[name]
}

const useCollectionFilter = () => {
  const { t, tm } = useI18n()
  const sortLabels = tm(sortLabelIds)

  const {
    getFilter,
    getInit,
    onChange,
    updateFilter,
  } = useFilter(filterId)

  const init = () => {
    useEffect(() => { getInit(FILTER_MAP, t, CollectionSort.FAV)}, [])
  }

  const process = (list: IUserCollectionExModel[]) => {
    let listIds = list.map(ob => [ob.code,ob.userCollection.priority].join()).sort()
    const [ parsedList, setParsedList ] = useState<IUserCollectionExModelParsed[]>()

    useEffect(() => {
      if (list.length) {
        const sortFilterOptions = SortModel.updateSortOptions({
          sortMap: SORT_MAP, sortLabels
        })
        const parsedList = list.map(item => ({ ...item, ...{ searchData: getSearchData(item) }}))
        updateFilter(SORT, { options: sortFilterOptions })
        updateOptions(parsedList).forEach(o => updateFilter(o.name, o))
        setParsedList(parsedList)
      }
    }, [ listIds.join() ])

    return {
      model: getFilter || [],
      list: parsedList
    }
  }

  return {
    init,
    process,
    filterProps: {
      onChange,
      filterId,
      updateOptions,
      filterMapper,
      sortMap: SORT_MAP,
      sortName: SORT,
      searchName: SEARCH
    }
  }  
}

export default useCollectionFilter