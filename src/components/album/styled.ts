import { MaxiListWrapper } from 'compose/popup/change/styled'
import styled, { css } from 'styled-components'


export const AlbumWrapperStyled = styled.ul`
  margin: 0 0.2rem;
  padding: 1rem;
  list-style: none;

  display: grid;
  grid-template-columns: repeat(8,1fr);
  grid-column-gap: 3px;
  grid-row-gap: 3px;
  & > li {
    width: 7rem;
  }
`
export const MaxiListWrapperAlbum = styled(MaxiListWrapper)`
  
  & > li {
    span {
      height: 2rem;
      overflow: hidden;
    }
  }
  ${({ variant }) => variant === 'list' ?
     css`
      margin-top: 0.2rem;
     `
     : css`
        margin-top: 0.2rem;
        margin-bottom: 0.2rem;
        &, & > ul {
          height: auto;
          max-height: 10rem;
        }
    `}
`

export const GroupStyled = styled.p`
  margin: 0;
  height: 0.6rem;
  width: 14rem;
  font-size: 0.5rem;
`
