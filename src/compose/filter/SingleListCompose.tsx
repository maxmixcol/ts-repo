import React, { FC, useState } from 'react'
import { Title } from 'components/common/common'
import { ListHorizontal, ListVertical, TextStyled } from 'components/common/common.styled'
import { LoadMoreButton } from 'components/filter/common'
import useI18n from 'use/i18n/useI18n'


interface ISingleList {
  title?: string;
  list: any[];
  pageSize: number;
  onClick?: (e: any) => void;
  linkTo?: (item: any) => string;
  item: React.FC<any>;
  labelNum: string;
  variant?: 'horizontal' | 'vertical'
}

const labelIds = {
  loadMore: 'filter.loadmore',
  show: 'filter.show'
}

export const SingleListCompose: FC<ISingleList> = ({
  title = '',
  list,
  pageSize,
  onClick,
  linkTo,
  item,
  labelNum,
  variant = 'horizontal'
}) => {
  const { t, tm } = useI18n()
  const labels = tm(labelIds)
  const [ page, setPage ] = useState(0)
  const show = pageSize * (page + 1)
  const total = list.length
  const slicedList = list.slice(0, show)

  
  const fetchMore = total > pageSize && (pageSize * page) < total
    ? () => setPage(page + 1)
    : null

  const List = {
    horizontal: ListHorizontal,
    vertical: ListVertical
  }[variant]

  const totalLabel = (total: number) => t(labelNum, { num: total })
  const fullText = (show: number, total: number) => total > 0 ? t(labels.show, { show, total: totalLabel(total) }) : ''

  return (
  <>
    { title && <Title value={title} />}
    { (show && total) ? <TextStyled smallText>{show && show < total && fullText(show, total)}</TextStyled> : '' }
    <List
      list={slicedList}
      onClick={onClick}
      linkTo={linkTo}
      item={item}>
      { fetchMore && <LoadMoreButton onClick={fetchMore}>{labels.loadMore}</LoadMoreButton>}

    </List>
  </>
  )
}