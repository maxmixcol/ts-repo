import React, { FC, memo } from 'react';
import icons from 'components/theme/icons';

import { IChildrenOpt } from 'utils/interface';
import { ActionWrapper, CardBlockStyled, InfoWrapperStyled } from '../styled';


import { NameWrapper } from 'components/common/NameWrapper';
import { IconStyled } from 'components/button/styled';
import { CardHeaderChangeStyled, StateStyled, DateChangeStyled, CardBodyStyled, ChatChangeStyled, CardFooterStyled } from 'components/card/change/styled';
import { AvatarColor } from 'components/card/custom/styled';


export interface IChangeCard extends IChildrenOpt {
  name?: string;
  date?: string;
  stateActual?: string;
  chat?: string;
  image?: string;
}

const CardChangeMemo: FC<IChangeCard> = ({
  name = '',
  chat = '',
  image = '',
  date = '',
  stateActual = '',
  children = ''
}) => {
  return (
    <ActionWrapper>
      <CardBlockStyled>
        <CardHeaderChangeStyled>
          <AvatarColor src={image} alt={name}/>
          <InfoWrapperStyled>
            <StateStyled>{stateActual}</StateStyled>
            <DateChangeStyled>{date}</DateChangeStyled>
            <NameWrapper name={name}>{ name }</NameWrapper>
          </InfoWrapperStyled>
          
          <IconStyled icon={icons.unfold}/>
        </CardHeaderChangeStyled>
        { chat &&
          <CardBodyStyled>
            <ChatChangeStyled>{chat}</ChatChangeStyled>
          </CardBodyStyled>
        }
      </CardBlockStyled>
      <CardFooterStyled>
        {children}
      </CardFooterStyled>
    </ActionWrapper>
  );
};

export const CardChange = memo(CardChangeMemo);
