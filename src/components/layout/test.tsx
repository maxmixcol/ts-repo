/**
 * React, Gatsby, Jest, TypeScript, Apollo - Starter
 * https://github.com/eduard-kirilov/gatsby-ts-apollo-starter
 * Copyright (c) 2020 Eduard Kirilov | MIT License
 */
import React from 'react';
import { shallow } from 'enzyme';

import { Layout } from './Layout';
import { withTheme } from 'setup/withTheme';

describe('Layout Container', () => {
  describe('Layout', () => {
    const mount = shallow(
      withTheme(
        <Layout>
          <div>Test</div>
        </Layout>
      ));

    it('<Layout /> is authorized test should work', () => {
      expect(mount.find('Layout')).toHaveLength(0);
    });
  });
});
