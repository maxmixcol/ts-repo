import { ButtonList, IButtonListProps } from 'components/button/ButtonList';
import { NameWrapper } from 'components/common/NameWrapper';
import { FullModalStyled, HeroStyled } from 'compose/popup/_shared/styled';
import CollectionModel, { ICollectionModel } from 'model/Collection.model';
import React, { FC } from 'react';
import { IChildren } from 'utils/interface';
import { LinearProgressStyled } from 'components/status/styled'
import { BigNameCollectionStyled, CardRaisedCollectionStyled, DescriptionStyled } from './styled';
import { CardPopupStyled } from 'components/card/styled';

interface IProps extends IChildren {
  collection: ICollectionModel;
  open?: boolean;
  close?: () => void;
  refresh?: boolean;
  showProgress?: boolean;
  description?: boolean;
  noPadding?: boolean;
  contentHero?: React.ReactNode;
  buttons: IButtonListProps[];
  title?: React.ReactNode;
}
export const PopupCollection: FC<IProps> = ({
  collection,
  open,
  close,
  refresh,
  showProgress,
  description,
  contentHero,
  noPadding,
  buttons,
  title,
  children,
}) => {

  const { code, name = '', description: collectionDescription } = collection || {}
  const image = code && CollectionModel.getImage(code)

  return (<>{ open && code &&
    <FullModalStyled
      open={open}
      close={close}
      headerDark={true}
      title={title || <BigNameCollectionStyled>{name}</BigNameCollectionStyled>}
    >
      { showProgress && <LinearProgressStyled /> }
      <div>
        <HeroStyled image={image} name={name} reduced={true}>
          <NameWrapper big name={name} />
        </HeroStyled>
        { description && (
          <DescriptionStyled>
            <div dangerouslySetInnerHTML={{ __html: collectionDescription }} />
          </DescriptionStyled>)}
        { contentHero }
      </div>
      <CardPopupStyled noPadding={noPadding}>
        <>
          { children }
          {!refresh && !!buttons.length && <ButtonList buttons={buttons} />}
        </>
      </CardPopupStyled>
    </FullModalStyled>
  }</>);
};
