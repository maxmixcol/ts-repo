import { TRefetch } from "use/fetch/interface"

export const isOpen = (id: string) => (state:any) => (state.modal[id] || {}).open
export const getData = (id: string) => (state:any) => (state.modal[id] || {}).data
export const getRefetch = (id: string) => (state:any): TRefetch => (state.modal[id] || {}).refetch