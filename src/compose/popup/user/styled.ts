import styled, { css } from 'styled-components'
import icons from 'components/theme/icons'
import { ISize } from 'utils/interface'
import { RawButtonWithIcon } from 'components/button/RawButtonWithIcon'
import { CardRaisedStyled } from 'compose/popup/_shared/styled'
import { Card } from 'components/core'

const getNameSize = size => ({
  [ISize.S]: 1.35,
  [ISize.M]: 1.2,
  [ISize.L]: 1
}[size])

export const BigNameUserStyled = styled.span`
  font-size: ${({ size }) => getNameSize(size)}rem;
  z-index: 1;
`
export const LikeButtonStyled = styled(RawButtonWithIcon)`
  & svg {
    margin: 0 0.5rem;
  }
`

