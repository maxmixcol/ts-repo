import React, { useEffect } from "react"

import { AUTH_TAB } from "components/popup/auth/PopupAuth"
import { EmptyView } from "components/typography/view/common"
import { PopupAuthCompose } from "compose/popup/PopupAuthCompose"

import useAuth from "use/auth/useAuth"
import { useModalAuth } from "use/modal/useModal"
import { IndexHeroCompose } from "compose/hero/IndexHeroCompose"

export default function Template(props) {
	const { saveResetData, logout } = useAuth()
	const { open } = useModalAuth()

	useEffect(() => {
		logout()
		const { selector, token } = props.params

		saveResetData({
			selector,
			token
		})

		open()
	}, [])

	return (
		<EmptyView>
      <IndexHeroCompose page={'reset'} />
			<PopupAuthCompose defaultTab={AUTH_TAB.RESET} />
		</EmptyView>
	)
}