import React, { FC, memo } from 'react'
import { navigate  } from 'gatsby'

import { ToolbarStyled, TabStyled, TabsStyled } from './styled'
import { Icon } from 'components/styled'
import { IMenuLink, IMenuProps } from 'utils/interface'




const StatusBarMemo: FC<IMenuProps> = ({
  index,
  links = []
}) => {
  const handleClick = (href: string) => (e: any) => {
    e.preventDefault()
    navigate(href)
  }

  return (
    <ToolbarStyled>
      <TabsStyled
        value={index}
        >{links.map((item: IMenuLink, i: number) => {
          const { text, icon, href } = item;
          return (
            <TabStyled
              onClick={handleClick(href)}
              key={i}
              value={i}
              href={href}
              label={text}
              icon={<Icon icon={icon} size={'2x'}/>} />
          )
        })}
      </TabsStyled>
    </ToolbarStyled>
  )
}

export const StatusBar = memo(StatusBarMemo);