import { isBrowser } from 'utils/helpers';
import environment from './base';

// const base = 'https://acabaramoscom.globat.com/dev/';
const base =  `${isBrowser() ? window.location.protocol : 'https:'}//acabaramos.com/`;
const baseApi = `${base}api.php/`;
const baseAuth = `${base}auth.php`;

const env = environment(base, baseApi, baseAuth);

const developmentEnv = {
  ...env,
  // override anything that gets added from base.
  api: {
    ...env.api,
    // error200: `${baseApi}/api/v1/error-200`,
    // error500: `${baseApi}/api/v1/error-500`,
  },
  isProduction: false,
  isDevelopment: true,
};

export default developmentEnv;
