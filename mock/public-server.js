const express = require('express');
const app = new express();
const port = process.env.PORT || 3000;
const PATH = __dirname.replace('/mock', '/public');

app.use('/dev2', express.static(PATH));
app.use(express.static(PATH));

app.get('/', function(req, res){
    const _path = `${PATH}/index.html`;
    res.sendFile(_path);
});

app.listen(port || 3000, () => {
    console.log('Server started! => At http://localhost:' + port);
});