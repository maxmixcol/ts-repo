import React, { FC, useEffect, useRef, useState } from 'react';
import useI18n from 'use/i18n/useI18n';
import { DescriptionWrapperStyled, DescriptionTextStyled, MoreButton } from 'compose/styled';

interface IProps {
  className?: string;
  toggle?: boolean;
}
export const Description: FC<IProps> = ({
  className,
  toggle,
  children
}) => {
  const { t } = useI18n()
  const more = t('common.more')
  const less = t('common.less')
  const [ opened, open ] = useState(false)
  const [ showButton, setShowButton ] = useState(false)
  const onClick = () => {
    open(!opened)
    if (!toggle) {
      setShowButton(false)
    }
  }
  const containerRef = useRef(null);
  useEffect(() => {
    const hasClamping = (el) => {
      const { clientHeight, scrollHeight } = el;
      return clientHeight !== scrollHeight;
    };
    if (containerRef.current) {
      setShowButton(hasClamping(containerRef.current));
    }
  }, [containerRef])

  return (
    <DescriptionWrapperStyled className={className}>
      <DescriptionTextStyled
        ref={containerRef}
        className={`${opened?' opened':''}`}
        >{ children }
      </DescriptionTextStyled>
      { showButton && <MoreButton onClick={onClick}>{ opened ? less : more }</MoreButton>}
    </DescriptionWrapperStyled>
  )
}

