import { AlbumWrapper } from 'components/album/AlbumWrapper';
import { IAlbumProps } from 'compose/popup/change/Album';
import { IElementModel } from 'model/Element.model';
import UserCollectionExModel from 'model/UserCollectionEx.model';
import React, { FC } from 'react';
import { AlbumEditStyled, EditContainerStyled } from './styled';

export interface IEditContainerProps {
  labels: {
    title: string;
    footer: string;
    faults: string;
    repeated: string; 
  };
  elements: IElementModel[];
  albumProps: Omit<IAlbumProps, 'items'>
}

export const EditContainer: FC<IEditContainerProps> = ({
  labels,
  elements,
  albumProps,
}) => {
  const paginate = UserCollectionExModel.paginateWithGroups
  return (
    <EditContainerStyled>
      <AlbumWrapper
        album={AlbumEditStyled}
        albumProps={albumProps}
        pages={paginate(elements, 12)} />
    </EditContainerStyled>
  );
};
