import { modals, routes } from 'setup/constants'
import useI18n from 'use/i18n/useI18n'
import useFullModal, { IProcess } from '../useFullModal'
import { useQueryAll } from 'use/fetch/useQuery'
import UserCollectionExModel, { IUserCollectionExModel } from 'model/UserCollectionEx.model'
import { POPUP_COLLECTION_SHARE_QUERY } from 'graphql/query'
import { useEffect, useState } from 'react'
import useAuth from 'use/auth/useAuth'
import { IPopupCollectionShareData } from 'use/popup/impl/interface'
import { IPopupCollectionShareResult } from 'compose/popup/interface'
import UserElementModel, { IUserElementModelEx } from 'model/UserElement.model'
import DateModel, { IDateFormat } from 'model/Date.model'


const modalId = modals.collectionShare
const labelIds = {
  text: 'popup.collection.share.text',
  title: 'popup.collection.share.title',
  brand: 'common.url',
  faults: 'popup.collection.share.faults',
  repeated: 'popup.collection.share.repeated',
  loading: 'popup.collection.share.loading',
  description: 'popup.collection.share.description',
}

const usePopupCollectionShare = () => useFullModal<IPopupCollectionShareData, IPopupCollectionShareResult>({
  labelIds,
  modalId,
  process
})

export default usePopupCollectionShare

function process (data: IPopupCollectionShareData): IPopupCollectionShareResult & IProcess {
  const { t, tm } = useI18n()
  const labels = tm(labelIds)
  const userId = useAuth().user
  const { collection } = data || {}
  const { code, name } = collection || {}

  const text = t(labelIds.text, { name })
  const [ url, setUrl ] = useState<string>()
  const handleImage = (img: string) => {
    setUrl(routes.imgShare(img))
  }

  // DATA MANAGEMENT
  const elementsQuery = useQueryAll<IUserCollectionExModel>('userCollectionEx', POPUP_COLLECTION_SHARE_QUERY)
  const { elements = [] } = (elementsQuery.list || [])[0] || {}
  
  useEffect(() => {
    setUrl('')
    elementsQuery.get({
      codes: [code],
      userId,
      elements: true,
      userElements: true,
    })
  }, [code])

  const today = DateModel.format(new Date().getTime(), IDateFormat.short)
    const { mappedElements = {}, mappedUserElements = {} } = (UserCollectionExModel.toElementMaps(elementsQuery.list) || {})[code] || {}
  const faults = Object.values(mappedUserElements)
    .filter((e: IUserElementModelEx) => UserElementModel.isFault(mappedUserElements[e.order]))
    .map((e: IUserElementModelEx) => mappedElements[e.order])
  const repeated = Object.values(mappedUserElements)
    .filter((e: IUserElementModelEx) => UserElementModel.isRepeated(mappedUserElements[e.order]))
    .map((e: IUserElementModelEx) => mappedElements[e.order])
  
  const title = `${name} (${labels.brand}) ${today}`

  return {
    collection,
    shareContainerProps: {
      elements,
      faults,
      repeated,
      albumProps: {
        elements: mappedElements,
        userElements: mappedUserElements
      },
      labels: {
        title,
        footer: labels.brand,
        faults: labels.faults,
        repeated: labels.repeated,
      }
    },
    loadingCollectionData: elementsQuery.loading,
    url,
    text,
    handleImage,
    labels
  }
}