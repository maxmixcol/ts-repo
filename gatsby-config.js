module.exports = {
  pathPrefix: `/latest`,
  // pathPrefix: `/dev/latest`,

  siteMetadata: {
    title: `Acabáramos`,
    description: "ACABARAMOS.COM es la herramienta definitiva para coleccionistas. Almacena colecciones ILIMITADAS GRATIS, encuentra los MEJORES CAMBIOS de forma SENCILLA, PUBLICA tu colección en FACEBOOK o TWITTER con un CLICK, CHATEA con otros coleccionistas. ACABARAMOS.COM, la herramienta definitiva para coleccionistas.",
    siteUrl: `https://acabaramos.com/`,
  },
  plugins: ["gatsby-plugin-styled-components", {
    resolve: 'gatsby-plugin-google-analytics',
    options: {
      "trackingId": "UA-122254422-1"
    }
  }, {
    resolve: `gatsby-plugin-sharp`,
    options: {
      icon: `src/assets/images/logo.png` //favicon
    }
  }, "gatsby-plugin-react-helmet", "gatsby-plugin-sitemap", `gatsby-plugin-layouts`, `gatsby-plugin-resolve-src`, "gatsby-transformer-remark", "gatsby-transformer-sharp", {
    resolve: 'gatsby-source-filesystem',
    options: {
      name: "assets",
      path: "./src/assets/"
    },
    __key: "assets"
  }, {
    resolve: 'gatsby-source-filesystem',
    options: {
      name: "pages",
      path: "./src/pages/"
    },
    __key: "pages"
  },{
    resolve: 'gatsby-source-filesystem',
    options: {
      name: "markdown-pages",
      path: "./src/markdown-pages",
    },
    __key: "md"
  },{
    resolve: `gatsby-plugin-alias-imports`,
    options: {
      alias: {
        "assets": "src/assets",
        "components": "src/components",
        "config": "src/config",
        "images": "src/images",
        "utilities": "src/utilities",
      }
    }
  }]
};