/**
 * React, Gatsby, Jest, TypeScript, Apollo - Starter
 * https://github.com/eduard-kirilov/gatsby-ts-apollo-starter
 * Copyright (c) 2020 Eduard Kirilov | MIT License
 */
import React from 'react';
import styled, { css } from 'styled-components';
import { Theme } from '@material-ui/core/styles';

interface IPropsWeight {
  [key: string]: number;
}
interface IPropsTypography {
  [key: string]: any;
}
interface IProps {
  theme: Theme;
  weight: string;
  mb?: string;
  mt?: string;
}

const switchWeight = (type: IPropsTypography): IPropsWeight => ({
  light: type.fontWeightLight,
  regular: type.fontWeightRegular,
  medium: type.fontWeightMedium,
  bold: type.fontWeightBold,
});

let Title1
let Title2
let Title3
let Title4
let Title5
if (typeof window == "undefined") {
  Title1 = ({children}) => (<h1>{children}</h1>)
  Title2 = ({children}) => (<h2>{children}</h2>)
  Title3 = ({children}) => (<h3>{children}</h3>)
  Title4 = ({children}) => (<h4>{children}</h4>)
  Title5 = ({children}) => (<h5>{children}</h5>)
} else {
  Title1 = styled.h1<IProps>`
    font-family: ${({ theme }) => theme.typography.h1.fontFamily};
    font-weight: ${({ theme, weight }) => {
      const types = switchWeight(theme.typography);
      return weight in types ? types[weight] : types.regular;
    }};
    font-size: ${({ theme }) => theme.typography.h1.fontSize};
    margin: 0;
    ${({ mb }) => mb && css`margin-bottom: ${mb}px;`};
    ${({ mt }) => mt && css`margin-top: ${mt}px;`};
  `;
  Title2 = styled.h2<IProps>`
    font-family: ${({ theme }) => theme.typography.h2.fontFamily};
    font-weight: ${({ theme, weight }) => {
      const types = switchWeight(theme.typography);
      return weight in types ? types[weight] : types.regular;
    }};
    font-size: ${({ theme }) => theme.typography.h2.fontSize};
    margin: 0;
    ${({ mb }) => mb && css`margin-bottom: ${mb}px;`};
    ${({ mt }) => mt && css`margin-top: ${mt}px;`};
  `;
  Title3 = styled.h3<IProps>`
    font-family: ${({ theme }) => theme.typography.h3.fontFamily};
    font-weight: ${({ theme, weight }) => {
      const types = switchWeight(theme.typography);
      return weight in types ? types[weight] : types.regular;
    }};
    font-size: ${({ theme }) => theme.typography.h3.fontSize};
    margin: 0;
    ${({ mb }) => mb && css`margin-bottom: ${mb}px;`};
    ${({ mt }) => mt && css`margin-top: ${mt}px;`};
  `;
  Title4 = styled.h4<IProps>`
    font-family: ${({ theme }) => theme.typography.h4.fontFamily};
    font-weight: ${({ theme, weight }) => {
      const types = switchWeight(theme.typography);
      return weight in types ? types[weight] : types.regular;
    }};
    font-size: ${({ theme }) => theme.typography.h4.fontSize};
    margin: 0;
    ${({ mb }) => mb && css`margin-bottom: ${mb}px;`};
    ${({ mt }) => mt && css`margin-top: ${mt}px;`};
  `;
  Title5 = styled.h5<IProps>`
    font-family: ${({ theme }) => theme.typography.h5.fontFamily};
    font-weight: ${({ theme, weight }) => {
      const types = switchWeight(theme.typography);
      return weight in types ? types[weight] : types.regular;
    }};
    font-size: ${({ theme }) => theme.typography.h5.fontSize};
    margin: 0;
    ${({ mb }) => mb && css`margin-bottom: ${mb}px;`};
    ${({ mt }) => mt && css`margin-top: ${mt}px;`};
  `;
}

export {
  Title1,
  Title2,
  Title3,
  Title4,
  Title5
}