import { IUserElementModel } from 'model/UserElement.model';
import React, { FC, ReactNode } from 'react';
import { IElementMap, TElement } from 'utils/interface';
import { MiniCard } from '../../../components/popup/change/element/styled';

export interface IAlbumProps {
  items?: TElement[];
  elements?: IElementMap;
  userElements?: {[key: string]: IUserElementModel};
  extraContent?: {[key: string]: ReactNode };
  item?: React.FC;
  onClick?: (o: string, i: number) => void;
  variant?: string;
  className?: string;
}
export const Album: FC<IAlbumProps> = ({
  items = [],
  elements = {},
  userElements = {},
  extraContent = {},
  item,
  onClick,
  variant,
  className
}) => {
  const Item = item || MiniCard
  return (
    <ul className={`${className || ''} ${variant || ''}`}>
      {items.map((o, i) => {
        const el = elements[o]
        const uel = userElements[o]
        const xtra = extraContent[o]
        return (
          <li key={`${o}_${i}`} onClick={onClick ? () => onClick(o, i) : null}>
            <Item {...el} {...uel}>{o}</Item>
            { xtra }
          </li>
        )
      })}
    </ul>
  );
};
