
import React, { FC } from 'react';
import icons from 'components/theme/icons';
import { Icon } from 'components/styled';
import { RawButtonWithIcon } from 'components/button/RawButtonWithIcon';
import {
  FacebookShareButton,
  TwitterShareButton,
  WhatsappShareButton,
} from 'react-share'

// const isMobileOrTablet = () => {
//   return /(android|iphone|ipad|mobile)/i.test(navigator.userAgent);
// }
// const whatsappLink = (url, { text }) => {
//   return `https://${isMobileOrTablet() ? 'api' : 'web'}.whatsapp.com/send?text=${encodeURIComponent(text)}%20${encodeURIComponent(url)}`
// }

// const WhatsappShareButton = ({ children, className, url, text = '' }) => {
//   const href = whatsappLink(url, { text })
//   return (
//     <a href={href} rel="nofollow noopener" target="_blank" className={ className }><img src={url} style={{height:'36px'}}/>{ children }</a>
//   )
// }
interface IProps {
  variant: 'facebook' | 'whatsapp' | 'twitter';
  url: string;
  text: string;
  className?: string;
}
export const ShareButton: FC<IProps> = ({
  variant,
  url,
  text,
  className,
}) => {
  
  const Button = {
    facebook: FacebookShareButton,
    twitter: TwitterShareButton,
    whatsapp: WhatsappShareButton,
  }[variant] || RawButtonWithIcon;

  const params = {
    facebook: {
      quote: text,
      hashtag: 'acabaramos'
    },
    twitter: {
      title: text,
      hashtags: ['acabaramos']
    },
    whatsapp: {
      title: text
    },
  }[variant] || {}

  return (
    <Button url={url} {...params} className={`${className || ''} ${variant}`} resetButtonStyle={false}>
      <Icon icon={icons[variant]} />
    </Button>      
  );
};
