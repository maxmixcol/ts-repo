import { IUserChangeModel } from 'model/UserChange.model'
import { IExchangeData } from 'model/UserExchange.model'
import { useDispatch, useSelector } from 'react-redux'
import { actions, get } from './store'

const { getChange, getExchanges } = get
const { setChange, setExchanges } = actions

const useExchange = () => {
  const dispatch = useDispatch()
  return {
    change: useSelector(getChange),
    exchanges: useSelector(getExchanges),
    setChange: (change: IUserChangeModel) => dispatch(setChange(change)),
    setExchanges: (exchanges: IExchangeData[]) => dispatch(setExchanges(exchanges))
  }
}

export default useExchange