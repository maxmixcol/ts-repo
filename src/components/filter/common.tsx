import React, { FC } from 'react'

import { IconButton } from 'components/core'
import { Icon } from 'components/styled'
import { IChildren } from 'utils/interface'
import { IconDefinition } from 'components/theme/icons';
import { ListButtonStyled } from './styled'
import { Button } from 'components/button/custom/styled'



export interface IFilterButtonProps extends IChildren {
  onClick: () => void;
  className?: string;
}

export const FilterButton: FC<IFilterButtonProps> = ({
  onClick,
  className,
  children
}) => (
    <Button 
      className={className}
      onClick={onClick}>
      {children}
    </Button>
)

interface IProps {
  className: string;
  icon: IconDefinition;
  onClick: () => void;
}

export const FilterIconButton: FC<IProps> = ({
  className,
  icon,
  onClick,
}) => <IconButton className={className} onClick={() => onClick()}
        ><Icon icon={icon}/></IconButton>


interface ILoadMoreProps extends IChildren {
  onClick: () => void;
}

export const LoadMoreButton: FC<ILoadMoreProps> = ({
  onClick,
  children
}) => {
  return (
    <ListButtonStyled onClick={onClick}>
      {children}
    </ListButtonStyled>
  )
}