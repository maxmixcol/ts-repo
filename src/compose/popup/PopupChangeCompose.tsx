import React, { FC, memo, useState } from 'react'

import { IAllStringProps } from 'utils/interface'

import { Grid } from 'components/core'
import { NameWrapper } from 'components/common/NameWrapper'
import { AvatarCompose } from 'compose/img/AvatarCompose'

import { IntroMessageCompose } from './change/IntroMessageCompose'

import {
  ButtonAvatarStyled,
  CustomStepperBlockStyled,
  CustomStepperStyled,
  DrawerBottom,
  ChangeWrapperFullStyled,
} from './change/styled'
import usePopupChange from 'use/popup/impl/usePopupChange'
import { ChatCard } from './change/card/ChatCard'
import { ChangeCard } from './change/card/ChangeCard'
import { TotalCard } from './change/card/TotalCard'

import { AddressCompose } from './change/AddressCompose'
import { LinearProgressStyled } from 'components/status/styled'
import { ChangeTotalCompose } from 'compose/popup/change/total/ChangeTotalCompose'
import { Tabs } from 'components/tabs/Tabs'
import { useTheme, useMediaQuery } from '@material-ui/core'
import { FullModalStyled } from 'compose/popup/_shared/styled'
import { IPopupChangeResult } from 'compose/popup/interface'


const PopupChangeComposeMemo: FC = () => {
  const theme = useTheme()
  const bigRes = useMediaQuery(theme.breakpoints.up('lg'))

  const { labels, result, fullModal }: {
    labels: IAllStringProps,
    result: IPopupChangeResult,
    fullModal: any
  } = usePopupChange()
  const {
    changeTitle,
    chatTitle,
    chatLabel,
    intro,
    introEditing,
    title,
  } = labels

  const {
    allowChat,
    chatList,
    chatMessage,
    editing,
    elementData,
    address,
    addressState,
    name,
    user,
    offered,
    requested,
    labelFn,
    sortFn,
    updateChat,
    stepList,
    stepNum,
    showProgress,
    showUserData,
    total,
    saving,
    actionLinksChange,
    actionLinksChat,
    actionLinksTotal
  } = result

  if (!result || !user) return <></>

  const changeCard = (
    <ChangeCard
      header={changeTitle}
      actionLinks={saving || editing ? [] : actionLinksChange}
      showViewMode={!editing && total.offered + total.requested > 0}
      showWrapper={bigRes && !editing}
      readonly={!editing}
      requested={requested}
      offered={offered}
      elementData={elementData || {}}
      sortFn={sortFn}
      labelFn={labelFn}
      >
        <AddressCompose state={addressState} {...address} />
        { editing ?
          <IntroMessageCompose label={introEditing} />
          : <p>{ intro }</p>}
        
      </ChangeCard>
  )

  const chatCard = editing ? <></> : (
    <ChatCard
      allowChat={allowChat}
      showWrapper={bigRes}
      header={chatTitle}
      label={chatLabel}
      chatList={chatList}
      message={chatMessage}
      onChange={updateChat}
      actionLinks={bigRes && actionLinksChat}>
    </ChatCard>
  )
  
  const [ activeTab, setActiveTab ] = useState('change')
  const onChangeTab = (tab) => {
    setActiveTab(tab)
  }
  let actionButtons = actionLinksTotal
  if (editing) actionButtons = actionLinksChange
  if (activeTab === 'chat') actionButtons = bigRes ? [] : actionLinksChat
  if (saving) actionButtons = []

  const totalCard = (
    <TotalCard actionLinks={actionButtons}>
      <ChangeTotalCompose
        offered={total.offered}
        requested={total.requested}
        errorMessage={total.errorMessage}
       />
    </TotalCard>)



  return (
    <>{ fullModal.open &&
      <FullModalStyled
        {...fullModal}
        fullScreen={true}
        title={<>
          <AvatarCompose user={user} hideBadge button={ButtonAvatarStyled} />
          <NameWrapper big name={name.slice(10)}>
            {title}
          </NameWrapper>
        </>}
      >
      { showProgress && <LinearProgressStyled /> }
      <PopupChangeWrapper>
        { !editing && 
          <>
            { !!stepNum && (
              <>
                <CustomStepperStyled variant="horizontal" steps={stepList} step={stepNum} />
                <CustomStepperBlockStyled />
              </>)}
            { bigRes 
              ? <>
                  {changeCard}
                  {showUserData && chatCard}
                </>
              : <Tabs
              onChangeTab={onChangeTab}
              tabs={[
                {
                  id: 'change',
                  button: changeTitle,
                  content: changeCard
                }
              ].concat(showUserData ? [{
                  id: 'chat',
                  button: chatTitle,
                  content: showUserData && chatCard
                }] : [])
              } />}
          </>}
        { editing && changeCard }
        
      </PopupChangeWrapper>
      <DrawerBottom>
        { totalCard }
      </DrawerBottom>
    </FullModalStyled>
  }
  </>
  )
}

export const PopupChangeCompose = memo(PopupChangeComposeMemo)

const PopupChangeWrapper: FC = ({ children }) => (
  <ChangeWrapperFullStyled>
    <Grid container spacing={1}>
      {children}
    </Grid>
  </ChangeWrapperFullStyled>
)
