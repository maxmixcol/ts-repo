import { BaseModel } from 'sjs-base-model';

export class Model extends BaseModel {
  update (data, conversionOptions = {}) {
    const options = Object.keys(data).reduce((tot, item) => {
      tot[item] = conversionOptions[item]
      return tot
    }, {})
    try {
    super.update(data, options);
    } catch (e) {
      debugger
    }
  }
}