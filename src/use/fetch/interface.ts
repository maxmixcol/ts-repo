import { ApolloError, DocumentNode } from "@apollo/client";

export type TRefetch = [DocumentNode, string]
export interface IRefetch {
  refetch: TRefetch;
}
export interface IMutationInput {
  name: string;
  mutation: DocumentNode;
  success?: Function;
  fail?: Function;
  refetch?: TRefetch;
  okMessage?: boolean;
}
export interface IVariables {
  page: IPageProps;
  filter: any;
}
export interface IPageProps {
  page: number;
  sort?: string;
  per_page: number;
  direction: string;
  noMerge?: boolean;
}
// interface IQueryFields extends ObservableQueryFields<DocumentNode, IVariables> {
// }

export interface IQueryFields<T> {
  error: ApolloError,
  loading: boolean,
  fetchMore?: () => void,
  get?: (filterProps: any, page?: number) => void;
  list?: T[]
  total?: number;
}