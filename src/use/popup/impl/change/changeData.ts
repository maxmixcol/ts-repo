import { useEffect, useState } from 'react'
import icons from 'components/theme/icons'
import { DASHBOARD_CHANGES_SEARCH_QUERY, POPUP_CHANGE_ELEMENTS_QUERY, USER_ADDRESS } from 'graphql/query'
import { ChangeOffer, ChangeStateIndex } from 'model/UserChange.model'
import UserExchangeModel, { IExchangeData, IUserExchangeModel } from 'model/UserExchange.model'
import { useQueryAll } from 'use/fetch/useQuery'
import { IChangeCollectionListProps } from 'compose/popup/change/ChangeCollection'
import { ITranslate } from 'utils/interface'
import UserInfoModel, { AddressState, IAddress, IUserInfoModel } from 'model/UserInfo.model'
import UserCollectionExModel, { IUserCollectionExModel } from 'model/UserCollectionEx.model'
import { IPopupChange } from 'compose/popup/interface'

const labelIds = {
  buttonAddress: 'popup.change.button.address',
  buttonDone: 'popup.change.button.done',
  buttonEdit: 'popup.change.button.edit',
  buttonRandom: 'popup.change.button.random',
  buttonSelect: 'popup.change.button.select',
  offered: 'popup.change.offered',
  requested: 'popup.change.requested',
  results: 'popup.change.results',
  resultsZero: 'popup.change.resultsZero',
}
const {
  NEW, INIT, ACCEPTED, RECEIVED, NOT_RECEIVED, FINISHED
} = ChangeStateIndex

const allowedEditStates = [NEW, INIT]
const allowedAddressStates = [ACCEPTED]
const allowedSelfAddressStates = [RECEIVED]
const hiddenAddressButtonStates = [AddressState.LOADING, AddressState.LOADED]
export interface IChangeDataOutputProps extends IPopupChange {
  edited: boolean;
  loading: boolean;
  saving: boolean;
  setSaving: Function;
}
export interface IChangeDataInputProps {
  state: ChangeStateIndex;
  exchanges: [IExchangeData];
  isSelf: boolean;
  isNew: boolean;
  userId: number;
  userName: string;
  otherUserId: number;
}
const changeData = (
  t: any,
  tm: any,
  props: IChangeDataInputProps,
): IChangeDataOutputProps => {
  const {
    state,
    exchanges,
    isSelf,
    isNew,
    userId,
    userName,
    otherUserId
  } = props
  const labels = tm(labelIds)

  const [ requested, setRequested ] = useState<IChangeCollectionListProps[]>([])
  const [ offered, setOffered ] = useState<IChangeCollectionListProps[]>([])
  const [ editing, setEditing ] = useState(false)
  const [ edited, setEdited ] = useState(false)
  const [ saving, setSaving ] = useState(false)
  const [ address, setAddress ] = useState<Partial<IAddress>>(null)
  const [ addressState, setAddressState ] = useState<AddressState>(AddressState.NEW)

  const allowEdit = allowedEditStates.indexOf(state) !== -1
  const allowAddress = !address &&
    (allowedAddressStates.indexOf(state) !== -1
      || (isSelf && allowedSelfAddressStates.indexOf(state) !== -1))
  const showAddressButton = hiddenAddressButtonStates.indexOf(addressState) === -1

  labels.requested = t(labelIds.requested, { name: userName })

  const getDefault = (newExchanges?: [IExchangeData]): IChangeCollectionListProps[][] => {
    const r = UserExchangeModel.exchangesProccess(exchanges, 'his', isNew)
    const o = UserExchangeModel.exchangesProccess(exchanges, 'mine', isNew)
    if (newExchanges) {
      const newReq = UserExchangeModel.exchangesProccess(newExchanges, 'his', true)
      const newOff = UserExchangeModel.exchangesProccess(newExchanges, 'mine', true)
      const joinedReq = UserExchangeModel.joinCols(r, newReq)
      const joinedOff = UserExchangeModel.joinCols(o, newOff)

      return [ joinedOff, joinedReq ]
    } else {
      return [o, r]
    }
  }
  const updateSelection = (off: IChangeCollectionListProps[], req: IChangeCollectionListProps[]) => {
    setOffered(UserExchangeModel.sortAll(off))
    setRequested(UserExchangeModel.sortAll(req))
  }

  const randomSelect = () => {
    const empty = { selected: [], unselected: [] }
    updateSelection(
      offered.map(item => ({ ...item, ...empty})),
      requested.map(item => ({ ...item, ...empty})),
    )
    setTimeout(() => {
      const [ randomOff, randomReq ] = UserExchangeModel.auto(offered, requested)
      updateSelection(randomOff, randomReq)
      setEdited(true)
    }, 300)
  }

  const onSelectRequested = (data: IChangeCollectionListProps) => {
    setRequested(requested.map(item =>
      item.code === data.code ? { ...item, ...data } : item))
    setEdited(true)
  }

  const onSelectOffered = (data: IChangeCollectionListProps) => {
    setOffered(offered.map(item =>
      item.code === data.code ? { ...item, ...data } : item))
    setEdited(true)
  }
  const refresh = () => {
    const exchangesList:  IUserExchangeModel[] = newExchangesQuery.list;
    const [ o, r ] = getDefault(exchangesList.length && exchangesList[0].exchanges)

    loadElements(exchanges.concat(exchangesList.length ? exchangesList[0].exchanges : []))
    updateSelection(o, r)
  }

  const elementsQuery = useQueryAll<IUserCollectionExModel>('userCollectionEx', POPUP_CHANGE_ELEMENTS_QUERY)
  const newExchangesQuery = useQueryAll<IUserExchangeModel>('allNewExchanges', DASHBOARD_CHANGES_SEARCH_QUERY, 
    { onCompleted: refresh })
  const addressQuery = useQueryAll<IUserInfoModel>('userInfo', USER_ADDRESS,
    { onCompleted: (e) => {
      setAddress(UserInfoModel.getAddress(e?.userInfo[0]))
      setAddressState(AddressState.LOADED)
    }, onError: (e) => {
      setAddress(null)
      setAddressState(AddressState.ERROR)
    }
    }
  )

  const loadElements = (exchanges: IExchangeData[]) => {
    elementsQuery.get({
      codes: UserExchangeModel.getCodes(exchanges),
      userId,
      elements: true
    })
  }

  useEffect(() => {
    updateSelection([], [])
    setEditing(false)
    setEdited(false)
    setAddress(null)
    setSaving(false)
    setAddressState(AddressState.NEW)

    if (otherUserId && allowEdit && !isNew) {
      newExchangesQuery.get({
        offer: ChangeOffer.FILLED,
        userId,
        otherUserId
      })
    } else {
      const [ o, r ] = getDefault()
      loadElements(exchanges)
      updateSelection(o, r)
    }
  }, [ exchanges ])

  const doRandom = () => { randomSelect() }
  const doDone = () => setEditing(false)
  const doEdit = () => setEditing(true)
  const doLoadAddress = () => {
    setAddress({})
    setAddressState(AddressState.LOADING)
    addressQuery.get({
      userIds: [otherUserId],
      userId
    })
  }

  const b = {
    random: { icon: icons.random, text: labels.buttonRandom, onClick: doRandom },
    done: { icon: icons.check, text: labels.buttonDone, onClick: doDone },
    edit: { icon: icons.edit, text: isNew ? labels.buttonSelect : labels.buttonEdit, onClick: doEdit },
    address: { icon: icons.address, text: labels.buttonAddress, onClick: doLoadAddress },
  }

  const actionLinksChange = []
  if (allowAddress && !editing && showAddressButton) {
    actionLinksChange.push(b.address)
  }

  if (allowEdit) {
    actionLinksChange.push(...editing ? [b.random, b.done] : [b.edit])
  }

  const elementData = 
    elementsQuery.list
      ? UserCollectionExModel.toElementMaps(elementsQuery.list)
      : {}

  return {
    actionLinksChange: actionLinksChange.filter(Boolean),
    address,
    addressState,
    editing,
    edited,
    elementData,
    saving,
    setSaving,
    loading: newExchangesQuery.loading || addressQuery.loading || elementsQuery.loading,
    offered: {
      title: labels.offered,
      list: offered,
      onSelect: onSelectOffered,
    },
    requested: {
      title: labels.requested,
      list: requested,
      onSelect: onSelectRequested,
    },
    labelFn: getResultsLabel(t),
    sortFn: UserExchangeModel.sort
  }
}

export default changeData

function getResultsLabel(t: ITranslate) {
  const results = t(labelIds.results)
  const resultsZero = t(labelIds.resultsZero)
  return (num: number) => (num ? `${results} ${num}` : resultsZero)
}